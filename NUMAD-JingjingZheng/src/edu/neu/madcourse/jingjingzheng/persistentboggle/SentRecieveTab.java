package edu.neu.madcourse.jingjingzheng.persistentboggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class SentRecieveTab extends TabActivity {
	TabHost tabHost;
	
	@SuppressWarnings("deprecation")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		setContentView(R.layout.persistent_sentrecievetab);
		tabHost = (TabHost)findViewById(android.R.id.tabhost);
		
		// Tab for Received
		TabSpec receivedSpec = tabHost.newTabSpec("Received");
		receivedSpec.setIndicator(this.getResources().getText(R.string.tab_label_received));
		Intent receivedIntent = new Intent(this, ReceivedRequest.class);
		receivedSpec.setContent(receivedIntent);

		// Tab for Sent
		TabSpec sentSpec = tabHost.newTabSpec("Sent");
		TextView sentTextView = new TextView(this); 
		sentSpec.setIndicator(this.getResources().getText(R.string.tab_label_sent));
		Intent sentIntent = new Intent(this, SentRequest.class);
		sentSpec.setContent(sentIntent);
		
		// Adding all TabSpec to TabHost
		tabHost.addTab(receivedSpec); // Adding received tab
		tabHost.addTab(sentSpec); // Adding sent tab
	}
	
	public void OnBackClicked(View v) {
		finish();
	}

	public void OnNewRequestClicked(View v) {
		Intent i = new Intent(this, SentRequest.class);
		startActivity(i);
	}
	
}
