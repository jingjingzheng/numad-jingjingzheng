package edu.neu.madcourse.jingjingzheng.persistentboggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.ArrayAdapter;

public class BoardDraw extends SurfaceView implements Callback, Runnable{
	
	private SurfaceHolder sfh;
	private Thread mthread;
	private Paint paint;
	private Canvas canvas;
	private final Rect rect = new Rect(); //？？？？
	private Gameplay gameplayBoggle; 	
	private int selectX;
	private int selectY;
	private char[] wordboard;
	private boolean isRun;
	
	public BoardDraw(Context context){
		super(context);
		this.gameplayBoggle = (Gameplay)context;
		//mthread = new Thread(this);
		sfh = this.getHolder();
		sfh.addCallback(this);
		paint = new Paint();
		setFocusable(true);
		setFocusableInTouchMode(true);
	}

	public BoardDraw(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.gameplayBoggle = (Gameplay)context;
		//mthread = new Thread(this);
		sfh = this.getHolder();
		sfh.addCallback(this);
		paint = new Paint();
		setFocusable(true);
		setFocusableInTouchMode(true);

		// TODO Auto-generated constructor stub
	}
	
	
	private void draw() {
		// TODO Auto-generated method stub
		//super.onDraw(canvas);
		try{
		canvas = sfh.lockCanvas();
		canvas.drawColor(getResources().getColor(R.color.background_boggle));
	    paint.setColor(getResources().getColor(R.color.gameboardline_boggle)); 
	    paint.setStrokeWidth(6);
		
	    canvas.drawLine(0, 0, getWidth(), 0, paint);
	    canvas.drawLine(0, getHeight()/5, getWidth(), getHeight()/5, paint);
	    canvas.drawLine(0, getHeight()/5*2, getWidth(), getHeight()/5*2, paint);
	    canvas.drawLine(0, getHeight()/5*3, getWidth(), getHeight()/5*3, paint);
	    canvas.drawLine(0, getHeight()/5*4, getWidth(), getHeight()/5*4, paint);
	    canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);
	      
	    canvas.drawLine(0, 0, 0, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/5, 0, getWidth()/5, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/5*2, 0, getWidth()/5*2, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/5*3, 0, getWidth()/5*3, getHeight(), paint);	
	    canvas.drawLine(getWidth()/5*4, 0, getWidth()/5*4, getHeight(), paint);	
	    canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), paint);	      
		
		paint.setColor(getResources().getColor(R.color.puzzle_word_boggle));
		paint.setTextSize(getHeight()/5*0.75f);
		//mLetterPaint.setColor(Color.BLACK);
		paint.setAntiAlias(true);			
		paint.setTextAlign(Align.CENTER);
		paint.setTypeface(Typeface.DEFAULT_BOLD);	

		//this.gameplayBoggle = new Gameplay_Boggle();
		wordboard = new char[25];
		wordboard = gameplayBoggle.getWordBoard();
	    // Draw the number in the center of the tile
	    FontMetrics fm = paint.getFontMetrics();
	    // Centering in X: use alignment (and X at midpoint)
	    float x = getWidth() / 10;
	      // Centering in Y: measure ascent/descent first
	    float y = getHeight() / 10 - (fm.ascent + fm.descent) / 2;
	    //float y = getHeight() / 8;
	    //canvas.drawText(String.valueOf(Gameplay.wordboard[0]), x , y, paint);
	    for (int i = 0; i < 5; i++) {
	         for (int j = 0; j < 5; j++) {
	            canvas.drawText(String.valueOf(Gameplay.wordboard[j*5 + i]), 
	            		x + i * getWidth()/5, y + j * getHeight()/5, paint);
	         }
		
	    }
		}catch(Exception ex){
			ex.printStackTrace(); 
		}finally{
			if(canvas != null)
				sfh.unlockCanvasAndPost(canvas);
		}
	    
	    //paint.setColor(getResources().getColor(R.color.puzzle_light));
	    //canvas.drawRect(rect, paint);
	}

	//@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		int action = event.getAction();
		if(action != MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);

		//currentWord = new char[16];
	      if(wrongArea(Math.min(Math.max((int) (event.getX()/(getWidth()/5)), 0), 4), Math.min(Math.max((int)(event.getY()/(getHeight()/5)), 0), 4))){
	    	  return super.onTouchEvent(event);
	      }
		
		gameplayBoggle.getAudioProvider().playSounds(new int[] {AudioProvider.LETTER_CLICKED});

		Gameplay.currentWordBoard[Gameplay.characterIndex++] = this.gameplayBoggle.wordboard[(int)(event.getY()/(getHeight()/5)) * 5 + (int)(event.getX()/(getWidth()/5))];
		//int endTextLength = index - 1;
		Log.i("boarddraw", String.copyValueOf(Gameplay.currentWordBoard));
		this.gameplayBoggle.tvWordboardView.setText(Gameplay.currentWordBoard, 0, Gameplay.characterIndex);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.gameplayBoggle,android.R.layout.simple_list_item_2, android.R.id.text2, gameplayBoggle.getFoundedWords());
		gameplayBoggle.getListView().setAdapter(adapter);
		
		Gameplay.lastSelectX = (int)(event.getX()/(getWidth()/5));
		Gameplay.lastSelectY = (int)(event.getY()/(getHeight()/5));
		//select(lastSelectX, lastSelectY);
		
		return true;	
	}
		
	//Disable words area which does not adjacent to current selected words
	private boolean wrongArea(int x, int y){
		if(Gameplay.lastSelectX == 100){
			return false;
		}
		if(x > Gameplay.lastSelectX + 1 || x < Gameplay.lastSelectX - 1 || y > Gameplay.lastSelectY + 1 || y < Gameplay.lastSelectY -1){
			return true;
		}
		return false;
	 }
	   
	 private void select(int x, int y){
		 selectX = Math.min(Math.max(x, 0), 4);
		 selectY = Math.min(Math.max(y, 0), 4);
		 getRect(selectX, selectY, rect);
		 invalidate(rect);
	 }
	 
	 private void getRect(int x, int y, Rect rect){
		 rect.set(x, y, x + (int)(getWidth()/5), y + (int)(getHeight()/5));
	 }

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		mthread = new Thread(this);
		isRun = true;
		mthread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		isRun = false;
		mthread = null;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(isRun) { 
			draw(); 
			try { 
				Thread.sleep(100); 
			} catch (InterruptedException e) { 
				// TODO Auto-generated catch block 
				e.printStackTrace(); 
			} 
		} 
	}
	
	
}
