package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import android.os.AsyncTask;
import android.util.Log;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class WebServerAccess {
	
	private static final String TeamName = "JingjingZheng";
	private static final String Password = "2013Spring";
	
	private static final String USER_KEY = "user_";
	private static final String PWD_KEY = "pwd_";
	
	private static final String BOARD_PREFIX = "board_";
	private static final String TURN_PREFIX = "turn_";
	private static final String ENTERED_WORDS_PREFIX = "entered_";
	private static final String HIGHSCORE_PREFIX = "highscore_";
	private static final String SCORES_PREFIX = "scores_";
	private static final String HISTORYSCORES_PREFIX = "historyscores_";
	private static final String NUM_TURNS_PREFIX = "numTurns_";
	private static final String REQUESTS_PREFIX = "req_";
	private static final String RECEIVED_PREFIX = "rec_";
	private static final String USERS_KEY = "users";
	private static final String GAMES_KEY = "games";
	private static final String PASS_KEY = "pass_";
	
	private static int boardSize = 5;	
	private static final String TAG = "WebServerAccess";
	private static final int size = 5;
	
	public WebServerAccess(){	
		
	}
	
	//TODO: ERROR HANDLING
	
	/**
	 * Get value associated with the given key
	 * @param key key associated with the given value
	 * @return value value associated with the given key
	 */
	public String getKey(String key){
		return KeyValueAPI.get(TeamName, Password, key);
	}
	
	/**
	 * Store given key/value pair on server
	 * @param key key of given key/value pair
	 * @param value value of given key/value pair
	 * @return null
	 */
	public void putKeyValuePair(String key, String value){
		KeyValueAPI.put(TeamName, Password, key, value);
	}
	
	/**
	 * Clear all key/value pairs from server
	 * @return null
	 */
	public void clearAllKeyValuePair(){
		KeyValueAPI.clear(TeamName, TeamName);
	}
	
	/**
	 * Clear a specific key/value pair 
	 * @param key key of given key/value pair to delete
	 * @return null
	 */
	public void clearSingleKeyValuePair(String key){
		KeyValueAPI.clearKey(TeamName, Password, key);
	}
	
	/**
	 * Checks if or not server is available
	 * @return true if server is available, or return false
	 */
	public boolean IsAvailable(){
		return KeyValueAPI.isServerAvailable();
	}
	
	/**
	 * Convert strings to a String type of arraylist  
	 * @param Users
	 * @return
	 */
	public ArrayList<String> stringToArrayList(String Users){
		ArrayList<String> usersList = new ArrayList<String>(Arrays.asList(Users.split(",")));
		return usersList;
	}
	
	/**
	 * Convert String type of arraylist to String
	 * @param users
	 * @return
	 */
	public String arrayListToString(ArrayList<String> users){
		StringBuilder builder = new StringBuilder();
		for(String user : users){
			if(! user.equals("")){
				builder.append(user + ",");
			}
		}
		return builder.toString();
	}
	
	
	/**
	 * Returns true if the given user/password combination exists
	 * @param user Given username from EditView
	 * @param pass Given password from EditView
	 */
	public void login(String user, String password, final OnBooleanReceivedListener rl){
		final WebServerAccess webServerAccess = this;
		final String userKey = USER_KEY;
		final String pwdKey = PWD_KEY + user;
		
		class LoginTask extends AsyncTask<String, Integer, Boolean>{

			@Override
			protected Boolean doInBackground(String... params) {
				// TODO Auto-generated method stub
				String username = params[0];
				String password = params[1];
				if(! webServerAccess.IsAvailable()){
					Log.d(TAG, "Can not connect to server");
					return false;
				}
				Log.d(TAG, "In doInBackground for LoginTask");
				String pwdVal = webServerAccess.getKey(pwdKey);
				Log.d(TAG, pwdVal + " is the password");
				ArrayList<String> users = stringToArrayList(webServerAccess.getKey(userKey));
				
				if (users.get(0).startsWith("ERROR")) {
					Log.d(TAG, "Connection error to server");
					return false;
				}

				if (!pwdVal.equals(password)) {
					Log.d(TAG, "passwords don't match: "+pwdVal+" stored, "+password+" entered.");
					return false;
				}
				// if we're here, the user exists and the password was right
				return true;

			}

			@Override
			protected void onPostExecute(Boolean result) {
				// TODO Auto-generated method stub
				//super.onPostExecute(result);
				Log.d(TAG, "OnPostExecute in Login asyTask");
				rl.run(result);
			}
			
		}
		try {
			new LoginTask().execute(user, password);
		} catch(Exception e){
			Log.e(TAG, "LoginTask thread died: " + e);
		}
	}
	
	public void register(String user, String password, final OnBooleanReceivedListener rl){
		final WebServerAccess webServerAccess = this;
		final String userKey = USER_KEY;
		final String pwdKey = PWD_KEY + user;
		
		class RegisterTask extends AsyncTask<String, Integer, Boolean>{

			@Override
			protected Boolean doInBackground(String... params) {
				// TODO Auto-generated method stub
				String username = params[0];
				String password = params[1];
				if(! webServerAccess.IsAvailable()){
					Log.d(TAG, "Can not connect to server");
					return false;
				}
				Log.d(TAG, "In doInBackground for RegisterTask");
				//webServerAccess.putKeyValuePair(pwdKey, password);
				ArrayList<String> users = stringToArrayList(webServerAccess.getKey(userKey));
				
				if (users.get(0).startsWith("ERROR")) {
					Log.d(TAG, "Connection error to server");
					return false;
				}

				users.add(username);
				webServerAccess.putKeyValuePair(userKey, webServerAccess.arrayListToString(users));
				webServerAccess.putKeyValuePair(pwdKey, password);
				return true;

			}

			@Override
			protected void onPostExecute(Boolean result) {
				// TODO Auto-generated method stub
				//super.onPostExecute(result);
				Log.d(TAG, "OnPostExecute in Register asyTask");
				rl.run(result);
			}			
		}
		
		try {
			new RegisterTask().execute(user, password);
		} catch(Exception e) {
			Log.e(TAG, "RegisterTask thread died: " +e);
		}

	}
	
	/**
	 * Add a game request for given user
	 * @param user user for whom to add the request
	 * @param req user to add to users'request list
	 */
	private void addRequest(String user, String req){
		String key = REQUESTS_PREFIX + user;
		ArrayList<String> reqs = this.stringToArrayList(this.getKey(key));
		ArrayList<String> recs = this.stringToArrayList(this.getKey(RECEIVED_PREFIX));
		
		//if we already have a received request from that user or we have all already sent
		//a request to that user
		if(reqs.contains(reqs) || recs.contains(recs)){
			Log.e(TAG, "Trying to add a request that already exists");
			throw new RuntimeException("given request is already in the requests list");
		}
		reqs.add(req);
		String val = this.arrayListToString(reqs);
		this.putKeyValuePair(key, val);
	}
	
	/**
	 * Returns a list of the given users' requests
	 * @param user the user whose requests we're getting
	 * @return an ArrayList<String> of all the given users' requests
	 */
	public ArrayList<String> getSentRequests(String user) {
		String key = REQUESTS_PREFIX + user;
		Log.d(TAG, "getSentRequests " + this.getKey(key));
		ArrayList<String> reqs = this.stringToArrayList(this.getKey(key));
		return reqs;
	}

	public void getSentRequests(String user, final OnStringArrayListLoadedListener l) {

		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask GetKeyTask");
		class GetSentRequestsTask extends AsyncTask<String, Integer, ArrayList<String>> {

			protected ArrayList<String> doInBackground(String... key) {
				Log.d(TAG, "in doInBackground for getSentRequests");
				return thisSA.getSentRequests(key[0]);
				//return stringToArrayList(thisSA.get(key[0]));
			}

			protected void onPostExecute(ArrayList<String> result) {
				Log.d(TAG, "in onPostExecute for getSentRequests");
				l.run(result);
			}
		}

		try {
			new GetSentRequestsTask().execute(user);
		} catch(Exception e) {
			Log.e(TAG, "GetKeyTask thread died: " +e);
		}
	}

	/**
	 * Adds all the given requests to the users' requests list
	 * @param user The user to add requests to
	 * @param reqsToAdd The list of Strings that represents all the requests to add
	 */
	public void addRequestList(String user, ArrayList<String> reqsToAdd) {
		ArrayList<String> reqs = this.getSentRequests(user);
		reqs.addAll(reqsToAdd);
		String key = REQUESTS_PREFIX + user;
		String val = this.arrayListToString(reqs);
		this.putKeyValuePair(key, val);
	}
	
	/**
	 * Remove the given request from the given users' sent request list
	 * @param user user whose request list we're removing stuff from
	 * @param req user request we're removing
	 */
	private void removeRequest(String user, String req){
		String key = REQUESTS_PREFIX + user;
		ArrayList<String> curReqs = this.getSentRequests(user);
		System.out.println("getRequests for " + user + "looks like: " + this.arrayListToString(this.getSentRequests(user)));
		if(curReqs.contains(req)){
			curReqs.remove(req);
			String val = this.arrayListToString(curReqs);
			System.out.println("putting '" + val + "' at key " + key);
			this.putKeyValuePair(key, val);
		}
	}
	
	//----------received -------------
	/**
	 * Adds user rec to user1's received list
	 * @param user user whose received list we're editing
	 * @param rec user to add to users's received list
	 * 
	 */
	private void addReceived(String user, String rec){
		String key = RECEIVED_PREFIX + user;
		ArrayList<String> recs = this.stringToArrayList(this.getKey(key));
		ArrayList<String> reqs = this.stringToArrayList(this.getKey(REQUESTS_PREFIX + user));
		
		if(recs.contains(rec) || reqs.contains(reqs)){
			Log.e(TAG, "Trying to add a received request that already exists");
			throw new RuntimeException("Given request already in received request list");
		} 
		recs.add(rec);
		String val = this.arrayListToString(recs);
		this.putKeyValuePair(key, val);
	}
	
	/**
	 * Returns a list of the given users' received requests...asynchronously
	 * @param user User whose requests we're returning
	 * @param arrayListListener Listener whose run(ArrayList<String>) function we're gonna call at the end
	 * @param List of users' received requests
	 */
	public void getReceivedRequests(String user, final OnStringArrayListLoadedListener arrayListListener) {

		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask GetKeyTask");
		class GetReceivedRequestsTask extends AsyncTask<String, Integer, ArrayList<String>> {

			protected ArrayList<String> doInBackground(String... key) {
				return stringToArrayList(thisSA.getKey(key[0]));
			}

			protected void onPostExecute(ArrayList<String> result) {
				Log.d(TAG, "in onPostExecute for getSentRequests");
				arrayListListener.run(result);
			}
		}

		try {
			new GetReceivedRequestsTask().execute(RECEIVED_PREFIX + user);
		} catch(Exception e) {
			Log.e(TAG, "GetKeyTask thread died: " +e);
		}
	}
	
	 /** Returns a list of the given users' received requests
	  * @param user the user whose requests we're getting
	  * @return an ArrayList<String> of all the given users' requests
	  */
	private ArrayList<String> getReceivedRequests(String user) {
		String key = RECEIVED_PREFIX + user;
		ArrayList<String> recs = this.stringToArrayList(this.getKey(key));
		return recs;
	}
	
	/**
	 * Adds all users in the list to the given users' received list
	 * @param user  The user whose received list we're editing
	 * @param recs  The list of requests to add
	 */
	private void addReceivedRequests(String user, ArrayList<String> recs) {
		String key = RECEIVED_PREFIX + user;
		ArrayList<String> curRecs = this.stringToArrayList(this.getKey(key));
		curRecs.addAll(recs);
		String val = this.arrayListToString(curRecs);
		this.putKeyValuePair(key, val);
	}
	
	/**
	 * Removes rec from the given users' list of received requests
	 * @param user  The user whose received list we're editing
	 * @param rec   The received request we're removing
	 */
	private void removeReceivedRequest(String user, String rec) {
		String key = RECEIVED_PREFIX + user;
		ArrayList<String> curRecs = this.getReceivedRequests(user);
		if (curRecs.remove(rec)) {
			System.out.println(rec + " was found in curRecs");
			String val = this.arrayListToString(curRecs);
			System.out.println("putting '" +val +"' at key " +key);
			this.putKeyValuePair(key, val);
		}
	}
	
	/**
	 * Gets list of users
	 * @return An ArrayList representation of users
	 */
	public void getUserList(final OnStringArrayListLoadedListener all){
		final WebServerAccess thisSA = this;
		class GetUserListTask extends AsyncTask<String, Integer, ArrayList<String>>{

			@Override
			protected ArrayList<String> doInBackground(String... params) {
				// TODO Auto-generated method stub
				Log.d(TAG, "In doInBackground for getUserList");
				if(!thisSA.IsAvailable()){
					return new ArrayList<String>(){{
						add("ERROR");
					}};
				}
				String userKey = USER_KEY;
				return thisSA.stringToArrayList(thisSA.getKey(userKey));
			}

			@Override
			protected void onPostExecute(ArrayList<String> result) {
				// TODO Auto-generated method stub
				//super.onPostExecute(result);
				Log.d(TAG, "In onPostExecute in getUserList");
				all.run(result);
			}		
		}
		try {
			new GetUserListTask().execute();
		}catch(Exception e){
			Log.e(TAG, "GetUserList thread died: " + e);
		}
	}
	
	/**
	 * Checks to see if a new user/pass combo can be registered
	 * @param user  The username to check
	 * @param pass  The password to check
	 * @return true if the username/pass can be registered, false otherwise
	 */
	public boolean canRegister(String user, String pass) {
		// if the username or password contains commas, it can't be registered
		if (user.contains(",") || pass.contains(",")) {
			return false;
		} else if (alreadyRegistered(user)) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Checks to see if the given username is already registered
	 * @param user  The username to check
	 * @return true if the username is already registered
	 */
	public boolean alreadyRegistered(String user) {
		String usersKey = USERS_KEY;
		ArrayList<String> users = stringToArrayList(this.getKey(usersKey));
		return users.contains(user);
	}
	
	/**
	 * Returns the key for the given two users - order unspecific
	 * I recognize that this does not generate a very unique
	 * key, but it needs to be an int and we have a very small use case.
	 * @param user1   The first user
	 * @param user2   The second user
	 * @return        An integer value that is the key value for the two users
	 */
	public String getTwoCompUsersKey(String user1, String user2) {
		// String.hashCode has used the same algorithm since java 1.3.1,
		// so i'm comfortable using it here to generate a consistent "unique" identifier
		// for the two given users
		int hash1 = user1.hashCode();
		int hash2 = user2.hashCode();
		int key = hash1+hash2;
		//String key = user1 + user2;
		return "" + key;
	}
	
	//public String setTwoCompUsersKey(String user1, String user2){
		
	//}
	
	/**
	 * Returns true if a game between the two users exists
	 * @param user1   The first user
	 * @param user2   The second user
	 * @return        True if a game exists, false otherwise
	 */
	public boolean doesGameExist(String user1, String user2) {
		String gameKey = GAMES_KEY;
		// get the unique identifier for these two users
		String thisGame = this.getTwoCompUsersKey(user1, user2);
		Log.d(TAG, "this game key is " + thisGame);
		Log.d(TAG, "whole game key is " + this.getKey(gameKey));
		ArrayList<String> games = this.stringToArrayList(this.getKey(gameKey));
		Log.d(TAG, "whole game key is of arraylist type " + games);
		return games.contains(thisGame);
	}
	
	public void IsGameExist(String user1, final OnStringReceivedListener sl){
		final WebServerAccess thisSA = this;
		String userKey = USER_KEY;
		final ArrayList<String> mSendRequestList = thisSA.stringToArrayList(thisSA.getKey(userKey));
		Log.d(TAG, "About to create an AsyncTask IsGameExistTask");
		
		class IsGameExistTask extends AsyncTask<String, Integer, String>{

			@Override
			protected String doInBackground(String... arg0) {
				// TODO Auto-generated method stub
				
				Log.d(TAG, "arg0[0] is " + arg0[0]);
				Log.d(TAG, "jingjing: " + mSendRequestList);
				for(String tmpt : mSendRequestList){
					if(thisSA.doesGameExist(tmpt, arg0[0])){
						return tmpt;
					}
				}
				return "null";
			}
			
			protected void onPostExecute(String result) {
				Log.d(TAG, "in onPostExecute for IsGameExistTask");
				Log.d(TAG, result);
				sl.run(result);
			}			
		}
		
		try {
			new IsGameExistTask().execute(user1);
		} catch(Exception e) {
			Log.e(TAG, "IsGameExistTask thread died: " +e);
		}
		
	}
	
	/**
	 * Adds a game between the two given users to the server
	 * @param user1   The first user
	 * @param user2   The second user
	 */
	private void addGame(String user1, String user2) {
		final String usersKey = this.getTwoCompUsersKey(user1, user2);
		ArrayList<String> games = this.stringToArrayList(this.getKey(GAMES_KEY));
		games.add(usersKey);
		String gameVal = this.arrayListToString(games);
		this.putKeyValuePair(GAMES_KEY,  gameVal);
	}

	public void removeGame(String user1, String user2) {
		String gameKey = GAMES_KEY;
		String usersKey = this.getTwoCompUsersKey(user1, user2);
		ArrayList<String> games = this.stringToArrayList(this.getKey(gameKey));
		games.remove(usersKey);
		String gameVal = this.arrayListToString(games);
		this.putKeyValuePair(gameKey,  gameVal);
	}
	
	/**
	 * Produce an arraylist of all the users that user has game with
	 * @param user User who we are concerned with
	 * @return An arraylist that contains every user that user is in a game with
	 */
	public ArrayList<String> getGames(String user, final OnStringArrayListLoadedListener arrayListListener){
		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask GetGamesTask");
		
		class GetGamesTask extends AsyncTask<String, Integer, ArrayList<String>>{

			@Override
			protected ArrayList<String> doInBackground(String... params) {
				// TODO Auto-generated method stub
				Log.d(TAG, "in doInBackground for GetGamestask");
				String user = params[0];
				ArrayList<String> serverGames = thisSA.stringToArrayList(thisSA.getKey(GAMES_KEY));
				ArrayList<String> serverUsers = thisSA.stringToArrayList(thisSA.getKey(USERS_KEY));
				ArrayList<String> games = new ArrayList<String>();
				
				for (String user2 : serverUsers) {
					if (serverGames.contains(thisSA.getTwoCompUsersKey(user, user2))) {
						Log.d(TAG, "serverUsers: "+ user2);
						games.add(user2);
					}
				}
				
				if (games.isEmpty()) {
					Log.d(TAG, "games is empty! populating with error message");
					games.add("ERROR");
				}
				Log.d(TAG, "returning: " +games.toString()+"for current games"); 
				return games;
			}
			
			protected void onPostExecute(ArrayList<String> result) {
				Log.d(TAG, "in onPostExecute for GetGamesTask");
				arrayListListener.run(result);
			}			
		}
		try {
			new GetGamesTask().execute(user);
		} catch(Exception e) {
			Log.e(TAG, "GetGames thread died: " +e);
		}
		
		ArrayList<String> serverGames = this.stringToArrayList(this.getKey(GAMES_KEY));
		ArrayList<String> serverUsers = this.stringToArrayList(this.getKey(USERS_KEY));
		ArrayList<String> games = new ArrayList<String>();
		for (String user2 : serverUsers) {
			if (serverGames.contains(this.getTwoCompUsersKey(user, user2))) {
				games.add(user2);
			}
		}
		return games;
	}
	
	/**
	 * Only called when a previous game does not exist
	 * @param creator
	 * @param opponent
	 * @param board
	 */
	private void initializeNewGame(String creator, String opponent, String board){
		Log.d(TAG, "Creator: " + creator + "|");
		Log.d(TAG, "Opponent: " + opponent + "|");
		
		String userskey = this.getTwoCompUsersKey(creator, opponent);
		
		//add game to game list
		this.addGame(creator, opponent);
		System.out.println("adding game: " +creator +" vs " +opponent);
		
		//add board to the server
		String boardKey = BOARD_PREFIX + userskey;
		Log.d(TAG, "board key: " + boardKey);
		Log.d(TAG, "board board: " + board);
		
		//put board on the server
		this.putKeyValuePair(boardKey, board);
		Log.d(TAG, "board Content: " + this.getKey(boardKey));
		
		//value of turn element will always be a username - initiates to the creator
		String turnKey = TURN_PREFIX + userskey;
		Log.d(TAG, "turnKey Key: " + turnKey);
		
		//put the turn on the server
		this.putKeyValuePair(turnKey, creator);
		Log.d(TAG, "turnKey Content: " + this.getKey(boardKey));
		
		//TODO:
		//if in asynchronous mode put entered words elements on the server
		//if in synchronous mode not do this
		String enteredKey = ENTERED_WORDS_PREFIX + userskey;
		this.putKeyValuePair(enteredKey, "");
		
		//put scores on the server
		//score is a string like "usr1|50. user2|44"
		String scoresKey = SCORES_PREFIX + userskey;
		String scoresContent = creator + "|0," + opponent + "|0";
		this.putKeyValuePair(scoresKey, scoresContent);
		
		Log.d(TAG, "scores key: "+ scoresKey);
		Log.d(TAG, "scores Content: "+ this.getKey(scoresKey));
		System.out.println(scoresKey + " is now: " + creator + "|0," + opponent + "|0");
		
		//create the number of turns element on the server
		String numTurnsKey = NUM_TURNS_PREFIX + userskey;
		this.putKeyValuePair(numTurnsKey, "0");
	}
	
	/**
	 * Update the score for game
	 * @param user1
	 * @param score_1
	 * @param user2
	 * @param score_2
	 */
	public void setScoreContent(String user1, int score_1, String user2, int score_2){
		String userskey = this.getTwoCompUsersKey(user1, user2);
		String scoresKey = SCORES_PREFIX + userskey;
		String scoresContent = user1 + "|" +score_1+ "," + user2 + "|"+score_2;
		this.putKeyValuePair(scoresKey, scoresContent);
	}
	
	/**
	 * Get the board fo game
	 * @param user1
	 * @param user2
	 * @return
	 */
	public String getBooard(String user1, String user2){
		String userskey = this.getTwoCompUsersKey(user1, user2);
		// add board to the server
		String boardKey = BOARD_PREFIX + userskey;
		return this.getKey(boardKey);
	}
	
	/**
	 * Send a request from user1 to user2
	 * @param user1 The sender of the request
	 * @param user2 The receiver of the request
	 */
	public void sendRequest(String user1, String user2, final OnBooleanReceivedListener booleanListener) {

		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask sendRequestTask");
		class sendRequestTask extends AsyncTask<String, Integer, Boolean> {

			protected Boolean doInBackground(String... key) {

				// if we can't connect to the server, stop and return false.
				if (!thisSA.IsAvailable()) {
					return false;
				}

				Log.d(TAG, "in doInBackground for sendRequestTask");
				String user1 = key[0];
				String user2 = key[1];

				try {
					thisSA.addRequest(user1, user2);
					thisSA.addReceived(user2, user1);
				} catch(Exception e) {
					return false;
				}
				Log.d(TAG, "Added the request successfully");
				return true;
			}

			protected void onPostExecute(Boolean result) {
				Log.d(TAG, "in onPostExecute for sendRequestTask. Result: " +result);
				booleanListener.run(result);
			}
		}

		try {
			new sendRequestTask().execute(user1, user2);
		} catch(Exception e) {
			Log.e(TAG, "sendRequestTask thread died: " +e);
		}
	}
	
	
	/**
	 * Remove a sent request from user1 to user2
	 * @param user1 The sender of the request to be removed
	 * @param user2 The receiver of the request to be removed
	 */
	public void removeSentRequest(String user1, String user2, final OnBooleanReceivedListener booleanListener) {
		final WebServerAccess thisSA = this;
		class removeSentRequestTask extends AsyncTask<String, Integer, Boolean> {
			protected Boolean doInBackground(String... keys) {
				String user1 = keys[0];
				String user2 = keys[1];

				Log.d(TAG, "In doInBackground for removeSentRequest");
				if (!thisSA.IsAvailable()) {
					return false;
				}
				try {
					thisSA.removeReceivedRequest(user2, user1);
					thisSA.removeRequest(user1, user2);
				} catch(Exception e) {
					return false;
				}
				Log.d(TAG, "removed request successfully");
				return true;
			}
			protected void onPostExecute(Boolean result) {
				Log.d(TAG, "in onPostExecute for removeSentRequest. Result: "+result);
				booleanListener.run(result);
			}
		}
		try {
			new removeSentRequestTask().execute(user1, user2);
		} catch(Exception e) {
			Log.e(TAG,  "removeRequestTask thread died: " +e);
		}
	}
	
	/**
	 * Generated random wordboard
	 * @return
	 */
	protected static String generateBoard(){
		Log.d(TAG, "create gameboard");
		int total_boardnum = size*size;
		List<Character> set_letters = new ArrayList<Character>(total_boardnum);
		
		Random randomWord = new Random();
		for(int i = 0; i < total_boardnum; i++){
			char c = (char)(65 + randomWord.nextInt(26));
			set_letters.add(c);
		}
		
		String final_letters = "";
		for(Character s : set_letters){
			final_letters += s;
		}
		Log.d(TAG, "Pick letters: " + final_letters);
		
		return final_letters;
		
	}
	
	/**
	 * Convert character type words to a string for state saving
	 * @param board list representation of board
	 * @return String representation of the board
	 */
	static private String boardToString(List<Character> board){
		StringBuilder builder = new StringBuilder();
		for(Character ch : board){
			builder.append(ch);
		}
		return builder.toString();
	}
	
	/**
	 * Convert a string to a board
	 * @param rep
	 * @return
	 */
	protected ArrayList<Character> stringToBoard(String rep){
		ArrayList<Character> board = new ArrayList<Character>();
		for(int i = 0; i < rep.length(); i++){
			board.add(rep.charAt(i));
		}
		return board;
	}
	
	/**
	 * Create a new game
	 * @param user1
	 * @param user2
	 * @param rl
	 */
	public void createNewGame(String user1, String user2, final OnBooleanReceivedListener rl){
		final String usersKey = this.getTwoCompUsersKey(user1, user2);
		final WebServerAccess thisSA = this;

		Log.d(TAG, "about to create an asynctask CreateGameTask");

		class CreateGameTask extends AsyncTask<String, Integer, Boolean> {

			protected Boolean doInBackground(String... key) {
				String user1 = key[0];
				String user2 = key[1];
				System.out.println("Creating a new game!");
				if (!thisSA.IsAvailable()) {
					return false;
				}
				String board = generateBoard();
				thisSA.initializeNewGame(user1, user2, board);

				// put updated games list into the server
				return true;
			}

			protected void onPostExecute(Boolean result) {
				Log.d(TAG, "in onPostExecute for createGameTask");
				rl.run(result);
			}
		}
		try {
			new CreateGameTask().execute(user1, user2);
		} catch(Exception e) {
			Log.e(TAG, "CreateGameTask thread died: "+e);
		}
	}
	
	/**
	 * Update gameboard on the server-side
	 * @param user1
	 * @param user2
	 * @param board
	 */
	public void updateBoard(String user1, String user2, String board) {
		String boardKey = BOARD_PREFIX + this.getTwoCompUsersKey(user1, user2);
		String boardVal = board;
		this.putKeyValuePair(boardKey, boardVal);
	}
	
	public GameWrapper getGame(String user1, String user2){
		Log.d(TAG, "USER1: " + user1 + "|");
		Log.d(TAG, "USER2: " + user2 + "|");

		String userskey = this.getTwoCompUsersKey(user1, user2);

		Log.d(TAG, "getGame userKey: "+ userskey);

		String turnKey = TURN_PREFIX + userskey;
		String currentTurn = 
				this.getKey(turnKey);

		String boardKey = BOARD_PREFIX + userskey;
		Log.d(TAG, "getGame boardKey: "+ boardKey);
		String board = this.getKey(boardKey);
		Log.d(TAG, "getGame boardLetters: "+ board.toString());

		String enteredWordsKey = ENTERED_WORDS_PREFIX + userskey;
		ArrayList<String> enteredWords =
				this.stringToArrayList(this.getKey(enteredWordsKey));

		String scoresKey = SCORES_PREFIX + userskey;
		Log.d(TAG, "getGame scoresKey: "+ scoresKey);
		String gameScores = this.getKey(scoresKey);
		Hashtable<String, Integer> scores = this.scoresStringToHashtable(
				gameScores);

		String numTurnsKey = NUM_TURNS_PREFIX + userskey;
		int numTurns = Integer.parseInt(this.getKey(numTurnsKey));

		return new GameWrapper(
				currentTurn,
				board,
				enteredWords,
				scores,
				numTurns);
	}

	/**
	 * 
	 * @param gameScores
	 * @return
	 */
	private Hashtable<String, Integer> scoresStringToHashtable(String scores) {
		// TODO Auto-generated method stub
		Log.d(TAG, "scoresToString: " + scores);
		ArrayList<String> parts = new ArrayList<String>(
				Arrays.asList(scores.split(",")));
		Hashtable<String, Integer> scoresboard = new Hashtable<String, Integer>();
		for(String usrScore : parts){
			List<String> part = Arrays.asList(usrScore.split("\\|"));
			System.out.println("part is now: " + part.toString());
			System.out.println("parsing " + part.get(1) + " as an integer.");
			scoresboard.put(part.get(0), Integer.parseInt(part.get(1)));
		}
		return scoresboard;
	}
	
	/**
	 * Returns current turn of the game
	 * @param user1
	 * @param user2
	 * @return
	 */
	public String getTurn(String user1, String user2){
		String userskey = this.getTwoCompUsersKey(user1, user2);
		String turnKey = TURN_PREFIX + userskey;
		String currentTurn = this.getKey(turnKey);
		return currentTurn;
	}
	
	/**
	 * Sets the Games current player's turn
	 * @param user1 << Host
	 * @param user2 << Opponent
	 * @param turn player's turn in the game
	 * @return string representation of current turn of the game
	 */
	public void setTurn(String user1, String user2, String turn){
		String userskey = this.getTwoCompUsersKey(user1, user2);
		String turnKey = TURN_PREFIX + userskey;
		this.putKeyValuePair(turnKey, turn);
	}
	
	/**
	 * Sets the number of turns passed
	 * @param user1
	 * @param user2
	 * @param numTurns
	 * @param l
	 */
	public void setTurnTotal(String user1, String user2, int numTurns, final OnBooleanReceivedListener l){
		
		final String usersKey = this.getTwoCompUsersKey(user1, user2);
		final String numTurnsKey = NUM_TURNS_PREFIX + usersKey;
		final WebServerAccess thisSA = this;
		final int nTurns = numTurns;
		Log.d(TAG, "about to create an asynctask setTurnTotalTask");

		class setTurnTotalTask extends AsyncTask<String, Integer, Boolean> {

			protected Boolean doInBackground(String... key) {
				System.out.println("Setting the turn totals!");
				if (!thisSA.IsAvailable()) {
					return false;
				}
				thisSA.putKeyValuePair(numTurnsKey, Integer.toString(nTurns));
				// this totally worked even though i didnt check.  trust me.
				return true;
			}

			protected void onPostExecute(Boolean result) {
				Log.d(TAG, "in onPostExecute for setTurnTotalTask");
				l.run(result);
			}
		}
		try {
			new setTurnTotalTask().execute();
		} catch(Exception e) {
			Log.e(TAG, "setTurnTotalTask thread died: "+e);
		}
	}
	
	
	/**
	 * Sets the enteredWords for the game
	 * @param user1
	 * @param user2
	 * @param usedWords
	 */
	public void setEnteredWords(String user1, String user2, String usedWords, final OnBooleanReceivedListener l){
		final WebServerAccess thisSA = this;
		final String usersKey = this.getTwoCompUsersKey(user1, user2);
		final String enteredWordsKey = ENTERED_WORDS_PREFIX + usersKey;
		class setEnteredWordsTask extends AsyncTask<String, Integer, Boolean> {

			protected Boolean doInBackground(String... key) {
				String usedWords = key[0];
				System.out.println("Setting the entered words list!");
				if (!thisSA.IsAvailable()) {
					return false;
				}
				thisSA.putKeyValuePair(enteredWordsKey, usedWords);
				// this totally worked even though i didnt check.  trust me.
				return true;
			}

			protected void onPostExecute(Boolean result) {
				Log.d(TAG, "in onPostExecute for setEnteredWordsTask");
				l.run(result);
			}
		}
		try {
			new setEnteredWordsTask().execute(usedWords);
		} catch(Exception e) {
			Log.e(TAG, "setEnteredWordsTask thread died: "+e);
		}
	}
	
	/**
	 * Gets the enteredWords for the game
	 * @param user1
	 * @param user2
	 * @param usedWords
	 */
	public String getEnteredWords(String user1, String user2){
		String userskey = this.getTwoCompUsersKey(user1, user2);
		String enteredWordsKey = ENTERED_WORDS_PREFIX + userskey;
		return this.getKey(enteredWordsKey);
	}
	
	public Boolean setHighscore(String user1, int score){
		String enternedHighScore = HIGHSCORE_PREFIX + user1;
		this.putKeyValuePair(enternedHighScore, Integer.toString(score));
		return true;
	}
	
	public void getHighscoreofPlayer(String user1, final OnIntReceivedListener ll){
		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask GetHighScoreTask");
		class GetHighScoreTask extends AsyncTask<String, Integer, Integer>{

			@Override
			protected Integer doInBackground(String... arg0) {
				// TODO Auto-generated method stub
				Log.d(TAG, "in doInBackground for getSentRequests");
				return getHighscore(arg0[0]);
			}
			
			protected void onPostExecute(Integer result) {
				Log.d(TAG, "in onPostExecute for getSentRequests");
				ll.run(result);
			}
			
		}
		try {
			new GetHighScoreTask().execute(user1);
		} catch(Exception e) {
			Log.e(TAG, "GetHighScoreTask thread died: " +e);
		}
		
	}
	
	public void setHighscoreofPlayer(String user1, int score, final OnBooleanReceivedListener ll){
		final WebServerAccess thisSA = this;
		Log.d(TAG, "About to create an AsyncTask GetHighScoreTask");
		class SetHighScoreTask extends AsyncTask<String, Integer, Boolean>{

			protected Boolean doInBackground(String... arg0) {
				// TODO Auto-generated method stub
				Log.d(TAG, "in doInBackground for SetHighScoreTask");
				return setHighscore(arg0[0], Integer.parseInt(arg0[1]));
			}
			
			protected void onPostExecute(Boolean result ) {
				Log.d(TAG, "in onPostExecute for SetHighScoreTask");
				ll.run(result);
			}
			
		}
		try {
			new SetHighScoreTask().execute(user1, String.valueOf(score));
		} catch(Exception e) {
			Log.e(TAG, "SetHighScoreTask thread died: " +e);
		}
		
	}
	
	public int getHighscore(String user1){
		String enternedHighScore = HIGHSCORE_PREFIX + user1;
		if(this.getKey(enternedHighScore) == ""){
			return 0;
		}else{
			int score = Integer.parseInt(this.getKey(enternedHighScore));
			return score;
		}
	}
	
	public void setHistoryScore(String user1, int score){
		String currentScore = HISTORYSCORES_PREFIX + user1;
		this.putKeyValuePair(currentScore, Integer.toString(score));
	}
	
	public int getHistoryScore(String user1){
		String  highScoreKey = HISTORYSCORES_PREFIX + user1;
		int score = Integer.parseInt(this.getKey(highScoreKey));
		return score;
	}
	
	/**
	 * Converts a given scores hashtable to a string
	 * @param scores The hashtable of the two users' scores
	 * @return The string representation of the two users' scores.
	 */
	private String hashtableToScoresString(Hashtable<String, Integer> scores) {
		StringBuilder sb = new StringBuilder();
		List<String> usernames = Collections.list(scores.keys());
		String user1 = usernames.get(0);
		String user2 = usernames.get(1);

		sb.append(user1 + "|" + scores.get(user1) + "," +
				user2 + "|" + scores.get(user2));
		return sb.toString();
	}
	
}
