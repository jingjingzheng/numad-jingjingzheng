package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;

public interface OnStringArrayListLoadedListener {

	public void run(ArrayList<String> result);

}
