package edu.neu.madcourse.jingjingzheng.persistentboggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Boggle extends Activity implements OnClickListener{
	
	public static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	private static final String PREF_USER = "prefUser";
	private static final String PREF_PASS = "prefPass";
	private static final String MULTI_PREF = null;
	
	private static final String TAG = "PersistBoggle";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/** Set titlebar with my name*/
		this.setTitle("Persistent Boggle");
		
		/**Add small icon on titlebar*/
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.persistent_launcher);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
	    // Set up click listeners for all the buttons

	    View btnChallengePersist = findViewById(R.id.btn_launcher_persist_challenge);
	    btnChallengePersist.setOnClickListener((OnClickListener)this);
	    View btnScorePersist = findViewById(R.id.btn_launcher_persist_scores);
	    btnScorePersist.setOnClickListener((OnClickListener)this);    
	    View btnAcknowledgePersist = findViewById(R.id.btn_launcher_persist_acknowledge);
	    btnAcknowledgePersist.setOnClickListener((OnClickListener)this);
	    View btnExitPersist = findViewById(R.id.btn_launcher_persist_exit);
	    btnExitPersist.setOnClickListener((OnClickListener)this);
	    View btnRulePersist = findViewById(R.id.btn_launcher_persist_rules);
	    btnRulePersist.setOnClickListener((OnClickListener)this);

	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_launcher_persist_challenge:
			if(isUserInfoAvailable()){
				Intent _intent_login = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.persistentboggle.Login.class);
				_intent_login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(_intent_login);
			}else{
				Intent _intent_newgame = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.persistentboggle.SentRecieveTab.class);
				_intent_newgame.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(_intent_newgame);
			}
			break;
		case R.id.btn_launcher_persist_scores:
			//build one dialog to show scoreinfo
			final Dialog mDialog = new Dialog(Boggle.this);
			mDialog.setContentView(R.layout.persistent_dialogscore);
			mDialog.setTitle("Game center");
			mDialog.setCancelable(true);
			
			TextView tvDialogScore = (TextView)mDialog.findViewById(R.id.tv_dialog_persist_score);
			
			Button btnDialogachieve = (Button)mDialog.findViewById(R.id.btn_dialog_persist_achievement);
			Button btnDialogleaderboard = (Button)mDialog.findViewById(R.id.btn_dialog_persist_leaderboard);
			Button btnDialogcancel = (Button)mDialog.findViewById(R.id.btn_dialog_persist_cancel);
			btnDialogachieve.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent _intent_score = new Intent(Boggle.this, Achievements.class );
					_intent_score.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(_intent_score);
				}
				
			});
			btnDialogleaderboard.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent _intent_score = new Intent(Boggle.this, Leaderboard.class);
					_intent_score.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(_intent_score);
				}
				
			});
			btnDialogcancel.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mDialog.cancel();
				}
				
			});
			mDialog.show();
			/*Intent _intent_score = new Intent(Boggle.this, Settings.class);
			_intent_score.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_score);*/
		    break;
		case R.id.btn_launcher_persist_acknowledge:
			Intent _intent_ack = new Intent(Boggle.this, Acknowledge.class);
			_intent_ack.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_ack);
		    break;
		case R.id.btn_launcher_persist_rules:
			Intent _intent_rules = new Intent(Boggle.this, Rules.class);
			_intent_rules.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_rules);
		    break;
		// More buttons go here (if any) ...
		case R.id.btn_launcher_persist_exit:
		    Boggle.this.finish();
		    System.exit(0);
		    break;
		}
	}
	
	private boolean isUserInfoAvailable(){
		SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		String username = pref.getString(PREF_USER, "null");
		String password = pref.getString(PREF_PASS, "null");
		Log.d(TAG, "isUserInfoAvailable: " + username);
		boolean result = (username.equalsIgnoreCase("null")&& password.equalsIgnoreCase("null"));
		Log.d(TAG, "isUserInfoAvaible: " + result);
		return result;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    super.onCreateOptionsMenu(menu);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}

}
