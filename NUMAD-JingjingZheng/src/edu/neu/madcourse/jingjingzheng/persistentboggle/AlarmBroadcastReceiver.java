package edu.neu.madcourse.jingjingzheng.persistentboggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.widget.Toast;


public class AlarmBroadcastReceiver extends BroadcastReceiver {
	private WebServerAccess mWebServerAccess;
	private static final String TAG = "Persist_AlarmBroadcastReceiver";
	public static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	public static final String PREF_USER = "prefUser";
	private static final String PREF_SCORE = "GameplayScore";
	private String username;
	private String opponentname;
	private int opponentScore;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
		wl.acquire();

		username = intent.getStringExtra("username");
		opponentname = intent.getStringExtra("opponent");
		opponentScore = intent.getIntExtra("opponentCurrentScore", 0);
		//String username = intent.getStringExtra("username");
		//String opponent = intent.getStringExtra("opponent");
		//String currentTurn = intent.getStringExtra("currentTurn");
		//String serverCurrentTurn = mWebServerAccess.getTurn(username, opponent);
		int serverCurrentOppoScore = mWebServerAccess.getHighscore(opponentname);

		if (serverCurrentOppoScore != opponentScore){

			Intent ns = new Intent(context, Notification_Receiver.class);
			PendingIntent pIntent = PendingIntent.getActivity(context, 0, ns, 0);
			
			NotificationManager nm = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
			@SuppressWarnings("deprecation")
			Notification notif = new Notification(
		            R.drawable.ic_launcher, 
		            "New score from " + opponentname,
		            System.currentTimeMillis());
			notif.setLatestEventInfo(context,"PersistentBoggle", "New Score from competitor", pIntent);
			nm.notify(0, notif);
		}

		Toast.makeText(context, "Alarm !", Toast.LENGTH_LONG).show(); // For example

		wl.release();
	}
	
	public void SetAlarm(Context context)
	{
		AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmBroadcastReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 5, pi); // Millisec * Second * Minute
	}

	public void CancelAlarm(Context context)
	{
		Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}

}
