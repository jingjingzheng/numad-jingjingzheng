package edu.neu.madcourse.jingjingzheng.persistentboggle;

public interface OnStringReceivedListener {
	public void run(String exitState);
}
