package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;
import java.util.Arrays;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SentRequest extends ListActivity {
	private static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	private static final String PREF_USER = "prefUser";
	private WebServerAccess sa;
	private String USERNAME;
	private String TAG = "Persist_Sent_Requests";
	Sent_Request_Adaptor adapter;
	ArrayList<String> mSendRequestList;

	public void setUsername(){
		SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		USERNAME = pref.getString(PREF_USER, null);
	}

	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		Log.d(TAG, "set contentview");

		setUsername();

		setContentView(R.layout.persistent_sent);

		getListView().setEmptyView(findViewById(android.R.id.empty));
		sa = new WebServerAccess();

		this.setAsynchronousListAdapter(); // populates the adapter with the sent requests
		this.setAutoStartListener();
		
		/*mSendRequestList = sa.getSentRequests(USERNAME);
		for(String tmpt : mSendRequestList){
			if(sa.doesGameExist(tmpt, USERNAME)){
				//String userskey = sa.getTwoCompUsersKey(tmpt, USERNAME);
				//String boardKey = BOARD_PREFIX + userskey;
				//String board = 
				Intent mIntent = new Intent(SentRequest.this, Gameplay.class);
				//PendingIntent _pending = PendingIntent.getActivity(this, 0, mIntent, 0);
				mIntent.putExtra("opponent", tmpt);
				startActivity(mIntent);
				break;
			}
		}*/
	}
	
	private void setAutoStartListener(){
		final SentRequest thisActivity = this;
		
		sa.IsGameExist(USERNAME, new OnStringReceivedListener(){

			@Override
			public void run(String exitState) {
				Log.d(TAG, "in onPostExecute for exitState");
				// TODO Auto-generated method stub
				if(!exitState.equals("null")){
					Intent mIntent = new Intent(SentRequest.this, Gameplay.class);
					//PendingIntent _pending = PendingIntent.getActivity(this, 0, mIntent, 0);
					mIntent.putExtra("opponent", exitState);
					startActivity(mIntent);
				}
			}
			
		});
	}

	/**
	 * Sets the listAdapter asynchronously, with information from
	 * the server.
	 */
	private void setAsynchronousListAdapter() {
		final SentRequest thisActivity = this;

		sa.getSentRequests(USERNAME, new OnStringArrayListLoadedListener() {
			public void run(ArrayList<String> list) {
				adapter = new Sent_Request_Adaptor(thisActivity, R.layout.persistent_sent, list);
				Log.v(TAG, "Setting Sent Requests List adapter: " +list.toString());
				setListAdapter(adapter);
			}
		});
	}

	public void onResume(){
		super.onResume();		
		getListView().setEmptyView(findViewById(R.id.emptyView));

		final SentRequest thisActivity = this;

		this.setAsynchronousListAdapter();
		this.setAutoStartListener();
	}



	public class Sent_Request_Adaptor extends BaseAdapter{
		private ArrayList<String> mSentRequests = new ArrayList<String>();
		private Context mContext;
		private int rowResID;

		public Sent_Request_Adaptor(Context c, int rowResID, ArrayList<String> sentRequestsList){
			this.rowResID = rowResID;
			this.mContext = c;
			
			Log.d(TAG, "board request is " +sentRequestsList.toString());
			Log.d(TAG, "first of the request list is " +sentRequestsList.get(0));
			Log.d(TAG, "does it start with error? " +sentRequestsList.get(0).startsWith("ERROR"));
			
			
			this.mSentRequests = sentRequestsList;
		}


		public int getCount() {
			// TODO Auto-generated method stub
			return mSentRequests.size();
		}


		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}


		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public boolean isEmptyList(){
			return mSentRequests.contains("");
		}
		
		public boolean hadError() {
			return mSentRequests.get(0).startsWith("ERROR");
		}

		public View getView(int position, View view, ViewGroup parent) {
			final String row = this.mSentRequests.get(position);
			boolean isEmpty = isEmptyList();
			boolean hadError = hadError();
			Log.d(TAG, "Empty ArrayList "+ isEmpty);
			Log.d(TAG, "Had error " +hadError);

			if(view == null ){
				getListView().setEmptyView(findViewById(android.R.id.empty));
				if(!isEmpty && !hadError){
					Log.d(TAG, "button is there");
					LayoutInflater inflater = LayoutInflater.from(parent.getContext());
					view = inflater.inflate(R.layout.persistent_sent_rows, parent, false);
				}else{
					Log.d(TAG, "button isn't there");
					LayoutInflater inflater = LayoutInflater.from(parent.getContext());
					view = inflater.inflate(R.layout.persistent_sent_rows_empty, parent, false);
				}
			}

			TextView username = (TextView) 
					view.findViewById(R.id.tv_sentrequests_persist_content);
			if(!isEmpty && !hadError){
				username.setText(mSentRequests.get(position));
			} else if (hadError) {
				username.setText("Couldn't retrieve data...");
			}
			else{
				username.setText("No Requests Sent");
			}

			buttonClickHandler btn_Handler = new buttonClickHandler(username, row);

			Button btnDelete = (Button) view.findViewById(R.id.btn_persist_delete);
			btnDelete.setOnClickListener(btn_Handler);
			Log.d(TAG,"mSentRequests list: "+ mSentRequests.toString());
			Log.d(TAG,"mSentRequests row: "+ row);
			btnDelete.setTag(row);

			// Give it a nice background
			return view;
		}			

		public void deleteRow(String row) {
			if(this.mSentRequests.contains(row)) {
				this.mSentRequests.remove(row);
			}
		}

		class buttonClickHandler implements View.OnClickListener {
			TextView textView;
			String row;

			public buttonClickHandler(TextView textView, String row ) {
				this.textView = textView;
				this.row = row;
			}

			public void onClick(View v) {
				Button button = (Button) v;
				final String row = (String) button.getTag();

				switch(v.getId()){
				case R.id.btn_persist_delete:
					//Start new game activity
					//TODO: Update Request List & Create new game pair -> Server Call
					Log.d(TAG, "Delete Button Clicked"); 
					
					//username is the user that sent the request that we have to remove
					sa.removeSentRequest(USERNAME, row, new OnBooleanReceivedListener() {
						public void run(Boolean exitState) {
							if (!exitState) {
								Toast.makeText(getBaseContext(), 
										"Could not delete request to "+row+".  Try again!", 
										Toast.LENGTH_SHORT).show();
							} else {
								Toast.makeText(getBaseContext(), 
										"Request to "+row +" deleted!", 
										Toast.LENGTH_SHORT).show();
							}
						}
					});
					//Log.d(TAG, "updated list after delete: "+sa.getSentRequests(USERNAME).toString());

					deleteRow(row);
					notifyDataSetChanged();
					Log.d(TAG, "Delete Button Clicked Delete Row");
					break;
				}
			}

		}

	}

	/*	
	 * Starts new Multiplayer Boggle Game	 
	 */
	public void onPersistRequestsOkButtonClicked(View v) {
		Intent i = new Intent(this, NewRequest.class);
		startActivity(i);
	}

	public void onPersistRequestsCancelButtonClicked(View v) {
		finish();
	}
	
	public ArrayList<String> stringToArrayList(String Users){
		ArrayList<String> usersList = new ArrayList<String>(Arrays.asList(Users.split(",")));
		return usersList;
	}
}
