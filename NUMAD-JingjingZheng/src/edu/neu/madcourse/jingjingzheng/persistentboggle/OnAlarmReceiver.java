package edu.neu.madcourse.jingjingzheng.persistentboggle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class OnAlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
	    GameIntentService.acquireStaticLock(context);
	    
	    Toast.makeText(context, "Alarm", Toast.LENGTH_LONG).show();
	    
	    Log.d("AlarmReceiver", "AlarmReceiver");
	    context.startService(new Intent(context, GameService.class));
	}

}
