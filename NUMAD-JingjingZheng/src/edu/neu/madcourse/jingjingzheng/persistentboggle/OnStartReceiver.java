package edu.neu.madcourse.jingjingzheng.persistentboggle;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class OnStartReceiver extends BroadcastReceiver {
	
	private static final String TAG = "OnStartReceiver";
	private static final int IntervalTime = 360000;

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		AlarmManager mgr = (AlarmManager)arg0.getSystemService(Context.ALARM_SERVICE);
		Intent _intent = new Intent(arg0, OnAlarmReceiver.class);
		PendingIntent _pending = PendingIntent.getBroadcast(arg0, 0, _intent, 0);
		
		Toast.makeText(arg0, "OnStart", Toast.LENGTH_LONG).show();
		Log.d(TAG, "OnStart");
		
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime()+60000,
                IntervalTime,
                _pending);
	}

}
