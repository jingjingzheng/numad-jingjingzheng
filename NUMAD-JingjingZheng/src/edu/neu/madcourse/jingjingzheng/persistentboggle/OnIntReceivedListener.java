package edu.neu.madcourse.jingjingzheng.persistentboggle;

public interface OnIntReceivedListener {
	public void run(int exitState);
}
