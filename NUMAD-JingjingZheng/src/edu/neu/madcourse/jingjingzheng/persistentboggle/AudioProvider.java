package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.HashMap;

import edu.neu.madcourse.jingjingzheng.R;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class AudioProvider {

	public static int BAD_WORD_SUBMITED = 0;
	public static int LETTER_CLICKED = 1;
	public static int WORD_CANCELED = 2;
	public static int GOOD_WORD_SUBMITED = 3;
	
	private Context mContext;
	public SoundPool mSoundPool;
	private HashMap<Integer, Integer> mSoundPoolMap;
	
	public AudioProvider(Context context) {
		mContext = context;
		init();
	}
	
	private void init() {
		mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		mSoundPoolMap = new HashMap<Integer, Integer>();
		mSoundPoolMap.put(BAD_WORD_SUBMITED, mSoundPool.load(mContext, R.raw.badsubmit_boggle, 1));
		mSoundPoolMap.put(LETTER_CLICKED, mSoundPool.load(mContext, R.raw.buttonclick_boggle, 1));	
		mSoundPoolMap.put(WORD_CANCELED, mSoundPool.load(mContext, R.raw.buttoncancel_boggle, 1));
		mSoundPoolMap.put(GOOD_WORD_SUBMITED, mSoundPool.load(mContext, R.raw.goodsubmit_boggle, 1));
	}
	
	private int[] mSoundFiles;
	
	public void playSounds(int[] sounds) {
		mSoundFiles = sounds;
		
		new Thread() {
			@Override
			public void run() {
				playSounds();
			}
		}.start();
		
	}
	
	private void playSounds() {
		AudioManager mgr = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		float volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		
		for (int sound : mSoundFiles) {
			mSoundPool.play(mSoundPoolMap.get(sound), volume, volume, 1, 0, 1f);
		}
	}
	
	public void destroy() {
		mSoundPool.release();
		mSoundPool = null;
	}
}
