package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

public class Achievements extends ListActivity{
	
	private Button btnAchievementBack;
	private WebServerAccess mWebServerAccess;
	private String TAG = "Persist_achievement";
	
	private static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	private static final String PREF_USER = "prefUser";
	private ArrayList<String> mUserList = new ArrayList<String>();
	private ArrayAdapter<String> mAdapter;
	private String username;
	
	public void setUsername(){
		    SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		    username = pref.getString(PREF_USER, null);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.persistent_scoreachievements);
		getListView().setEmptyView(findViewById(android.R.id.empty));
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
		setUsername();
		mWebServerAccess = new WebServerAccess();
		
		mWebServerAccess.getUserList(new OnStringArrayListLoadedListener(){

			@Override
			public void run(ArrayList<String> result) {
				// TODO Auto-generated method stub
				if(result.get(0).startsWith("ERROR")){
					Log.e(TAG, "request playerlist from Server On Error");
					Toast.makeText(getBaseContext(), "Cannot access server", Toast.LENGTH_SHORT).show();
					finish();
				}else if(result.isEmpty()){
					Log.e(TAG, "PlayerList is empty");
					Toast.makeText(getBaseContext(), "PlayerList is empty", Toast.LENGTH_SHORT).show();
					finish();
				}
				
				Log.d(TAG, "PlayerList is " + result.toString());
				result.remove(username);
				mAdapter = new ArrayAdapter<String>(Achievements.this,
						android.R.layout.simple_list_item_single_choice,
						result);
				setListAdapter(mAdapter);
				mUserList = result;
			}			
		});
		
		btnAchievementBack = (Button)findViewById(R.id.btn_achieve_persist_back);
		btnAchievementBack.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}});
	}
	
}
