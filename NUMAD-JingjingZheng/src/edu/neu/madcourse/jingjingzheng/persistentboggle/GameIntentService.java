package edu.neu.madcourse.jingjingzheng.persistentboggle;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public abstract class GameIntentService extends IntentService {

	public static final String WAKE_LOCK_SERVICE = "edu.neu.jingjingzheng.wakelock";
	private static WakeLock mWakeLock = null;
	
	abstract void doWakeupWork(Intent _intent);
	
	 public static void acquireStaticLock(Context context) {
		    getLock(context).acquire();
	 }
	
	synchronized private static PowerManager.WakeLock getLock(Context ctx){
		if(mWakeLock == null){
			PowerManager mgr = (PowerManager)ctx.getSystemService(Context.POWER_SERVICE);
			mWakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_SERVICE);
			mWakeLock.setReferenceCounted(true);
		}
		return mWakeLock;
	}
	
	public GameIntentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent arg0) {
		// TODO Auto-generated method stub
		try {
			doWakeupWork(arg0);
		}finally{
			getLock(this).release();
		}
	}

}
