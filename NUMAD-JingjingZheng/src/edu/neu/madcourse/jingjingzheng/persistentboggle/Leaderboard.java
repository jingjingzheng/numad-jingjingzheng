package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Leaderboard extends ListActivity{
	
	private Button btnLeaderboardBack;
	private WebServerAccess mWebServerAccess;
	private String TAG = "Persist_Leaderboard";
	
	private static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	private static final String PREF_USER = "prefUser";
	private ArrayList<String> mUserList = new ArrayList<String>();
	private ArrayAdapter<String> mAdapter;
	HighScores_Adaptor mHighScoresAdapter;
	private String username;
	
	public void setUsername(){
		    SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		    username = pref.getString(PREF_USER, null);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.persistent_scoreleaderboard);
		getListView().setEmptyView(findViewById(android.R.id.empty));
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
		setUsername();
		mWebServerAccess = new WebServerAccess();
		final Leaderboard thisActivity = this;
		
		mWebServerAccess.getSentRequests(username,
				new OnStringArrayListLoadedListener() {

			public void run(ArrayList<String> list) {
				mHighScoresAdapter = new HighScores_Adaptor(thisActivity, R.layout.persistent_scoreleaderboard, list);
				setListAdapter(mHighScoresAdapter);

			}
		});
		

		/*mWebServerAccess.getUserList(new OnStringArrayListLoadedListener(){

			@Override
			public void run(ArrayList<String> result) {
				// TODO Auto-generated method stub
				if(result.get(0).startsWith("ERROR")){
					Log.e(TAG, "request playerlist from Server On Error");
					Toast.makeText(getBaseContext(), "Cannot access server", Toast.LENGTH_SHORT).show();
					finish();
				}else if(result.isEmpty()){
					Log.e(TAG, "PlayerList is empty");
					Toast.makeText(getBaseContext(), "PlayerList is empty", Toast.LENGTH_SHORT).show();
					finish();
				}
				
				Log.d(TAG, "PlayerList is " + result.toString());
				result.remove(username);
				mAdapter = new ArrayAdapter<String>(Leaderboard.this,
						android.R.layout.simple_list_item_single_choice,
						result);
				setListAdapter(mAdapter);
				mUserList = result;
			}			
		});*/
		
		btnLeaderboardBack = (Button)findViewById(R.id.btn_leaderboard_persist_back);
		btnLeaderboardBack.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}});
	}
	
	public void onResume(){
		super.onResume();
		SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		username = pref.getString(PREF_USER, null);


		final Leaderboard thisActivity = this;

		mWebServerAccess.getSentRequests(username,
				new OnStringArrayListLoadedListener() {

			public void run(ArrayList<String> list) {
				mHighScoresAdapter = new HighScores_Adaptor(thisActivity, R.layout.persistent_scoreleaderboard, list);
				setListAdapter(mHighScoresAdapter);

			}
		});
	}
	
	public class HighScores_Adaptor extends BaseAdapter {
		
		private ArrayList<String> mHighScores = new ArrayList<String>();
		private Context mContext;
		private int rowResID;

		public HighScores_Adaptor(Context c,
				int rowResID, ArrayList<String> highscoresList) {
			// TODO Auto-generated constructor stub
			this.mContext = c;
			this.mHighScores = highscoresList;
			this.rowResID = rowResID;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mHighScores.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}
		
		public boolean isEmptyList(){
			return mHighScores.contains("");
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			boolean isEmpty = isEmptyList();

			if(arg1 == null ){
				getListView().setEmptyView(findViewById(android.R.id.empty));
				if(!isEmpty){
					LayoutInflater inflater = LayoutInflater.from(arg2.getContext());
					arg1 = inflater.inflate(R.layout.persistent_scoreleaderboard_rows, arg2, false);
				}else{
					LayoutInflater inflater = LayoutInflater.from(arg2.getContext());
					arg1 = inflater.inflate(R.layout.persistent_scoreleaderboard_rows_empty, arg2, false);
				}
			}

			TextView tvusername = (TextView) 
					arg1.findViewById(R.id.multiplayer_high_scores_textView_player);
			if(!isEmpty){
				tvusername.setText(mHighScores.get(arg0));
			}else{
				tvusername.setText("No New High Scores");
			}

			//TODO: change to actual scores
			final TextView score = (TextView) 
					arg1.findViewById(R.id.multiplayer_high_scores_textView_score);
			//score.setText(mWebServerAccess.getHighscore(mHighScores.get(arg0)));
			mWebServerAccess.getHighscoreofPlayer(mHighScores.get(arg0), new OnIntReceivedListener(){

				@Override
				public void run(int exitState) {
					// TODO Auto-generated method stub
					score.setText(String.valueOf(exitState));
				}
				
			});

			// Give it a nice background
			return arg1;
		}			

	}

	
}
