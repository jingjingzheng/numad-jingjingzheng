package edu.neu.madcourse.jingjingzheng.persistentboggle;


import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Gameover extends Activity implements OnClickListener{
	
	public static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	public static final String PREF_USER = "prefUser";
	private SharedPreferences mSharedPreferences;
	private WebServerAccess mWebServerAccess = new WebServerAccess();
	
	private TextView mpersonalScore;
	private TextView moppoScore;
	private ListView mwordlist;
	private Gameplay mgameplay;
	private String username;
	private String opponentname;
	private int serverside_player1;
	private int serverside_player2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		setContentView(R.layout.persistent_gameover);
		
		mgameplay = new Gameplay();
		username = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE).getString(PREF_USER, null);
		opponentname = Gameplay.opponentname;
		//serverside_player1 = mWebServerAccess.getHighscore(username);
		//serverside_player2 = mWebServerAccess.getHighscore(opponentname);
		
		mpersonalScore = (TextView)findViewById(R.id.tv_end_persist_yourscoreshow);
		moppoScore = (TextView)findViewById(R.id.tv_end_persist_opposcoreshow);
		//mpersonalScore.setText(String.valueOf(serverside_player1));
		//moppoScore.setText(String.valueOf(serverside_player2));
		//mpersonalScore.setText(String.valueOf(mgameplay.getCurrentScore()));
		updateUser1Score();
		updateUser2Score();
		
		mwordlist = (ListView)findViewById(R.id.lv_play_persist_wordlist);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, mgameplay.getFoundedWords());
		mwordlist.setAdapter(adapter);
		
		View menuGameover = findViewById(R.id.btn_end_persist_menu);
		//View againGameover = findViewById(R.id.btn_end_persist_newgame);
		menuGameover.setOnClickListener((OnClickListener)this);
		//againGameover.setOnClickListener((OnClickListener)this);	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btn_end_persist_menu:
			Intent _intent_menu = new Intent(Gameover.this, Boggle.class);
			_intent_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_menu);
			break;
		}
	}
	
	private void updateUser1Score(){
		
		final Gameover thisActivity = this;
		
		//int serverside_player1 = mWebServerAccess.getHighscore(username);
		//int serverside_player2 = mWebServerAccess.getHighscore(opponentname);
		mWebServerAccess.getHighscoreofPlayer(username, new OnIntReceivedListener(){

			@Override
			public void run(int exitState) {
				// TODO Auto-generated method stub
				thisActivity.serverside_player1= exitState;
				thisActivity.mpersonalScore.setText(String.valueOf(thisActivity.serverside_player1));
			}});
		
		/*if(serverside_player1 < this.mScorePlayer1){
			mWebServerAccess.setHighscore(username, this.mScorePlayer1);
		}*/

	}

	private void updateUser2Score(){
	
		final Gameover thisActivity = this;
	
		//int serverside_player1 = mWebServerAccess.getHighscore(username);
		//int serverside_player2 = mWebServerAccess.getHighscore(opponentname);
		mWebServerAccess.getHighscoreofPlayer(opponentname, new OnIntReceivedListener(){

			@Override
			public void run(int exitState) {
				// TODO Auto-generated method stub
				thisActivity.serverside_player2= exitState;
				thisActivity.moppoScore.setText(String.valueOf(thisActivity.serverside_player2));
			}});
	
		/*if(serverside_player1 < this.mScorePlayer1){
			mWebServerAccess.setHighscore(username, this.mScorePlayer1);
		}*/

	}
	

}
