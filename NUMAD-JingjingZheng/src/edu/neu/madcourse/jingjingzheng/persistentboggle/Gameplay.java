package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Gameplay extends Activity{
	
	private static final String TAG = "Persist_Gameplay";
	public static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	public static final String PREF_USER = "prefUser";
	private static final String PREF_SCORE = "GameplayScore";
	
	Timer timer;
	private static int current_time = 180; // 3 minutes timer
	private static int current_score = 0;
	private static ArrayList<String> savedWords;
	
	private SharedPreferences mSharedPreferences;
	private WebServerAccess mWebServerAccess = new WebServerAccess();
	private int mScorePlayer1;
	private int mScorePlayer2;
	private String username;
	public static String opponentname;
	private GameWrapper mGameWrapper;
	private GameService mGameService;

	
	public TextView tvMyScoreView;
	private TextView tvOppoScoreView;
	public TextView tvWordboardView;
	public TextView tvPlayer;
	public TextView tvOpponentPlayer;
	private TextView timerView;
	private Button btnEnd;
	private Button btnClear;
	private Button btnSubmit;
	private ListView lvWordlist;
	
	private AudioProvider mAudioProvider;
	private WordDatabase mWordDatabase;
	
	private String boardSet;
	public static char[] currentWordBoard;
	public static char[] wordboard;
	public static int characterIndex;
	public static int lastSelectX = 100;
	public static int lastSelectY = 100;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);		
		setContentView(R.layout.persistent_gameplay);
		
		username = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE).getString(PREF_USER, null);
		opponentname = getIntent().getStringExtra("opponent");
		
		sendBroadcast(new Intent(this, OnStartReceiver.class));
		
		//TODO sendBroadcast????
		
		this.mGameWrapper = mWebServerAccess.getGame(username, opponentname);
		
		this.boardSet = this.mGameWrapper.getBoard();
		Log.d(TAG, "onCreate Letterset: "+boardSet);
		/*if(boardSet.isEmpty()){
			boardSet = this.getWordBoard().toString();
			mWebServerAccess.updateBoard(username, opponentname, boardSet);//???
		}*/
		
		this.mScorePlayer1 = this.mGameWrapper.getUserScore(username);
		this.mScorePlayer2 = this.mGameWrapper.getUserScore(opponentname);
		
		
		currentWordBoard = new char[]{
				' ',' ',' ',' ',' ',
				' ',' ',' ',' ',' ',
				' ',' ',' ',' ',' ',
				' ',' ',' ',' ',' ',
				' ',' ',' ',' ',' ',};
		savedWords = new ArrayList<String>();
		savedWords.clear();
		current_time = 180;
		
		timerView = (TextView)findViewById(R.id.tv_play_persist_timeleftshow);
		tvMyScoreView = (TextView)findViewById(R.id.tv_play_persist_myscoreshow);
		tvOppoScoreView = (TextView)findViewById(R.id.tv_play_persist_opposcoreshow);
		tvWordboardView = (TextView)findViewById(R.id.tv_play__persist_wordboard);
		tvPlayer = (TextView)findViewById(R.id.tv_play_persist_userself);
		tvOpponentPlayer = (TextView)findViewById(R.id.tv_play_persist_oppo);
		btnEnd = (Button)findViewById(R.id.btn_play_persist_end);
		btnClear = (Button)findViewById(R.id.btn_play_persist_clear);
		btnSubmit = (Button)findViewById(R.id.btn_play_persist_submit);	
		lvWordlist = (ListView)findViewById(R.id.lv_play_persist_wordlist);
		
		mWordDatabase = new WordDatabase(this);
		mAudioProvider = new AudioProvider(this);
		
		tvPlayer.setText(username);
		tvOpponentPlayer.setText(opponentname);
	    
		final Handler mhandlerTime = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				
				if(msg.what > 0){
					int minutes = msg.what / 60;
					int seconds = msg.what % 60;
					Log.i("minutes", String.valueOf(minutes));
					Log.i("seconds", String.valueOf(minutes));

					if (seconds < 10) {
						timerView.setText("" + minutes + ":0" + seconds);
					} else {
						timerView.setText("" + minutes + ":" + seconds);            
					}
				}else{
                	timerView.setText("0");
                    timer.cancel();
                    mWebServerAccess.removeGame(username, opponentname);
                	Intent _intent_gameover = new Intent(Gameplay.this, Gameover.class);
                	_intent_gameover.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(_intent_gameover);
                }
			 }

		};
		
		TimerTask task = new TimerTask(){
			//int i = 180;
			public void run(){
				Message message = new Message();
				message.what = current_time--;
				
				mhandlerTime.sendMessage(message);
			}
		};
		timer = new Timer();
		timer.schedule(task, 1000, 1000);

    	btnEnd.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub			
				timer.cancel();
				mWebServerAccess.removeGame(username, opponentname);
				Intent _intent = new Intent(Gameplay.this, Gameover.class);
				_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(_intent);
			}});
    	
    	btnClear.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tvWordboardView.setText("");
				Gameplay.characterIndex = 0;
				Gameplay.lastSelectX = 100;
				Gameplay.lastSelectY = 100;
				for(int i = 0; i < 25; i++){
					currentWordBoard[i] = ' ';
				}
			}
    		
    	});
		
    	btnSubmit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					if((Gameplay.characterIndex >= 3) && mWordDatabase.getWordFromCache(String.copyValueOf(currentWordBoard), Gameplay.characterIndex) 
							&& !savedWords.contains(String.valueOf(currentWordBoard)) 
						){
					current_score += 10;
					Toast.makeText(Gameplay.this, "You got 10 credit", 1000).show();
					tvMyScoreView.setText(String.valueOf(current_score));
					updateUser2Score();
					updateUser1Score();
					
					mAudioProvider.playSounds(new int[] {AudioProvider.GOOD_WORD_SUBMITED});
					savedWords.add(String.valueOf(currentWordBoard));
					Log.d("savedWords", String.valueOf(savedWords));
					
				}else{
					tvMyScoreView.setText(String.valueOf(current_score).trim());		
					mAudioProvider.playSounds(new int[] {AudioProvider.BAD_WORD_SUBMITED});

				}
				tvWordboardView.setText(null);
				Gameplay.characterIndex = 0;
				Gameplay.lastSelectX = 100;
				Gameplay.lastSelectY = 100;
				for(int i = 0; i < 25; i++){
					currentWordBoard[i] = ' ';
				}
				Log.d("currentword", String.copyValueOf(currentWordBoard));

			}});
		
	}
	
	//update competitors' scores
	/*private void updateUserScore(){
		int serverScore
	}*/

	public char[] getWordBoard(){
		
		wordboard = boardSet.toCharArray(); 
		/*wordboard = new char[25];
			
		Random randomWord = new Random();
		
		for(int i = 0; i < 25; i++){
			wordboard[i] = (char)(65 + randomWord.nextInt(26));
		}*/
		return wordboard;
	}
	
	private void changeFormatOfTime(int time){
		int minutes = time / 60;
		int seconds = time % 60;

		if (seconds < 10) {
			timerView.setText("" + minutes + ":0" + seconds);
		} else {
			timerView.setText("" + minutes + ":" + seconds);            
		}
	}

	public AudioProvider getAudioProvider() {
		// TODO Auto-generated method stub
		return mAudioProvider;
	}
	
	public ListView getListView(){
		return 	lvWordlist;	
	}
	
	public ArrayList<String> getFoundedWords(){
		return savedWords;
	}
	
	private void updateUser2Score(){
		
		final Gameplay thisActivity = this;
		
		//int serverside_player1 = mWebServerAccess.getHighscore(username);
		//int serverside_player2 = mWebServerAccess.getHighscore(opponentname);
		mWebServerAccess.getHighscoreofPlayer(opponentname, new OnIntReceivedListener(){

			@Override
			public void run(int exitState) {
				// TODO Auto-generated method stub
				int serverside_player2 = exitState;
				if(thisActivity.mScorePlayer2 < serverside_player2){
					tvOppoScoreView.setText(String.valueOf(serverside_player2));
				}
			}});
		
		/*if(serverside_player1 < this.mScorePlayer1){
			mWebServerAccess.setHighscore(username, this.mScorePlayer1);
		}*/

	}
	
	private void updateUser1Score(){
		
		final Gameplay thisActivity = this;
		
		//int serverside_player1 = mWebServerAccess.getHighscore(username);
		//int serverside_player2 = mWebServerAccess.getHighscore(opponentname);
		mWebServerAccess.setHighscoreofPlayer(username, thisActivity.mScorePlayer1, new OnBooleanReceivedListener(){

			@Override
			public void run(Boolean exitState) {
				// TODO Auto-generated method stub
				Log.d(TAG, "Successfully set" + username + "'s score");
			}});
		
		/*if(serverside_player1 < this.mScorePlayer1){
			mWebServerAccess.setHighscore(username, this.mScorePlayer1);
		}*/

	}
	
	public String getOpponent(){
		return opponentname;
	}
	
	public void foo(){
		Intent i = new Intent(this, Gameplay.class);
		i.putExtra("username", this.username);
		i.putExtra("opponent", this.opponentname);
		i.putExtra("opponentCurrentScore", this.mScorePlayer2);

	}
	
	
}

