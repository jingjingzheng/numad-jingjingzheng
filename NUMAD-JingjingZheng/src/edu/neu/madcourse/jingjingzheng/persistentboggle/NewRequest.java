package edu.neu.madcourse.jingjingzheng.persistentboggle;

import java.util.ArrayList;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class NewRequest extends ListActivity {

	private String TAG = "Persist_NewRequest";
	private static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	private static final String PREF_USER = "prefUser";
	private WebServerAccess mWebServerAccess;
	
	private ArrayList<String> mUserList = new ArrayList<String>();
	private ArrayAdapter<String> mAdapter;
	
	private String username;
	private String opponame;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.persistent_new_request_list);
		getListView().setEmptyView(findViewById(android.R.id.empty));
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
				
		Log.d(TAG, "OnCreate");
		
		setUsername();
		mWebServerAccess = new WebServerAccess();
		mWebServerAccess.getUserList(new OnStringArrayListLoadedListener(){

			@Override
			public void run(ArrayList<String> result) {
				// TODO Auto-generated method stub
				if(result.get(0).startsWith("ERROR")){
					Log.e(TAG, "request playerlist from Server On Error");
					Toast.makeText(getBaseContext(), "Cannot access server", Toast.LENGTH_SHORT).show();
					finish();
				}else if(result.isEmpty()){
					Log.e(TAG, "PlayerList is empty");
					Toast.makeText(getBaseContext(), "PlayerList is empty", Toast.LENGTH_SHORT).show();
					finish();
				}
				
				Log.d(TAG, "PlayerList is " + result.toString());
				result.remove(username);
				mAdapter = new ArrayAdapter<String>(edu.neu.madcourse.jingjingzheng.persistentboggle.NewRequest.this,
						android.R.layout.simple_list_item_single_choice,
						result);
				setListAdapter(mAdapter);
				mUserList = result;
			}			
		});
		
		OnItemClickListener itemClickListener = new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				opponame = mUserList.get(arg2);
				Toast.makeText(getBaseContext(), "You selected : " + mUserList.get(arg2), Toast.LENGTH_SHORT).show();
			}			
		};
		
		//Set item click listener for listView
		getListView().setOnItemClickListener(itemClickListener);
	}
	
	  public void setUsername(){
		    SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
		    username = pref.getString(PREF_USER, null);
		  }
	
	public void onPersistRequestsOkButtonClicked(View v){
		Log.d(TAG, "ClickOnOk:" + opponame);
		if(!(opponame == null)){
			this.sendRequest(username, opponame);
			finish();
		}else{
			Toast.makeText(getBaseContext(), "Please select your competitor", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void onPersistRequestsCancelButtonClicked(View v){
		finish();
	}

	public void sendRequest(String username1, String opponame1) {
		// TODO Auto-generated method stub
		final NewRequest thisActivity = this;
		final String player1 = username1;
		final String player2 = opponame1;
		
		mWebServerAccess.sendRequest(player1, player2, new OnBooleanReceivedListener(){

			@Override
			public void run(Boolean exitState) {
				// TODO Auto-generated method stub
				if(!exitState){
					thisActivity.sendRequest(player1, player2, 2);
				}else{
					Log.d(TAG, "Request to send notification");
					Toast.makeText(getBaseContext(), "Request sent to " + player2, Toast.LENGTH_SHORT).show();
				}
			}
			
		});
	}

	protected void sendRequest(String username1, String opponame1, final int count) {
		// TODO Auto-generated method stub
		final NewRequest thisActivity = this;
		final String player1 = username1;
		final String player2 = opponame1;
		
		mWebServerAccess.sendRequest(player1, player2, new OnBooleanReceivedListener(){

			@Override
			public void run(Boolean exitState) {
				// TODO Auto-generated method stub
				if(!exitState && (count != 0)){
					Log.d(TAG, "trying for the " + (3 - count) + "th time");
					thisActivity.sendRequest(player1, player2, count-1);
				}else if(!exitState && (count == 0)){
					Log.e(TAG, "Couldn't send the request.");
					Log.d(TAG, "Sending error toast");
					Toast.makeText(getBaseContext(), "Sorry, could not send the request",Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getBaseContext(), "Request sent to " + player2, Toast.LENGTH_SHORT).show();
				}
			}});
	}


	
}
