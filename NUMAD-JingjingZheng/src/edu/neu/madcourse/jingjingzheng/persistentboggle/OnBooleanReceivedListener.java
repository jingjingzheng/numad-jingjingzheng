package edu.neu.madcourse.jingjingzheng.persistentboggle;

public interface OnBooleanReceivedListener {
	public void run(Boolean exitState);
}
