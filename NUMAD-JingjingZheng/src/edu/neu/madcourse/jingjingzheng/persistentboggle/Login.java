package edu.neu.madcourse.jingjingzheng.persistentboggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity{
	
	private String TAG = "Persistent_Login";
	private static final String BOGGLE_PREF = "edu.madcourse.jingjingzheng.persistentboggle";
	public static final String PREF_USER = "prefUser";
	public static final String PREF_PASS = "prefPass";
	
	private EditText etUsername;
	private EditText etPassword;
	private Button btnLogin;
	private Button btnRegister;
	private Button btnCancel;
	private WebServerAccess webaccess;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/** Set titlebar*/
		this.setTitle("Boggle");
		
		/**Add small icon on titlebar*/
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.persistent_login);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
		Log.d(TAG, "Login Form!!!");

		etUsername = (EditText)findViewById(R.id.et_login_persist_username);
		etPassword = (EditText)findViewById(R.id.et_login__persist_password);
		btnLogin = (Button)findViewById(R.id.btn_login_persist_Login);
		btnCancel = (Button)findViewById(R.id.btn_login_persist_Cancel);
		btnRegister = (Button)findViewById(R.id.btn_login_persist_Register);
		btnRegister.setVisibility(Button.GONE);
		webaccess = new WebServerAccess();


		// Set Click Listener
		btnLogin.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// Check Login
				final String username = etUsername.getText().toString();
				final String password = etPassword.getText().toString();

				webaccess.login(username, password, new OnBooleanReceivedListener() {
					public void run(Boolean loggedIn) {
						if (loggedIn) {
							Toast.makeText(getBaseContext(), "Login Sucessful. Welcome " + username, Toast.LENGTH_SHORT).show();
						
							//TODO: Should this be BOGGLE_PREF or MULTI_PREF?
							SharedPreferences pref = getSharedPreferences(BOGGLE_PREF, MODE_PRIVATE);
							SharedPreferences.Editor edit = pref.edit();

							edit.putString(PREF_USER, username).commit();
							edit.putString(PREF_PASS, password).commit();

							Log.d(TAG, "Login: "+ pref.getString(PREF_USER, null));
							Log.d(TAG, "Login: " + pref.getString(PREF_PASS, null));
							finish();
						}else{
							Toast.makeText(getBaseContext(), "Login Failed. Username and/or password doesn't match.", Toast.LENGTH_SHORT).show();
							btnRegister.setVisibility(Button.VISIBLE);
						}
					}
				});
			}
		});

		btnRegister.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// Check Login
				final String username = etUsername.getText().toString();
				final String password = etPassword.getText().toString();
				webaccess.register(username, password, new OnBooleanReceivedListener() {
					public void run(Boolean registered) {
						if (registered) {
							Toast.makeText(getBaseContext(), "Registration Sucessful. " +
									"Welcome " + username, Toast.LENGTH_SHORT).show();
							etUsername.setText(username);
							etPassword.setText(password);
						}else{
							Toast.makeText(getBaseContext(), 
									"Registration Failed. Either the username " +
											"already exists, or your username or password " +
											"contained commas", Toast.LENGTH_SHORT).show();
						}
					}
				});
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// Close the application
				finish();
			}
		});
	}
	
}
