package edu.neu.madcourse.jingjingzheng;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import edu.neu.madcourse.jingjingzheng.trickiestpart.FinalDescription;
import edu.neu.madcourse.jingjingzheng.trickiestpart.PhotoDishes;
import edu.neu.mobileClass.*;

public class MainActivity extends Activity {

	@Override
	/** Called when the activity is first created*/
	protected void onCreate(Bundle savedInstanceState) {		
		
		super.onCreate(savedInstanceState);
		
		/** Set titlebar with my name*/
		this.setTitle("Jingjing Zheng");
		
		/**Do authorization to ensure that 
		 * your app will only run on specific phones
		 * which we authorize*/
		
		//PhoneCheckAPI.doAuthorization(this);
		/**Add small icon on titlebar*/
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.activity_first);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
		/**Click on "Team Members" button, display your name 
		 * and email
		 * and a headshot of you*/
		Button btn_meminfo = (Button)findViewById(R.id.btnteam);
		btn_meminfo.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				/**Build one dialog to show your info*/
				final Dialog dialog = new Dialog(MainActivity.this);
				dialog.setContentView(R.layout.dialog_teaminfo);
				dialog.setTitle("This is my basic information");
				dialog.setCancelable(true);
				
				//set up image view
				ImageView img_dial = (ImageView)dialog.findViewById(R.id.imgviw_dialog);
				img_dial.setImageResource(R.drawable.me);
				
				//set up text
				TextView txt_dial = (TextView)dialog.findViewById(R.id.txtview_dialog);
				txt_dial.setText(R.string.lots_of_text);
				
				//set up button
				Button btn_dial = (Button)dialog.findViewById(R.id.btn_dialog);
				btn_dial.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
						//finish();
					}});
				dialog.show();
			}});
		
		
		/**Add "Sudoku" button, click on it, then start Sudoku game*/
		Button btn_sudoku = (Button)findViewById(R.id.btnsudoku);
		btn_sudoku.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/**create an Intent instance, start main_screen of Sudoku app*/
				Intent _intent = new Intent(Intent.ACTION_MAIN);
				_intent.setComponent(new ComponentName("edu.neu.madcourse.jingjingzheng", "edu.neu.madcourse.jingjingzheng.sudoku.Sudoku"));
				startActivity(_intent);
			}});
		
		/**Add "Boggle" button, click on it, then start Boggle game*/
		Button btn_boggle = (Button)findViewById(R.id.btnboggle);
		btn_boggle.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent _intent = new Intent(MainActivity.this, edu.neu.madcourse.jingjingzheng.boggle.Boggle.class);
				startActivity(_intent);
			}});
		
		/**Add "Persistent_Boggle" button, click on it, then start PersistentBoggle game*/
		Button btn_persistent = (Button)findViewById(R.id.btnpersistentboggle);
		btn_persistent.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent _intent = new Intent(MainActivity.this, edu.neu.madcourse.jingjingzheng.persistentboggle.Boggle.class);
				startActivity(_intent);
			}});
		
		/**
		 * Add "Trickiest Part" button, click on it, then start
		 */
		Button btn_trickiestpart = (Button)findViewById(R.id.btnTrickypart);
		btn_trickiestpart.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

//				PackageManager pm = getPackageManager();
//				Intent _intent = pm.getLaunchIntentForPackage("edu.neu.madcourse.shanshanhuang");
				
				Intent _intent = new Intent(MainActivity.this,PhotoDishes.class);
				startActivity(_intent);
			}});
		
		/**
		 * Add "Final Project" button, click on it, then start
		 */
		Button btn_finalproj = (Button)findViewById(R.id.btnFinalproj);
		btn_finalproj.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				//PackageManager pm = getPackageManager();
				//Intent _intent = pm.getLaunchIntentForPackage("edu.neu.madcourse.shanshanhuang");
	        	Intent finalproject = new Intent(MainActivity.this,FinalDescription.class);
	        	startActivity(finalproject);
				//startActivity(_intent);
			}});
		
		
		/**Add "Create error" button, click on it, then cause
		 * an error in the code to crash your program*/
		Button btn_err = (Button)findViewById(R.id.btnerr);
		btn_err.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//TextView tv;
				try {
					//tv.setText("1");
					Button btn_err1 = null;
					btn_err1.setText("2");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.d("daisylog", e.getMessage());
				}
			}
			
		});
		
		/**
		 * Add "Exit" button, click on it, then exit from main screen of 
		 * this app
		 */
		Button btn_mainexit = (Button)findViewById(R.id.btnhomexit);
		btn_mainexit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_first, menu);
		return true;
	}

}
