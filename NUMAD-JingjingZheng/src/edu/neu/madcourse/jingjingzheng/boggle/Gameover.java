package edu.neu.madcourse.jingjingzheng.boggle;


import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Gameover extends Activity implements OnClickListener{
	
	private TextView mpersonalScore;
	private ListView mwordlist;
	private Gameplay mgameplay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		setContentView(R.layout.wordlist_boggle);
		
		mgameplay = new Gameplay();
		
		mpersonalScore = (TextView)findViewById(R.id.gamescoreupdate_end_boggle);
		mpersonalScore.setText(String.valueOf(mgameplay.getCurrentScore()));
		
		mwordlist = (ListView)findViewById(R.id.WordBank);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, mgameplay.getFoundedWords());
		mwordlist.setAdapter(adapter);
		
		View menuGameover = findViewById(R.id.btnmenu_end_boggle);
		View againGameover = findViewById(R.id.btnagain_end_boggle);
		menuGameover.setOnClickListener((OnClickListener)this);
		againGameover.setOnClickListener((OnClickListener)this);	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btnmenu_end_boggle:
			Intent _intent_menu = new Intent(Gameover.this, Boggle.class);
			_intent_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_menu);
			break;
		case R.id.btnagain_end_boggle:
			Intent _intent_again = new Intent(Gameover.this, Gameplay.class);
			_intent_again.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_again);
			break;
		}
	}
	

}
