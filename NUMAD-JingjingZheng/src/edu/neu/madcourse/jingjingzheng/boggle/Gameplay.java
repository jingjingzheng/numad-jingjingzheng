package edu.neu.madcourse.jingjingzheng.boggle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Gameplay extends Activity{
	
	Timer timer;
	private TimerTask task;
	private static int current_score = 0;
	private static int pause_current_score;

	private static int current_time = 180; //3 minutes
	public TextView scoreView;
	public TextView scoreboardView;
	private Button pausePlay;
	private Button endPlay;
	private Button clearPlay;
	public char[] wordboard;
	private AudioProvider mAudioProvider;
	private WordDatabase mWordDatabase;
	private BoardDraw mboardDraw;
	public static char[] currentWord;
	private static ArrayList<String> mFoundWords;
	private Boggle mMainBoggle;
	//private Gameover_Boggle mgameoverBoggle;
	
	public static final String KEY_CONTINUE = "edu.neu.madcourse.jingjingzheng";
	public static final int NEW_GAME = 1;
	public static final int CONTINUE_GAME = -1;
	private static final String PREF_PUZZLE = "puzzle" ;
	private static final String PREF_SCORE = "score";
	private static final String PREF_SAVEDWORDS = "savedwords";
	private static final String PREF_TIME = "time";
	private static ArrayList<String> savedWords; 
	boolean[] puzzleClicked;
	int score = 0;
	public static int diff;
	public TextView timerView;
	private static boolean isActive = true;
	private static String TAG = "Gameplay";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);	
		diff = this.getIntent().getIntExtra(Gameplay.KEY_CONTINUE, NEW_GAME);
		Log.i("hhhh", String.valueOf(diff));
		Log.d(TAG, String.valueOf(isActive)+"haha1");
		getWordBoard(diff);    
	    getPuzzleClicked();
		
		setContentView(R.layout.gameplay_boggle);
		
		currentWord = new char[]{
				' ',' ',' ',' ',
				' ',' ',' ',' ',
				' ',' ',' ',' ',
				' ',' ',' ',' ',};
		
		savedWords = new ArrayList<String>();
		timerView = (TextView)findViewById(R.id.time_play_boggle);
		scoreView = (TextView)findViewById(R.id.score_play_boggle);
		pausePlay = (Button)findViewById(R.id.pause_play_boggle);
		endPlay = (Button)findViewById(R.id.end_play_boggle);
		clearPlay = (Button)findViewById(R.id.clear_play_boggle);
		scoreboardView = (TextView)findViewById(R.id.scoreboard_play_boggle);
    	mWordDatabase = new WordDatabase(this);
    	mboardDraw = new BoardDraw(this);
    	mAudioProvider = new AudioProvider(this);
    	mMainBoggle = new Boggle();
		
	    initSavedWords(diff);
	    initTime(diff);
	    initScore (diff);
	    Log.d(TAG, String.valueOf(isActive)+"haha4");
	    
		final Handler mhandlerTime = new Handler(){
			@Override
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				
				if(msg.what > 0){
					int minutes = msg.what / 60;
					int seconds = msg.what % 60;
					Log.i("minutes", String.valueOf(minutes));
					Log.i("seconds", String.valueOf(minutes));

					if (seconds < 10) {
						timerView.setText("" + minutes + ":0" + seconds);
					} else {
						timerView.setText("" + minutes + ":" + seconds);            
					}
				}else{
                	timerView.setText("0");
                    timer.cancel();
    				pause_current_score = current_score;
                	Intent _intent_gameover = new Intent(Gameplay.this, Gameover.class);
                	_intent_gameover.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(_intent_gameover);
                }
			 }

		};
		
		task = new TimerTask(){
			//int i = 180;
			public void run(){
				Message message = new Message();
				message.what = current_time--;
				
				mhandlerTime.sendMessage(message);
			}
		};
		timer = new Timer();
		timer.schedule(task, 1000, 1000);

    	pausePlay.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				timer.cancel();
				pause_current_score = current_score;
				Intent _intent = new Intent(Gameplay.this, Paused.class);
				_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(_intent);
			}});
    	
    	clearPlay.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				scoreboardView.setText("");
				BoardDraw.index = 0;
				BoardDraw.lastSelectX = 100;
				BoardDraw.lastSelectY = 100;
				for(int i = 0; i < 16; i++){
					currentWord[i] = ' ';
				}
			}
    		
    	});
		
    	endPlay.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					if((BoardDraw.index >= 3) && mWordDatabase.getWordFromCache(String.copyValueOf(currentWord), BoardDraw.index) 
							&& !savedWords.contains(String.valueOf(currentWord)) 
						){
					current_score += 10;
					Toast.makeText(Gameplay.this, "You got 10 credit", 1000).show();
					scoreView.setText(String.valueOf(current_score));
					
					mAudioProvider.playSounds(new int[] {AudioProvider.GOOD_WORD_SUBMITED});
					savedWords.add(String.valueOf(currentWord));
					Log.d("savedWords", String.valueOf(savedWords));
					
				}else{
					scoreView.setText(String.valueOf(current_score).trim());		
					mAudioProvider.playSounds(new int[] {AudioProvider.BAD_WORD_SUBMITED});

				}
				scoreboardView.setText(null);
				BoardDraw.index = 0;
				BoardDraw.lastSelectX = 100;
				BoardDraw.lastSelectY = 100;
				for(int i = 0; i < 16; i++){
					currentWord[i] = ' ';
				}
				Log.d("currentword", String.copyValueOf(currentWord));

			}});
		
	}

	public char[] getWordBoard(int diff){
		
		wordboard = new char[16];
			
		Random randomWord = new Random();
		
		for(int i = 0; i < 16; i++){
			wordboard[i] = (char)(65 + randomWord.nextInt(26));
		}
		if(diff == -1 || isActive == false){
			String defaultRandom = new String(wordboard);
			String puz = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE,
					defaultRandom);
			wordboard = puz.toCharArray();
		}

		
		return wordboard;
	}
	
	private void getPuzzleClicked() {
		puzzleClicked = new boolean[16];
		 

		  for ( int i = 0; i < 16; i++ ){
		    puzzleClicked[i] = false;
		  }

		}
	
	public void initScore (int diff){
		current_score = 0;
		if(diff == -1 || isActive == false){
			current_score = getPreferences(MODE_PRIVATE).getInt(PREF_SCORE, 0);
		}
		scoreView.setText(String.valueOf(current_score));
	}
	
	public void initSavedWords(int diff){
		savedWords.clear();
		if(diff == -1 || isActive == false){
			String pu = getPreferences(MODE_PRIVATE).getString(PREF_SAVEDWORDS, "");
			String[] ss = pu.split(",");
			for(int i = 0; i < ss.length; i++){
				savedWords.add(ss[i]);
			}
		}
	}
	
	public void initTime(int diff){
		current_time = 180;
		if(diff == -1 || isActive == false){
			current_time = getPreferences(MODE_PRIVATE).getInt(PREF_TIME, 1);
		}
		changeFormatOfTime(current_time);

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		this.mAudioProvider.destroy();
		this.mAudioProvider = null;
		
		
	    String puzzleString = new String(wordboard);
	    getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE, puzzleString).commit();
	    getPreferences(MODE_PRIVATE).edit().putInt(PREF_SCORE, current_score).commit();
	    getPreferences(MODE_PRIVATE).edit().putInt(PREF_TIME, current_time).commit();
	    
	      
	    String[] record = new String[savedWords.size()];
	    record = savedWords.toArray(record);
	      
	    String s = "";
	    s = join(record, ",");

	    getPreferences(MODE_PRIVATE).edit().putString(PREF_SAVEDWORDS, s).commit();
	}

	public <T> String join(T[] array, String cement) {
	    StringBuilder builder = new StringBuilder();

	    if(array == null || array.length == 0) {
	        return null;
	    }

	    for (T t : array) {
	        builder.append(t).append(cement);
	    }

	    builder.delete(builder.length() - cement.length(), builder.length());

	    return builder.toString();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!isActive) {
			isActive = true;
		}
		Log.d(TAG, String.valueOf(isActive)+"haha2");
		mAudioProvider = new AudioProvider(this);
	}
	
	
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(!isAppOnForeground()){
			isActive = false;
		}
		Log.d(TAG, String.valueOf(isActive)+"haha3");
		/*timer.cancel();
	    String puzzleString = new String(wordboard);
	    getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE, puzzleString).commit();
	    getPreferences(MODE_PRIVATE).edit().putInt(PREF_SCORE, current_score).commit();
	    getPreferences(MODE_PRIVATE).edit().putInt(PREF_TIME, current_time).commit();
	    
	      
	    String[] record = new String[savedWords.size()];
	    record = savedWords.toArray(record);
	      
	    String s = "";
	    s = join(record, ",");

	    getPreferences(MODE_PRIVATE).edit().putString(PREF_SAVEDWORDS, s).commit();*/
	}
	
	
	
	//@Override
	/*protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		timer = new Timer();
		timer.schedule(task, 1000, 1000);
		diff = -1;
	}*/

	public boolean isAppOnForeground() {
        // Returns a list of application processes that are running on the
        // device
         
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        Log.d(TAG, packageName +"packageName");
        
        List<RunningAppProcessInfo> appProcesses = activityManager
                        .getRunningAppProcesses();
        if (appProcesses == null)
                return false;

        for (RunningAppProcessInfo appProcess : appProcesses) {
                // The name of the process that this object is associated with.
                if (appProcess.processName.equals(packageName)
                                && appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        return true;
                }
        }

        return false;
}
	
	

	public AudioProvider getAudioProvider() {
		return mAudioProvider;
	}
	
	private void changeFormatOfTime(int time){
		int minutes = time / 60;
		int seconds = time % 60;

		if (seconds < 10) {
			timerView.setText("" + minutes + ":0" + seconds);
		} else {
			timerView.setText("" + minutes + ":" + seconds);            
		}
	}

	
	public int getCurrentTime(){
		return current_score; 
	}

	public ArrayList<String> getFoundedWords(){
		return savedWords;
	}
	
	public int getCurrentScore(){
		return pause_current_score;
	}
	
	public void updateCurrentScore(){
		pause_current_score = 0;
	}
	
	
}

