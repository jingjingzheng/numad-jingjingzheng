package edu.neu.madcourse.jingjingzheng.boggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;

public class Paused extends Activity implements OnClickListener{


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		super.setContentView(R.layout.pause_boggle);
		
	    View btnpausemenuBoggle = findViewById(R.id.btn_pausemenu_boggle);
	    btnpausemenuBoggle.setOnClickListener(this);
	    View btnpausettingBoggle = findViewById(R.id.btn_pausetting_boggle);
	    btnpausettingBoggle.setOnClickListener(this);
	    View btnpausendgBoggle = findViewById(R.id.btn_pausend_boggle);
	    btnpausendgBoggle.setOnClickListener(this);
	    View btnpauserestartBoggle = findViewById(R.id.btn_pauserestart_boggle);
	    btnpauserestartBoggle.setOnClickListener(this);
	    View btnpauseresumeBoggle = findViewById(R.id.btn_pauseresume_boggle);
	    btnpauseresumeBoggle.setOnClickListener(this);
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_pausemenu_boggle:
			Intent _intent_menu = new Intent(Paused.this, Boggle.class);
			_intent_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_menu);
		    break;
		case R.id.btn_pauserestart_boggle:
			Intent _intent_restart = new Intent(Paused.this, Gameplay.class);
			_intent_restart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_restart);
			break;
		case R.id.btn_pausetting_boggle:
			Intent _intent_set = new Intent(Paused.this, Settings.class);
			_intent_set.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_set);
			break;
		case R.id.btn_pauseresume_boggle:
			Intent _intent_resume = new Intent(Paused.this, Gameplay.class);
			_intent_resume.putExtra(Gameplay.KEY_CONTINUE, -1);
			_intent_resume.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_resume);		
			break;
		case R.id.btn_pausend_boggle:
			Intent _intent_end = new Intent(Paused.this, Gameover.class);
			_intent_end.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_end);
			break;
		}
	}
	
}
