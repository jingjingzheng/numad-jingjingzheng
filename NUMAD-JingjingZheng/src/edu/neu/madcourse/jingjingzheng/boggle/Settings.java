package edu.neu.madcourse.jingjingzheng.boggle;

import java.io.IOException;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

public class Settings extends Activity{

	ToggleButton toggleBtn = null;
	MediaPlayer media = null;
	AudioManager audioManager = null;
	SeekBar seekBar = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.settings_boggle);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);	
		
		this.audioManager = (AudioManager) super.getSystemService(Context.AUDIO_SERVICE);
		this.toggleBtn = (ToggleButton)super.findViewById(R.id.toggleBtn_boggle);
		this.toggleBtn.setOnClickListener(new PlayOnClickListener());
		this.seekBar = (SeekBar) super.findViewById(R.id.seekBar_boggle);
		
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        arg1, AudioManager.FLAG_PLAY_SOUND);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}});
		
		//back button for returning to boggle main screen
		Button btnSetBack = (Button)findViewById(R.id.btn_setback_boggle);
		btnSetBack.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}});	
	}
	
	private class PlayOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//if(toggleBtn.getText().toString().compareTo("Turn On") == 0){
			if(toggleBtn.isChecked()){
				Settings.this.media = MediaPlayer.create(Settings.this, R.raw.game_boggle);
				if(Settings.this.media != null){
					Settings.this.media.stop();
				}
				/*Settings_Boggle.this.media.setOnCompletionListener(new OnCompletionListener(){

					@Override
					public void onCompletion(MediaPlayer arg0) {
						// TODO Auto-generated method stub
						Settings_Boggle.this.media.release();
					}});*/
				try {
					Settings.this.media.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Settings.this.media.start();
			}else{
				Settings.this.media.stop();
				Settings.this.media.release();
			}
		}
		
	}
	

}
