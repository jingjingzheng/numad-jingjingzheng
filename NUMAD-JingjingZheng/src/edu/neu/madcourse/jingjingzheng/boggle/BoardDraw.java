package edu.neu.madcourse.jingjingzheng.boggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;

public class BoardDraw extends SurfaceView implements Callback, Runnable{
	
	private SurfaceHolder sfh;
	private Thread mthread;
	private Paint paint;
	private Canvas canvas;
	private Rect rect;
	private Gameplay gameplayBoggle; 	
	private int selectX;
	private int selectY;
	public static int lastSelectX = 100;
	public static int lastSelectY = 100;
	public static int index = 0;	
	private char[] wordboard;
	private boolean isRun;
	
	public BoardDraw(Context context){
		super(context);
		//this.gameplayBoggle = (Gameplay)context;
		mthread = new Thread(this);
		sfh = this.getHolder();
		sfh.addCallback(this);
		paint = new Paint();
		rect = new Rect();
		setFocusable(true);
		setFocusableInTouchMode(true);
	}

	public BoardDraw(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.gameplayBoggle = (Gameplay)context;
		//mthread = new Thread(this);
		sfh = this.getHolder();
		sfh.addCallback(this);
		paint = new Paint();
		rect = new Rect();
		setFocusable(true);
		setFocusableInTouchMode(true);

		// TODO Auto-generated constructor stub
	}
	
	
	private void draw() {
		// TODO Auto-generated method stub
		//super.onDraw(canvas);
		try{
		canvas = sfh.lockCanvas();
		canvas.drawColor(getResources().getColor(R.color.background_boggle));
		
	    paint.setColor(getResources().getColor(R.color.gameboardline_boggle)); 
	    paint.setStrokeWidth(6);
		
	    canvas.drawLine(0, 0, getWidth(), 0, paint);
	    canvas.drawLine(0, getHeight()/4, getWidth(), getHeight()/4, paint);
	    canvas.drawLine(0, getHeight()/4*2, getWidth(), getHeight()/4*2, paint);
	    canvas.drawLine(0, getHeight()/4*3, getWidth(), getHeight()/4*3, paint);
	    canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);
	      
	    canvas.drawLine(0, 0, 0, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/4, 0, getWidth()/4, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/4*2, 0, getWidth()/4*2, getHeight(), paint);	      
	    canvas.drawLine(getWidth()/4*3, 0, getWidth()/4*3, getHeight(), paint);	      
	    canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), paint);	      
		
		paint.setColor(getResources().getColor(R.color.puzzle_word_boggle));
		paint.setTextSize(getHeight()/4*0.75f);
		//mLetterPaint.setColor(Color.BLACK);
		paint.setAntiAlias(true);			
		paint.setTextAlign(Align.CENTER);
		paint.setTypeface(Typeface.DEFAULT_BOLD);	

		//this.gameplayBoggle = new Gameplay_Boggle();
		wordboard = new char[16];
		int diff = Gameplay.diff;
		wordboard = gameplayBoggle.getWordBoard(diff);
	    // Draw the number in the center of the tile
	    FontMetrics fm = paint.getFontMetrics();
	    // Centering in X: use alignment (and X at midpoint)
	    float x = getWidth() / 8;
	      // Centering in Y: measure ascent/descent first
	    float y = getHeight() / 8 - (fm.ascent + fm.descent) / 2;
	    //float y = getHeight() / 8;

	    for (int i = 0; i < 4; i++) {
	         for (int j = 0; j < 4; j++) {
	            canvas.drawText(String.valueOf(this.wordboard[j*4 + i]), 
	            		x + i * getWidth()/4, y + j * getHeight()/4, paint);
	         }
		
	    }
		}catch(Exception ex){
			ex.printStackTrace(); 
		}finally{
			if(canvas != null)
				sfh.unlockCanvasAndPost(canvas);
		}
	    
	    //paint.setColor(getResources().getColor(R.color.puzzle_light));
	    //canvas.drawRect(rect, paint);
	    //invalidate();
	}

	//@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		int action = event.getAction();
		if(action != MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);

		//currentWord = new char[16];
	      if(wrongArea(Math.min(Math.max((int) (event.getX()/(getWidth()/4)), 0), 3), Math.min(Math.max((int)(event.getY()/(getHeight()/4)), 0), 3))){
	    	  return super.onTouchEvent(event);
	      }
		
		gameplayBoggle.getAudioProvider().playSounds(new int[] {AudioProvider.LETTER_CLICKED});

		Gameplay.currentWord[index++] = this.gameplayBoggle.wordboard[(int)(event.getY()/(getHeight()/4)) * 4 + (int)(event.getX()/(getWidth()/4))];
		//int endTextLength = index - 1;
		Log.i("boarddraw", String.copyValueOf(Gameplay.currentWord));
		this.gameplayBoggle.scoreboardView.setText(Gameplay.currentWord, 0, index);

		lastSelectX = (int)(event.getX()/(getWidth()/4));
		lastSelectY = (int)(event.getY()/(getHeight()/4));
		//select(lastSelectX, lastSelectY);
		
		return true;	
	}
		
	//Disable words area which does not adjacent to current selected words
	private boolean wrongArea(int x, int y){
		if(lastSelectX == 100){
			return false;
		}
		if(x > lastSelectX + 1 || x < BoardDraw.lastSelectX - 1 || y > lastSelectY + 1 || y < lastSelectY -1){
			return true;
		}
		return false;
	 }
	   
	 private void select(int x, int y){
		 selectX = Math.min(Math.max(x, 0), 3);
		 selectY = Math.min(Math.max(y, 0), 3);
		 getRect(selectX, selectY, rect);
		 invalidate(rect);
	 }
	 
	 private void getRect(int x, int y, Rect rect){
		 rect.set(x, y, x + (int)(getWidth()/4), y + (int)(getHeight()/4));
	 }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		 if(isRun) { 
			draw(); 
			try { 
				Thread.sleep(100); 
			} catch (InterruptedException e) { 
				// TODO Auto-generated catch block 
				e.printStackTrace(); 
			} 
		} 
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		mthread = new Thread(this);
		isRun = true;
		mthread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		isRun = false;
		mthread = null;
	}
	
	
}
