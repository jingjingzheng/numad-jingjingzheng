package edu.neu.madcourse.jingjingzheng.boggle;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class Boggle extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/** Set titlebar with my name*/
		this.setTitle("Boggle");
		
		/**Add small icon on titlebar*/
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.launcher_boggle);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		
	    // Set up click listeners for all the buttons

	    View btnNewgameBoggle = findViewById(R.id.btn_newgame_boggle);
	    btnNewgameBoggle.setOnClickListener((OnClickListener)this);
	    View btnResumeBoggle = findViewById(R.id.btn_resume_boggle);
	    btnResumeBoggle.setOnClickListener((OnClickListener)this);
	    View btnSettingBoggle = findViewById(R.id.btn_setting_boggle);
	    btnSettingBoggle.setOnClickListener((OnClickListener)this);	    
	    View btnAcknowledgeBoggle = findViewById(R.id.btn_acknowledge_boggle);
	    btnAcknowledgeBoggle.setOnClickListener((OnClickListener)this);
	    View btnExitBoggle = findViewById(R.id.btn_exit_boggle);
	    btnExitBoggle.setOnClickListener((OnClickListener)this);
	    View btnRulesBoggle = findViewById(R.id.btn_rules_boggle);
	    btnRulesBoggle.setOnClickListener((OnClickListener)this);

	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_newgame_boggle:
			Intent _intent_newgame = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.boggle.Gameplay.class);
			_intent_newgame.putExtra(Gameplay.KEY_CONTINUE, 1);
			_intent_newgame.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_newgame);
			break;
		case R.id.btn_resume_boggle:
			Intent _intent_resume = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.boggle.Gameplay.class);
			_intent_resume.putExtra(Gameplay.KEY_CONTINUE, -1);
			_intent_resume.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_resume);			
			break;
		case R.id.btn_setting_boggle:
			Intent _intent_set = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.boggle.Settings.class);
			_intent_set.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_set);
		    break;
		case R.id.btn_acknowledge_boggle:
			Intent _intent_ack = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.boggle.Acknowledge.class);
			_intent_ack.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_ack);
		    break;
		case R.id.btn_rules_boggle:
			Intent _intent_rules = new Intent(Boggle.this, edu.neu.madcourse.jingjingzheng.boggle.Rules.class);
			_intent_rules.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(_intent_rules);
		    break;
		// More buttons go here (if any) ...
		case R.id.btn_exit_boggle:
		    Boggle.this.finish();
		    System.exit(0);
		    break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    super.onCreateOptionsMenu(menu);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}

}
