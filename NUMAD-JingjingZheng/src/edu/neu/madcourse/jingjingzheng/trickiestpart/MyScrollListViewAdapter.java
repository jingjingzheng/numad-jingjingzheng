package edu.neu.madcourse.jingjingzheng.trickiestpart;

import edu.neu.madcourse.jingjingzheng.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyScrollListViewAdapter extends ArrayAdapter<String>{
	private final Context context;
	private final String[] values;

	public MyScrollListViewAdapter(Context context,
			String[] values) {
		super(context, R.layout.trickiestpart_today, values);
		this.context = context;
		this.values = values;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.trickiestpart_today, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.horzscroll_list_label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.horzscroll_list_icon);
		textView.setText(values[position]);
 
		// Change icon based on name
		String s = values[position];
 
		System.out.println(s);
 
		if (s.equals("Breakfast")) {
			imageView.setImageResource(R.drawable.logo);
		} else if (s.equals("Snack")) {
			imageView.setImageResource(R.drawable.logo);
		} else if (s.equals("Lunch")) {
			imageView.setImageResource(R.drawable.logo);
		} else if (s.equals("Dinner")){
			imageView.setImageResource(R.drawable.logo);
		} else if (s.equals("Today")){
			imageView.setImageResource(R.drawable.logo);
		} else if (s.equals("History")) {
			imageView.setImageResource(R.drawable.logo);
		} else {
			imageView.setImageResource(R.drawable.logo);
		}
 
		return rowView;
	}

}
