package edu.neu.madcourse.jingjingzheng.trickiestpart;

///////////////////added code

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.LinearLayout.LayoutParams;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.R.layout;
import edu.neu.madcourse.jingjingzheng.trickiestpart.TabViewFragment.PhotoListAdapter;
import edu.neu.mobileclass.apis.FileUtils;


public class DishMatchList extends Activity{
	
    /** Called when the activity is first created. */
    ImageView imageView;
    Mat img;
    Mat curimg;
    private ImageComparator c;
    private static final String TAG = "LEARNOPENCV";
    public static final String MEAL_TYPE="meal_type";
    ArrayList<Bitmap> myPhotos = new ArrayList<Bitmap>();
    ArrayList<Object> photos = new ArrayList<Object>();
    private HashMap<String,String> mImageLabelingMap;
    private ArrayList<String> orderedTopMatchFileName;
    File[] listFile;
    final DishMatchList thisActivity = this;
	private static final String MEAL_PREF = "edu.madcourse.shanshanhuang.trickiestpart.mealtime";
	private String mTitle;
	public SharedPreferences mMealTimeSF;
	private static String title;
    private List<Map<String, Object>> imageHisPair = new ArrayList<Map<String, Object>>();
    private HashMap<String, Object> mMap = new HashMap<String, Object>();
    private Button skipButton;
    private String timeStamp;

    
	@Override
    public void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        Log.d(TAG, "hah1");
        
        setContentView(R.layout.trickiestpart_dish_match);
        mImageLabelingMap = new HashMap<String,String>();
        if(Global.getMapObjects()!=null){
        	mImageLabelingMap = Global.getMapObjects();
        }
        orderedTopMatchFileName = new ArrayList<String>();
        mMealTimeSF = getSharedPreferences(MEAL_PREF, MODE_PRIVATE);
        mTitle = mMealTimeSF.getString("Cur_MealDir", null);
        title = getIntent().getStringExtra(MEAL_TYPE);
        orderedTopMatchFileName = getIntent().getStringArrayListExtra("orderedTopMatchFileName");
        skipButton = (Button)findViewById(R.id.btn_dishmatch_tricky_skip);
        skipButton.setOnClickListener(new OnClickListener(){
            File src = new File(Environment.getExternalStoragePublicDirectory(
    	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+"tmp"+File.separator+timeStamp);
            File des = new File(Environment.getExternalStoragePublicDirectory(
    	           Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+mTitle);
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					PhotoDishesUtils.renameImgFile(src,title);
					PhotoDishesUtils.copyDirectory(src,des);
					Intent _intent = new Intent(DishMatchList.this, Today_TabView.class);
					_intent.putExtra(MEAL_TYPE, title);
					startActivity(_intent);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}});
        Log.d(TAG, "hah2");

        myPhotos = (ArrayList<Bitmap>)Global.getObjects();
        
	    ListView listView = (ListView)findViewById(R.id.lv_dishmatch_tricky);
        listView.setAdapter(new PhotoListAdapter(getApplicationContext(),myPhotos));
        listView.setDivider(this.getResources().getDrawable(R.drawable.trickiestpart_green));
        listView.setDividerHeight(5);
        listView.setOnItemClickListener(new ListView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Log.d("test","inside on click listener");
				Context context = view.getContext();
				Bitmap image = myPhotos.get(position);
				String fName = orderedTopMatchFileName.get(position);
				try {
					File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+
							File.separator + "MyCameraApp" + File.separator+title+ File.separator + new SimpleDateFormat("yyyyMMdd").format(new Date()));
					//image.compress(CompressFormat.PNG, 100, new FileOutputStream(file));
					
				    // Create the storage directory if it does not exist
				    if (! file.exists()){
				        if (! file.mkdirs()){
				            Log.d("MyCameraApp"+File.separator+title, "failed to create directory");
				        }
				    }
				    
					 // Create a media file name
				    String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
				    Log.d(TAG, file.getPath());
				    String newfName = title+time+".jpg";
				    //if the fName has a data file i the map, 
				    //then we add a new key and value, key is new file name with the name time, value is the same data file. 
				    /////////////////////////////////////////////////////////////////
				    if(mImageLabelingMap.get(fName)!= null){
				    	mImageLabelingMap.put(newfName, mImageLabelingMap.get(fName));
				    }
				    File mediaFile = new File(file.getPath(), title+ time + ".jpg");
				   // File mediaFile = new File(file.getPath(),fName);
				   // Log.d("DishMatchList", file.getPath() + File.separator +
				    	//	title+ time + ".jpg");
				    
					FileOutputStream fos = null;
					fos = new FileOutputStream(mediaFile);
					image.compress(CompressFormat.JPEG, 100, fos);
					fos.flush();
					fos.close();
					PhotoDishesUtils.DeleteRecursive(new File(Environment.getExternalStoragePublicDirectory(
				            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+"tmp"));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.d(TAG, "saving image failed");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Log.d("TEST","image onclick is: "+ image);
				Log.d("TEST","image onclick filename is : " + fName);
				Intent i = new Intent(context,Today_TabView.class);
				i.putExtra("Bitmap",image);
				i.putExtra(MEAL_TYPE, title);
				startActivity(i);
			}});
        
        
    }
    
    public class PhotoListAdapter extends BaseAdapter{
  	  Context c;
  	  private LayoutInflater mInflater;
  	  ArrayList<Bitmap> myPhotos = new ArrayList<Bitmap>();
  	  public PhotoListAdapter(Context mContext,ArrayList<Bitmap> bitmapArrayList){
  		  c = mContext;
  		  myPhotos = bitmapArrayList;
  		  mInflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  	  }
  	  @Override
  	  public int getCount() {
  	  	// TODO Auto-generated method stub
  	  	return myPhotos.size();
  	  }

  	  @Override
  	  public Object getItem(int position) {
  	  	// TODO Auto-generated method stub
  	  	return position;
  	  }

  	  @Override
  	  public long getItemId(int position) {
  	  	// TODO Auto-generated method stub
  	  	return position;
  	  }

  	  @Override
  	  public View getView(int position, View view, ViewGroup parent) {
  	  	// TODO Auto-generated method stub
  		  System.out.println("getView " + position + " " + view);
            ImageView imageView;
            if (view == null) {
                view = mInflater.inflate(R.layout.trickiestpart_photolist, null);        
            } else {
          	  imageView = (ImageView)view.findViewById(R.id.photo_list);
            }
            imageView = (ImageView)view.findViewById(R.id.photo_list);
            if(checkLabeled(position)){
          	  Bitmap mBitmap = myPhotos.get(position);
                BitmapFactory.Options ops = new BitmapFactory.Options();
                //ops.inSampleSize=;
                Bitmap checkBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.label_check);
                mBitmap = getBitmapOverlay(mBitmap,checkBitmap,mBitmap.getWidth()/3,0);
                imageView.setImageBitmap(mBitmap);
            }else{
          	  imageView.setImageBitmap(myPhotos.get(position));
            }
            return view;
        }
    }
    
    public static Bitmap getBitmapOverlay(Bitmap bmp1,Bitmap bmp2,int left, int top ){
  	  Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),bmp1.getHeight(),bmp1.getConfig());
  	  Canvas canvas = new Canvas(bmOverlay);
  	  Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
  	  canvas.drawBitmap(bmp1, 0, 0,paint);
  	  canvas.drawBitmap(bmp2, left, top,paint);
  	  return bmOverlay;
    }
    
    public Boolean checkLabeled(int position){
  	  String key = orderedTopMatchFileName.get(position);
  	  if(mImageLabelingMap.get(key) != null){
  		  return true;
  	  }else {
  	  return false;
  	  }
    }
    
   
   
    
    
}
