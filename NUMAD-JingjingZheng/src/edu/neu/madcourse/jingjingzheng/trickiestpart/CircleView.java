package edu.neu.madcourse.jingjingzheng.trickiestpart;


import java.util.ArrayList;
import edu.neu.madcourse.jingjingzheng.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Bitmap.Config;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class CircleView extends View implements Runnable{
	private String TAG = "edu.neu.madcourse.shanshanhuang.trickiestpart.CircleView";
	float x,y;
	float min_x,min_y;
	float max_x,max_y;
	float center_x,center_y;
	 private float mCircleX,mCircleY;
	 Canvas canvas;
	 Bitmap resizedBitmap1;
	 Bitmap resizedBitmap2;
	// public static Bitmap croppedBitmap;
	 Bitmap pointBitmap;
	  private Context mContext;
	 Paint paint;
	 Paint centerPaint;
	 private static int view_w;
	 private static int view_h;
	 public static PointF center;
	 Boolean afterClear = false;
     ArrayList<PointF> pointsAlongPath = new ArrayList<PointF>();
     Path path = new Path();
    

     
	 CircleView(Context context)
	 {  
		 super(context);
		 mContext = context;
	 }
	 
	 public CircleView(Context context, AttributeSet aSet) { 
		 super(context, aSet);
		 mContext = context;
		 Log.d("circleview","inside constructor");
		 WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		  view_w = display.getWidth(); 
		  view_h = display.getHeight();
		  max_x = 0;
		  max_y = 0;
		  min_x = view_w;
		  min_y= view_w;
		  Log.d("circleview","inside init");
			 paint=new Paint();
			 paint.setColor(Color.CYAN);
			 paint.setAlpha(180);	 
			 paint.setStyle(Paint.Style.STROKE); 		 
			 paint.setStrokeWidth(10);
			 
			 centerPaint = new Paint();
			 centerPaint.setColor(Color.TRANSPARENT);
			 centerPaint.setStyle(Paint.Style.STROKE);
			 centerPaint.setStrokeWidth(20);
			 resizedBitmap1 = Bitmap.createBitmap(view_w,view_h, Config.ARGB_8888);
			 canvas = new Canvas(resizedBitmap1);
			 
			 new Thread(this).start();
			 }
	 

	 public boolean onTouchEvent(MotionEvent event)
	 {
		 if(event.getX() <0){
			 x = 0;
		 }else if(event.getX() > view_w){
			 x = view_w;
		 }else {
			 x= event.getX();
		 }
		 
		 if( (event.getY()-40) <0){
			 y = 0;
		 }else if ( (event.getY()-40) > view_h){
			 y = view_h;
		 }else{
		 y=(event.getY()-40);
		 }
		 if(x>max_x){
			 max_x = x;
		 }else if(x<min_x){
			 min_x = x;
		 }else {
			 
		 }

		 if(y>max_y){
			 max_y = y;
		 }else if(y + 40 <min_y){
			 min_y = y;
		 }else {
			 
		 }
		 
		 PointF p = new PointF(x,y);
		 pointsAlongPath.add(p);
		 
		 
		 switch (event.getAction()) 
		 {
		 case MotionEvent.ACTION_DOWN:
			 if(afterClear){
		     path = new Path();
			 }
			 path.moveTo(x, y);
			 
			 break;
 
		 case MotionEvent.ACTION_MOVE:
			 path.lineTo(x, y);  
			 //path.close();
			 canvas.drawPath(path, paint); 

			 break;
 
		 case MotionEvent.ACTION_UP:
			 afterClear = false;
			 if (Global.getPoints() != null){
				 Global.setPointsEmpty();
			 }
			 Global.setPoint(pointsAlongPath);
			 center_x = (max_x - min_x) /2 + min_x ;
             center_y = (max_y -  min_y) /2 + min_y ;
             mCircleX = center_x;
             mCircleY = center_y;
             Log.d("CIRCLE_VIEW","center x is : " + center_x);
             Log.d("CIRCLE_VIEW","center y is : " + center_y);
             center = new PointF(center_x,center_y);
             canvas.drawPoint(center_x, center_y, centerPaint);
             //croppedBitmap = Bitmap.createScaledBitmap(resizedBitmap1, 200, 200, false);
             Log.d("circleview","create a cropped bitmap");  
             
             
			 break;
 
		 default:            
			 break;
  
		 }

		 return true;
	 }



	 @Override
	 public  void onDraw(Canvas canvas) {
    
       super.onDraw(canvas);
           canvas.drawBitmap(resizedBitmap1,0,0,paint);
	 }

    public void clear(){

    	pointsAlongPath.clear();
    	 resizedBitmap1.eraseColor(Color.TRANSPARENT);
    	 canvas.drawBitmap(resizedBitmap1,0,0,paint);
    	 afterClear = true;
    	 invalidate();
    }

	 public void run()
	 {
		 while (!Thread.currentThread().isInterrupted())
		 {
			 try
			 {
				 Thread.sleep(20);
			 }
			 catch (InterruptedException e)
			 {
				 Thread.currentThread().interrupt();
			 }
			
			 postInvalidate();
		 }
	 }
	
}