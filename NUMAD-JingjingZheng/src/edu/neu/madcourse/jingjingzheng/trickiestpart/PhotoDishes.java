package edu.neu.madcourse.jingjingzheng.trickiestpart;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.mobileClass.PhoneCheckAPI;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class PhotoDishes extends Activity implements OnClickListener{
	public static final String TRACKING = "tracking_setting";
	private static final String PREF_INITIAL_RUN = "initial_run";
	private SharedPreferences sf;
	
/*	private boolean mFirst = true;
	private Intent mIntent = null;*/
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trickiestpart_main);
		//PhoneCheckAPI.doAuthorization(this);
/*		mFirst = getPreferences(MODE_PRIVATE).getBoolean("first", true);
		Log.d("mFirst: ", String.valueOf(mFirst));*/
		ImageView logo = (ImageView)findViewById(R.id.main_logo);
		logo.setOnClickListener(this);
		sf=this.getSharedPreferences(TRACKING, MODE_PRIVATE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.main_logo:
			//createIntent();
			
/*			if(sf.getBoolean(PREF_INITIAL_RUN, true)){
			sf.edit().putBoolean(PREF_INITIAL_RUN, false).commit();			
			Intent i = new Intent(this,TutorialActivity.class);
			startActivity(i);
			break;
			}else{
				Intent i = new Intent(this,Today.class);
				startActivity(i);
				break;
			}*/
			
			if(sf.getBoolean(PREF_INITIAL_RUN, true)){
				//sf.edit().putBoolean(PREF_INITIAL_RUN, false).commit();			
				Intent i = new Intent(this,Tutorial.class);
				startActivity(i);
				break;
				}else{
					Intent i = new Intent(this,Today.class);
					startActivity(i);
					break;
				}
			
		}
	}
	
/*	protected void createIntent() {
		if (isFirstOpened()) {
			mIntent = new Intent(this, TutorialActivity.class);
		} else {
			mIntent = new Intent(this, FirstTimeTrackingSetting.class);
		}
		startActivity(mIntent);
	}*/
	
	
	protected void onDestroy() {
		getPreferences(MODE_PRIVATE).edit().putBoolean("first", false).commit();
		
		sf.edit().putBoolean(PREF_INITIAL_RUN, false).commit();
		super.onDestroy();
	}
	
/*	protected boolean isFirstOpened() {
		return mFirst;
	}*/
}

