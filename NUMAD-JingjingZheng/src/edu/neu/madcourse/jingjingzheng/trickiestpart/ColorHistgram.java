package edu.neu.madcourse.jingjingzheng.trickiestpart;


////////////////////added code

import java.util.ArrayList;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Size;
import org.opencv.imgproc.*;
import android.util.Log;

public class ColorHistgram {
        
    private int histSize;
    private float[] hranges;
    private float[][] ranges;
    private int[] channels;
    private static final String TAG = "ColorHistgram";
    
    
    public ColorHistgram() {
        
        //Prepare arguments for a color histogram
        this.histSize = 256;
        this.hranges = new float[2];
        this.ranges = new float[3][];
        this.channels = new int[3];
        
        //this.histSize[0] = 256;
        this.hranges[0] = (float) 0.0;
        this.hranges[1] = (float) 255.0;
        //all channels have the same range
        this.ranges[0] = hranges;
        this.ranges[1] = hranges;
        this.ranges[2] = hranges;
        //the three channels
        this.channels[0] = 0;
        this.channels[1] = 1;
        this.channels[2] = 2;
           
    }
    
    
    //Compute the histogram
    public Mat getHistogram(final Mat img){
        
        ArrayList<Mat> images = new ArrayList<Mat>();
        images.add(img);
        
        Mat hist;
        double scale = 1.0;
        
        //BGR color histogram
        hranges[0] = (float) 0.0; //BRG range
        hranges[1] = (float) 255.0;
        //the three channels
        channels[0] = 0;
        channels[1] = 1;
        channels[2] = 2;
        
        
//        MatOfInt mchannels = new MatOfInt();
//        mchannels.fromArray(channels);
        Mat mmask = new Mat();
        
        Size dsize = new Size(img.width()*scale, img.height()*scale);
        
        //Mat img2 = new Mat(dsize, CvType.CV_32F);
        hist = new Mat(dsize, CvType.CV_32F);
        
        MatOfInt mhistSize = new MatOfInt();
        mhistSize.fromArray(histSize);
        
        MatOfFloat mhranges = new MatOfFloat();
        mhranges.fromArray(hranges);
        //haha.fromArray(channels);
        
        Log.d(TAG, "computing begin...");
        
        for(int c = 0; c < 3; c++){
            MatOfInt mchannels = new MatOfInt();
            mchannels.fromArray(channels[c]);
        	Imgproc.calcHist(images, mchannels, mmask, hist, mhistSize, mhranges);
        }
        
        Log.i("SERVICE HISTOGRAMS", "histograms size is = " + histSize);
       
        return hist;
        
    }
    
    public Mat colorReduce(final Mat image, int div){
        div = 64;
        double scale = 1.0;
        int n = (int)(Math.log(div)/Math.log(2.0));
        //Mask used to round the pixel value
        char mask = (char) (0xFF << n); //TODO uchar mask = 0xFF << n 
        float[] pixel = new float[image.channels()];
        	
//		//Set output iamge (always 1-channel)
        Size dsize = new Size(image.width()*scale,image.height()*scale);
        Mat result = new Mat(image.rows(), image.cols(), image.type());

        int factor = div >> 1;		
        for(int i = 0; i < image.rows(); ++i){
            for(int j = 0; j < image.cols(); ++j){
                
                image.get(i, j, pixel);
                pixel[0] = (float) (((int) pixel[0] & mask) + factor);
                pixel[1] = (float) (((int) pixel[1] & mask) + factor);
                pixel[2] = (float) (((int) pixel[2] & mask) + factor);				
                result.put(i, j, pixel);
                //result.
            }
            
        }
        
        return result;
        
    }	
}
