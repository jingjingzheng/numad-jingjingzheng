package edu.neu.madcourse.jingjingzheng.trickiestpart;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class TrickiestPart extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/**Add small icon on titlebar*/
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.trickiestpart_launcher);
		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
	}
	
}
