package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import edu.neu.madcourse.jingjingzheng.R;

public class ShowTypeDialogFragment extends DialogFragment{
	ArrayList<Integer> mSelectedItems;
    private final String LABELING = "Labeling";
    private final static String START_LABELING = "start_labeling";
    private SharedPreferences sf;
    private final String TAG="dialog";
	String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oils or Fats"};
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    mSelectedItems = new ArrayList<Integer>();  // Where we track the selected items
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    sf = getActivity().getSharedPreferences(LABELING, 0);
	 // Get the layout inflater
	   // LayoutInflater inflater = .getLayoutInflater();
	    // Set the dialog title
	    builder.setTitle(R.string.select_type_label)
	    // Specify the list array, the items to be selected by default (null for none),
	    // and the listener through which to receive callbacks when items are selected
	           .setMultiChoiceItems(R.array.food_types, null,
	                      new DialogInterface.OnMultiChoiceClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int which,
	                       boolean isChecked) {
	                   if (isChecked) {
	                       // If the user checked the item, add it to the selected items
	                       mSelectedItems.add(which);
	                       sf.edit().putBoolean(select_image[which], true).commit(); 
	                       Log.d(TAG,"select category: "+ select_image[which]);
	                   } else if (mSelectedItems.contains(which)) {
	                       // Else, if the item is already in the array, remove it 
	                       mSelectedItems.remove(Integer.valueOf(which));
	                       sf.edit().putBoolean(select_image[which], false).commit(); 
	                   }
	               }
	           })
	    // Set the action buttons
	           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   // User clicked OK, so save the mSelectedItems results somewhere
	                   // or return them to the component that opened the dialog
	            	
	            	//sf.edit().putBoolean(START_LABELING, true).commit();
	            	ArrayList<String> selected = new ArrayList<String>();
	            	for(int i:mSelectedItems){
	            		selected.add(select_image[i]);
//	            		sf.edit().putBoolean(select_image[i], true).commit();
//	            		Log.d(TAG,"select category: "+ select_image[i]);
	            	}
	            	//sf.edit().putString("categories", StringUtils.join(selected,","));
	            	//sf.edit().putBoolean(Labeling.START_LABELING, true).commit();
	            	sf.edit().putString(Labeling.TAG_FILENAME,"").commit();
	            	sf.edit().putBoolean(Labeling.TAG_ISEDITING,true).commit();
	            	Intent i = new Intent(getActivity(), Labeling.class);
//	           		// need to give AddChart the array of items selected.
	           		i.putExtra("categories",selected);
	           		i.putExtra(START_LABELING, true);
//	           		i.putExtra(Labeling.TAG_ISEDITING, true);
//	           		i.putExtra(Labeling.TAG_FILENAME, "");
//	           		i.putExtra(START_LABELING, true);
	           		startActivity(i);
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                 
	               }
	           });

	    return builder.create();
	}
}
