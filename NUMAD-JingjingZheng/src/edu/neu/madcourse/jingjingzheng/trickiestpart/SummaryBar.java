package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.content.Context;
import android.util.Log;


public class SummaryBar {
  ArrayList<Category> mCategories;
  ArrayList<SummaryBarSegment> mProfileBarSegments;
  Context mContext;
  private String mFileName;
  //private SharedPreferences sf;
  private static ArrayList<Category> selected=new ArrayList<Category>();
  private static final String TAG = "edu.neu.madcourse.shanshanhuang.trickiestpart.SummaryBar";

  // Constructor for a given list of categories
  public SummaryBar(ArrayList<Category> categories) {
    mCategories = categories;
    mProfileBarSegments = categoriesToSegments(categories);
  }

  //  Constructor with only a context means we have to get categories from
  // files.
  public SummaryBar(Context ctx) {
    mContext = ctx;
  }

  /**
   * Creates an arrayList of segments that represents the averaged totals of
   * all segments - the sum of the value of the segments returned is 1.
   * god this is awful
   */
  public ArrayList<SummaryBarSegment> averageAllSegments() {
	//=========================================================
    // Keeping track of all the segments total percentages
//	  Log.d(TAG, "## AverageAllSegment CALLED");
//	  trackingList = tracking;
//	  Log.d(TAG,"selected tracking types: "+ trackingList);
	  //selected = s;
    HashMap<Integer, Double> segmentTotal = 
      new HashMap<Integer, Double>();
    // Count the number of segments by color
    HashMap<Integer, Integer> numberOfSegmentsWithColor =
      new HashMap<Integer, Integer>();

    // The hashmap to return, that contains the averages for each color
    HashMap<Integer, Double> segmentAverage = 
      new HashMap<Integer, Double>();
    //==========================================================
    
    // populate the counting map and the total map
    for (SummaryBarSegment segment : getAllSegments()) { 
      // if we haven't added one of this color, initialize it with 0
      if (!numberOfSegmentsWithColor.containsKey(segment.getColor())) {
        numberOfSegmentsWithColor.put(segment.getColor(), 0);
      }
      // add 1 to the number of segments with this color
      numberOfSegmentsWithColor.put(segment.getColor(), 
          numberOfSegmentsWithColor.get(segment.getColor()) + 1);
      // add this segment's value to the total
      // if we havent added one of this color, initialize it with 0
      if (!segmentTotal.containsKey(segment.getColor())) {
        segmentTotal.put(segment.getColor(), (double)0);
      }
      // then/otherwise add the value
      segmentTotal.put(segment.getColor(),
          segmentTotal.get(segment.getColor()) + segment.getValue());
    }

    // Turn the totals into averages
    Iterator<HashMap.Entry<Integer, Double>> it = segmentTotal.entrySet().iterator();
    while (it.hasNext()) {
      HashMap.Entry<Integer, Double> pair = it.next();
      // put the average percentage in te segmentAverage map
      segmentAverage.put(pair.getKey(), 
          // compute the average percentage
          (pair.getValue() /
           numberOfSegmentsWithColor.get(pair.getKey())));
    }
    
    ArrayList<SummaryBarSegment> segments = new ArrayList<SummaryBarSegment>();
    // segmentAverage now has Color -> Average Percentage. 

    Iterator<HashMap.Entry<Integer, Double>> iter = segmentAverage.entrySet().iterator();
    // add all of the average percentages to a list of segments
    while(iter.hasNext()) {
      HashMap.Entry<Integer, Double> pair = iter.next();
      segments.add(new SummaryBarSegment(pair.getValue(), pair.getKey()));
    }
    double total = 0;

    // compute the total for everything
    for (SummaryBarSegment segment : segments) {
      total+=segment.getValue();
    }
    // using the total, compute the correct fraction for each average
    for (SummaryBarSegment segment : segments) {
      double val = segment.getValue();
      segment.setValue(val / total);
    }
    // now each segment is a decimal representing it's percentage representation
    return segments;
  }

  private int countSegmentsWithColor(ArrayList<SummaryBarSegment> segments, int color) {
    int counter = 0;
    for (SummaryBarSegment seg : segments) {
      if (seg.getColor() == color) {
        counter++;
      }
    }
	return counter;
  }
  /**
   * Returns an ArrayList of all the segments stored in files
   */
  private ArrayList<SummaryBarSegment> getAllSegments() {
    ArrayList<SummaryBarSegment> segments = new ArrayList<SummaryBarSegment>();
    for (Category cat : getAllCategoriesFromFiles()) {
      segments.add(new SummaryBarSegment(cat));
    }
    return segments;
  }
  
  public void setCurrentFile(String filename){
	  this.mFileName = filename;
  }
  
  
  private ArrayList<Category> getSelectedCategories(){
	return selected;
//	  ArrayList<Category> selected = new ArrayList<Category>();
//	  for(String str : trackingList){
//		  selected.addAll(JSONParser.getCatListFromString(str, mContext));
//	  }
//	  return selected;
  }

  /**
   * Returns a list of all the categories stored in files
   */
  public ArrayList<Category> getAllCategoriesFromFiles() {
    String[] savedFiles = mContext.fileList();
    for (String str : savedFiles) {
      Log.d(TAG, "@@@@ "+str);
    }
    ArrayList<Category> categories = new ArrayList<Category>();
//    for (String filename : savedFiles) {
//      String fileData = getFileData(filename);
//      // if we parsed the file correctly
//      if (!fileData.equals("error")) {
//        // add all the categories in the current file to the list
//        categories.addAll(JSONParser.getCatListFromString(fileData, mContext));
//      }
//    }
  
    //Log.d(TAG, "mfILENAME IN SUMMARYBAR: " + mFileName);
    String fileData = getFileData(mFileName);

    //String fileData = getFileData(savedFiles[savedFiles.length-1]);
    if(!fileData.equals("error")){
    	categories.addAll(JSONParser.getCatListFromString(fileData, mContext));
    }
    Log.d(TAG,"categoriest from files : " + categories);
	return categories;
  }
  
  /**
   * Gets file data from a given filename
   * @param filename the file to get data from
   */
  private String getFileData(String filename) {
    String data = "error";
    try {
      FileInputStream fis = mContext.openFileInput(filename);
      BufferedReader r = new BufferedReader(new InputStreamReader(fis));
      data = r.readLine();
      r.close();
    } catch (FileNotFoundException e) {
      /* Toast.makeText(mContext, "Something's wrong with the filesystem..."); */
      Log.e(TAG, "Couldn't find a file in Profile: ");
          e.printStackTrace();
    } catch (IOException e) {
      /* Toast.makeText(mContext, "I burped while reading a file, so I can't show your profile"); */
      Log.e(TAG, "Had trouble reading from a file inn Profile: ");
      e.printStackTrace();
    }
    return data;
  }

  /**
   * Converts an ArrayList of categories to an ArrayList of segments
   * @param cats The list of categories to convert
   */
  private ArrayList<SummaryBarSegment> categoriesToSegments(ArrayList<Category> cats) {
	  ArrayList<SummaryBarSegment> segments = new ArrayList<SummaryBarSegment>();
	  // iterate through cats, calculating percentage for each
	  for (Category cat : cats) {
		  segments.add(new SummaryBarSegment(cat));
	  }
	  return segments;
  }

  /**
   * Uses the rad values of the given category to calculate the percentage
   * of the circle it takes up
   * @param cat The category we're using to calculate the percentages
   */
  protected static double categoryToPercentage(Category cat) {
	  double diff = TouchPoint.getDistCW(cat.getpCCW(), cat.getpCW());
	  if (diff == 0){
		  diff = Math.PI*2;
	  }
	  double frac = diff/(2*Math.PI);
	  return frac;
  }


}
