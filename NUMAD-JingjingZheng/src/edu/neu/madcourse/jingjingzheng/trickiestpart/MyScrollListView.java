package edu.neu.madcourse.jingjingzheng.trickiestpart;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class MyScrollListView extends ListActivity{
	
   static String[] values = new String[] {};
   
   public MyScrollListView(String[] values){
	   MyScrollListView.values = values;
   }
   
   @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setListAdapter(new MyScrollListViewAdapter(this, values));

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		//get selected items
		String selectedValue = (String) getListAdapter().getItem(position);
		Toast.makeText(this, selectedValue, Toast.LENGTH_SHORT).show();

	}
}
