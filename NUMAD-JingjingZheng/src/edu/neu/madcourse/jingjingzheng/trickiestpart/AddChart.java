package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.R.color;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AddChart extends Activity {
  private final String TAG = "trickiestpart.AddChart";
  private final String SPREFSKEY = "edu.neu.madcourse.shanshanhuang.trickiestpart";
  public final static String TAG_ISEDITING = "editing";
  public static final String MEAL_TYPE="meal_type";
  public final static String TAG_FILENAME = "FileName";
  public final static String FILE_NAME_FORMAT = "yyyy-MM-dd_HH+mm+ss";
  public final static String HISTORY_DATE_FORMAT = "yyyy-MM-dd";
  public final static String HISTORY_TIME_FORMAT = "HH+mm+ss";
  /** Called when the activity is first created. */
  private String fName;
  private ArrayList<Bitmap> croppedBitmap = new ArrayList<Bitmap>();
  private Bitmap image;

  private static String title;
  private CircleView mGameView;
  public void onCreate(Bundle savedInstanceState) {
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.trickiestpart_labeling);
    Display myDisplay =getWindowManager().getDefaultDisplay();
	int width =myDisplay.getWidth();
	int height=myDisplay.getHeight();
	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
		    width, height/2);
	image = (Bitmap)getIntent().getParcelableExtra("Bitmap");
	fName = getIntent().getStringExtra("ImageFileName");
    CircleView c =  (CircleView)findViewById(R.id.circle_view);
    mGameView =c;
    mGameView.setLayoutParams(params);
    Drawable d = new BitmapDrawable(getResources(),image);
    mGameView.setBackgroundDrawable(d);
    
    croppedBitmap.add(mGameView.resizedBitmap1);
    Global.setObjects(croppedBitmap);
    
    Button go = (Button)findViewById(R.id.btn);
    go.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			//check if user draws or not?
			if(mGameView.pointsAlongPath.size()!=0&&pathClosed()){
			Intent i = new Intent(AddChart.this,CategorySelection.class);
			Log.d("DRAW","go on click");
//			i.putExtra("bitmap", CircleView.croppedBitmap);
			i.putExtra("Bitmap",image);
			i.putExtra("ImageFileName",fName);
			i.putExtra("center_x", mGameView.center_x);
			i.putExtra("center_y", mGameView.center_y);
			i.putExtra(MEAL_TYPE,title);
			Log.d("DRAW","put extra success");
			startActivity(i);
			}else {
				Toast.makeText(getApplicationContext(), "Please draw a closed path.", Toast.LENGTH_SHORT).show();
				mGameView.clear();
			}
		}

		
    });
    
    
    Button clear = (Button)findViewById(R.id.clear_btn);
    clear.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
		  mGameView.clear();
		 // onResume();
		}
    	
    });

	title = getIntent().getStringExtra(MEAL_TYPE);
	View backBtn = findViewById(R.id.labeling_back);
	backBtn.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
		
	});
	View saveBtn = findViewById(R.id.labeling_right);
	saveBtn.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(mGameView.pointsAlongPath.size()!=0&&pathClosed()){
				Intent i = new Intent(AddChart.this,CategorySelection.class);
				Log.d("DRAW","go on click");
//				i.putExtra("bitmap", CircleView.croppedBitmap);
				i.putExtra("Bitmap",image);
				i.putExtra("center_x", mGameView.center_x);
				i.putExtra("center_y", mGameView.center_y);
				i.putExtra(MEAL_TYPE,title);
				Log.d("DRAW","put extra success");
				startActivity(i);
				}else {
					Toast.makeText(getApplicationContext(), "Please draw a closed path.", Toast.LENGTH_SHORT).show();
					mGameView.clear();
				}
			}
		
	});
	
  }
  
  private boolean pathClosed() {
		// TODO Auto-generated method stub
  	ArrayList<PointF> l = new ArrayList<PointF>();
  	l = mGameView.pointsAlongPath;
  	float firstX = l.get(0).x;
  	float firstY = l.get(0).y;
  	float lastX = l.get(l.size()-1).x;
  	float lastY = l.get(l.size()-1).y;
  	Log.d(TAG,"distance between two points: "+ Math.sqrt((Math.pow(firstX - lastX,2) + Math.pow(firstY - lastY,2))));
  	if(Math.sqrt((Math.pow(firstX - lastX,2) + Math.pow(firstY - lastY,2))) <=100 ){
  		return true;
  	}
		return false;
	}


}

