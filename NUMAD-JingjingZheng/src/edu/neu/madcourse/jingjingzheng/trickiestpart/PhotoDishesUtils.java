package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

public class PhotoDishesUtils {
	
	public  static void renameImgFile(File path,String title){
		Log.d("PhotoDishesUtils","inside renameImgFile");
		Log.d("PhotoDishesUtils",path.toString());
    	File[] listFile = path.listFiles();
    	String time = new SimpleDateFormat("yyyyMMdd").format(new Date());
    	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    	File file =new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+
				File.separator + "MyCameraApp" + File.separator+title+ File.separator + time);
    	File newPath = new File(file.getPath(), title+ timeStamp + ".jpg");
    	Log.d("PhotoDishesUtils",path.length() + "");
//    	if(path.length()>0){
    	for(File f:listFile){
    		if(f.getName().substring(0, 3).equals("tmp")){
    			f.renameTo(newPath);
    		}
    	}
    }
	
	 public static void copyDirectory(File sourceLocation , File targetLocation)
	 		   throws IOException {

	 		       if (sourceLocation.isDirectory()) {
	 		           if (!targetLocation.exists() && !targetLocation.mkdirs()) {
	 		               throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
	 		           }

	 		           String[] children = sourceLocation.list();
	 		           for (int i=0; i<children.length; i++) {
	 		               copyDirectory(new File(sourceLocation, children[i]),
	 		                       new File(targetLocation, children[i]));
	 		               new File(sourceLocation, children[i]).delete();
	 		           }
	 		       } else {

	 		           // make sure the directory we plan to store the recording in exists
	 		           File directory = targetLocation.getParentFile();
	 		           if (directory != null && !directory.exists() && !directory.mkdirs()) {
	 		               throw new IOException("Cannot create dir " + directory.getAbsolutePath());
	 		           }

	 		           InputStream in = new FileInputStream(sourceLocation);
	 		           OutputStream out = new FileOutputStream(targetLocation);

	 		           // Copy the bits from instream to outstream
	 		           byte[] buf = new byte[1024];
	 		           int len;
	 		           while ((len = in.read(buf)) > 0) {
	 		               out.write(buf, 0, len);
	 		           }
	 		           in.close();
	 		           out.close();
	 		       }    
	 		   }


	 public static void DeleteRecursive(File fileOrDirectory) {
	        if (fileOrDirectory.isDirectory())
	            for (File child : fileOrDirectory.listFiles())
	                DeleteRecursive(child);

	        fileOrDirectory.delete();
	    }


	 public static void loadImages(File[] listFiles,ArrayList<Bitmap> myPhotos,ArrayList<String> myFileNames) {
	 	   
		    final int REQUIRED_SIZE=200;
		    String fName;
			BitmapFactory.Options options = new BitmapFactory.Options(); 
			options.inJustDecodeBounds = true;        
	        for (int i = 0; i<listFiles.length;i++) {
	        	 Bitmap scaled = null;	
	        	 BitmapFactory.decodeFile(listFiles[i].getAbsolutePath(),options);
	        	 int width_tmp = options.outWidth,height_tmp=options.outHeight;
	        	 int scale = 1;
	        	 while(true){
	        		 if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
	        			 break;
	        		 width_tmp/=2;
	        		 height_tmp/=2;
	        		 scale*=2;
	        	 }
	      	
	        	 BitmapFactory.Options options2 = new BitmapFactory.Options();
	        	 options2.inSampleSize = scale;
	        	 scaled = BitmapFactory.decodeFile(listFiles[i].getAbsolutePath(), options2);
	        	 fName = listFiles[i].getName();
	        	 
	        	 // add to the image list
	        	 if (scaled != null) {
	        		 //origin.recycle(); // explicit call to avoid out of memory
	        		 myPhotos.add(scaled);
	        		 myFileNames.add(fName);
	        	 } else {
	        		// myPhotos.add(origin);
	        	 }
	        }
		}
	 
	 
	 public static void loadImages(File[] listFiles,ArrayList<Bitmap> myPhotos) {
	 	   
		    final int REQUIRED_SIZE=200;
		 
			BitmapFactory.Options options = new BitmapFactory.Options(); 
			options.inJustDecodeBounds = true;        
	        for (int i = 0; i<listFiles.length;i++) {
	        	 Bitmap scaled = null;	
	        	 BitmapFactory.decodeFile(listFiles[i].getAbsolutePath(),options);
	        	 int width_tmp = options.outWidth,height_tmp=options.outHeight;
	        	 int scale = 1;
	        	 while(true){
	        		 if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
	        			 break;
	        		 width_tmp/=2;
	        		 height_tmp/=2;
	        		 scale*=2;
	        	 }
	      	
	        	 BitmapFactory.Options options2 = new BitmapFactory.Options();
	        	 options2.inSampleSize = scale;
	        	 scaled = BitmapFactory.decodeFile(listFiles[i].getAbsolutePath(), options2);
	        	
	        	 
	        	 // add to the image list
	        	 if (scaled != null) {
	        		 //origin.recycle(); // explicit call to avoid out of memory
	        		 myPhotos.add(scaled);
	        		
	        	 } else {
	        		// myPhotos.add(origin);
	        	 }
	        }
		}
	 
	 

}
