package edu.neu.madcourse.jingjingzheng.trickiestpart;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import edu.neu.madcourse.jingjingzheng.R;
import com.viewpagerindicator.TabPageIndicator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class Today_TabView extends FragmentActivity{
	private static final String[] CONTENT = new String[] {"Breakfast","Lunch","Dinner","Snack"};
	private static final String TAG = "HorzScrollWithImageMenu";
	public static final String MEAL_TYPE="meal_type";
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public  int currentPos;
	String mealType;
    MyHorizontalScrollView scrollView;
    View menu;
    View app;
    ImageView btnSlide;
    ImageView btnCamera;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
    ViewPager pager;
    int pos;
    Time today;
    String currentDate;

    public SharedPreferences mMealTimeSF;
    private static Uri fileUri;
    final Today_TabView thisActivity = this;
    private AlertDialog.Builder builder;
    private AlertDialog loadingDialog;
    private static String title;
    private String timeStamp;
    private ImageMatching im = new ImageMatching(thisActivity);
 
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.trickiestpart_today_tabview);
        Toast.makeText(thisActivity, "Click on the image to begin", Toast.LENGTH_SHORT).show();
        View backBtn = findViewById(R.id.today_tabview_back);
        backBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//finish();
				startActivity(new Intent(thisActivity,Today.class));				
			}
        	
        });
        mealType = getIntent().getStringExtra(MEAL_TYPE);
        for(int i=0;i<CONTENT.length;i++){
        	if(CONTENT[i].equals(mealType)){
        		pos = i;
        	}
        }

		
		FragmentStatePagerAdapter adapter = new MealTypeAdapter(getSupportFragmentManager());

		//Set the pager with an adapter
		pager = (ViewPager)findViewById(R.id.pager);
		pager.setAdapter(adapter);

		//Bind the title indicator to the adapter
		TabPageIndicator tabIndicator = (TabPageIndicator)findViewById(R.id.indicator);
		tabIndicator.setViewPager(pager,pos);
		tabIndicator.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int position) {
				// TODO Auto-generated method stub
				Log.d(TAG,"scroll position:" + position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				Log.d(TAG,"page selected position:" + position);
				pos = position;
			}});
		
		timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
		btnCamera=(ImageView)findViewById(R.id.today_tabview_camera);
        btnCamera.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Log.d(TAG,"inside camera on click");
				Log.d(TAG,"current position is: "+ pos);
				
			    title = (String) new MealTypeAdapter(getSupportFragmentManager()).getPageTitle(pos);
			    Log.d(TAG,"current title is:" +title);
			    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			    MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, title);
                fileUri = MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE,"tmp"); 
                
                // create a file to save the image
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
                
                // start the image capture Intent
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                
               

			}});
        
	}
	
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
		 builder = new AlertDialog.Builder(thisActivity);
		 builder.setTitle(R.string.dialog_title);
         builder.setMessage(R.string.matchlish_loading);
         loadingDialog = builder.create();
         		        
         loadingDialog.show();
	        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
	            if (resultCode == RESULT_OK) {
	            	File src = new File(Environment.getExternalStoragePublicDirectory(
	        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+"tmp"+File.separator+timeStamp);
	            	File file = new File(Environment.getExternalStoragePublicDirectory(
	        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+title);
	            	File fileTimeStamp = new File(Environment.getExternalStoragePublicDirectory(
	        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+title+File.separator+timeStamp);
	            	if(file.exists()){
	            		
	            	  if(file.length()>1||fileTimeStamp.listFiles().length>1){
	                    im.matching(title,fileUri, loadingDialog);
	                    //loadingDialog.dismiss();
	                  }else {
	               	 	loadingDialog.dismiss();
	               		try {
	               			PhotoDishesUtils.renameImgFile(src, title);
							PhotoDishesUtils.copyDirectory(src,fileTimeStamp);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	               		Intent i = new Intent(this,Today_TabView.class);
	               		i.putExtra(MEAL_TYPE, title);
	               		startActivity(i);
	               	}
	              }     
	            } else if (resultCode == RESULT_CANCELED) {
	            	Log.d(TAG, "cancelled capture the pic");
                    loadingDialog.dismiss();
	                // User cancelled the image capture
	            } else {
	                Toast.makeText(this, "failed", Toast.LENGTH_LONG).show();
	            }
	        }
	    }
	        

	
	class MealTypeAdapter extends FragmentStatePagerAdapter {
        public MealTypeAdapter(FragmentManager fm) {
            super(fm);           
        }

        @Override
        public Fragment getItem(int position) {
        	return TabViewFragment.newInstance(CONTENT[position%CONTENT.length]);
        }

        public CharSequence getPageTitle(int position) {	
            return CONTENT[position % CONTENT.length];
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }
       
    }

	
//	//initialize menu list items
//    public  void initMenuListView(Context context,ListView listView,int layout){
//    	final String[] arr = {"My Meal","Tracking Setting","Help","Acknowledgment"};
//    	final Integer[] images = { R.drawable.meal,
//                R.drawable.documents, R.drawable.settings, R.drawable.help };
//        	List<RowItem> rowItems;
//        	rowItems = new ArrayList<RowItem>();
//            for (int i = 0; i < arr.length; i++) {
//                RowItem item = new RowItem(images[i], arr[i]);
//                rowItems.add(item);
//            }
//            CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
//                    R.layout.trickiestpart_list_item, rowItems);
//    	listView.setAdapter(menu_adapter);
//    	listView.setOnItemClickListener(new OnItemClickListener(){
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position,
//					long id) {
//				// TODO Auto-generated method stub
//				Context context = view.getContext();
//				String str = arr[position];
//				if(str.equals("My Meal")){
//					vibrate(25);
//					startActivity(new Intent(context,Today.class));
//				}else if(str.equals("Tra")){
//					vibrate(25);
//					startActivity(new Intent(context,History.class));
//				}else if(str.equals("Tracking Setting")){
//					vibrate(25);
//					startActivity(new Intent(context,Tracking_Setting.class));
//				}else {
//					vibrate(25);
//					startActivity(new Intent(context,TutorialActivity.class));
//				}
//				
//			}
//    		
//    	});
//    }
    private void vibrate(int ms) {
        Vibrator v = 
          (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(ms);
      }


}

