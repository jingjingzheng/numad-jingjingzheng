package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import java.util.List;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.ClickListenerForScrolling;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.SizeCallbackForMenu;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Window;



public class Tutorial extends FragmentActivity{
	private static final String[] CONTENT = 
			new String[] {"Tutorial1",
		                  "Tutorial2",
		                  "Tutorial3",
		                  "Tutorial4"};
	public final static  String TAG="Tutorial";
	 TutorialFragmentAdapter mAdapter;
	 ViewPager mPager;
	 PageIndicator mIndicator;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.trickiestpart_history);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tutorial_circles);
		mAdapter = new TutorialFragmentAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.tutorial_pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator)findViewById(R.id.tutorial_indicator);
        mIndicator.setViewPager(mPager);
	}
	
	class TutorialFragmentAdapter extends FragmentStatePagerAdapter {
        public TutorialFragmentAdapter(FragmentManager fm) {
            super(fm);           
        }

        @Override
        public Fragment getItem(int position) {
        	return TutorialFragment.newInstance(CONTENT[position%CONTENT.length]);
        }

        public CharSequence getPageTitle(int position) {	
            return CONTENT[position % CONTENT.length];
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }
       
    }
	
	
}
