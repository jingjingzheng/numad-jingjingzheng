
package edu.neu.madcourse.jingjingzheng.trickiestpart;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.MyHorizontalScrollView.SizeCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import com.slidingmenu.lib.app.SlidingActivity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
/**
 * This demo uses a custom HorizontalScrollView that ignores touch events, and therefore does NOT allow manual scrolling.
 * 
 * The only scrolling allowed is scrolling in code triggered by the menu button.
 * 
 * When the button is pressed, both the menu and the app will scroll. So the menu isn't revealed from beneath the app, it
 * adjoins the app and moves with the app.
 */
public class Today extends SlidingActivity {
	private static final String TAG = "Today";
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	public static final String MEAL_TYPE="meal_type";
    MyHorizontalScrollView scrollView;
    View menu;
    View app;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
    String currentDate;
    Time today;
    private static String title;
    private Uri fileUri;
    private String timeStamp;
    private AlertDialog.Builder builder;
    private AlertDialog loadingDialog;
    private Today thisActivity = this;
    //private SlideHolder mSlideHolder;
    
	String[] select_image = {"GRAINS","DAIRY","ANIMAL_PROTEIN","VEGGIES","FRUITS","SWEETS","LEGUMES_NUTS","OILS_FATS"};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(this);
        timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.trickiestpart_scroll_view, null);
        /// added code for touch to slide
        setContentView(R.layout.trickiestpart_today);
  	    setBehindContentView(R.layout.trickiestpart_touch_menu);
  	    ListView touchMenuList =(ListView) findViewById(R.id.touch_menu_list);
  	    initMenuListView(this,touchMenuList,android.R.layout.simple_list_item_1);
  	    getSlidingMenu().setBehindOffset(80);
        // added code here 
  	    
        setContentView(scrollView);
       
        menu = inflater.inflate(R.layout.trickiestpart_menu, null);
        app = inflater.inflate(R.layout.trickiestpart_today, null);
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBar);

        ListView listView = (ListView) app.findViewById(R.id.list);
        initListView(this, listView, android.R.layout.simple_list_item_1);

        listView = (ListView) menu.findViewById(R.id.menu_list);
        initMenuListView(this, listView, android.R.layout.simple_list_item_1);

        btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlide);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));

        final View[] children = new View[] { menu, app };

        // Scroll to app (view[1]) when layout finished.
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));

    }
    
    //initialize the list items for today activity
    public void initListView(Context context, ListView listView,int layout) {
    	final String[] arr = {"Breakfast","Lunch","Dinner","Snack"};
 
    	final Integer[] images = { R.drawable.coffee,
            R.drawable.pizza, R.drawable.dinner, R.drawable.apple };
    	List<RowItem> rowItems;
    	rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < arr.length; i++) {
            RowItem item = new RowItem(images[i], arr[i]);
            rowItems.add(item);
        }
       TodayListViewAdapter adapter = new TodayListViewAdapter(this,
                R.layout.trickiestpart_list_item, rowItems);
        listView.setAdapter(adapter);
        
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	String str = arr[position];
            	if(str.equals("Breakfast")){
            		vibrate(25);
            		checkDirectory("Breakfast");
            	}else if(str.equals("Lunch")){
            		vibrate(25);
            		checkDirectory("Lunch");
            	}else if(str.equals("Dinner")){
            		vibrate(25);
            		checkDirectory("Dinner");
            	}else if(str.equals("Snack")){
            		vibrate(25);
                		checkDirectory("Snack");
            	}else{
            	}
            }
        });
    }
    
    public void checkDirectory(String path){
    	title = path;
    	timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        Log.d(TAG,"current date is : "+ timeStamp);
    	File file = new File(Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+path+File.separator+timeStamp);
    	if(file.exists()){
	      if(file.listFiles().length !=0){
	    	  Intent i = new Intent(this,Today_TabView.class);
	    	  i.putExtra(MEAL_TYPE, path);
	    	  startActivity(i);
	      }else{
	    	  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    	  MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, path);
              fileUri = MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE,"tmp"); // create a file to save the image
              intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

              // start the image capture Intent
              startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
             
              		        
             // loadingDialog.show();
	      }
    	}else{
	    	  // create Intent to take a picture and return control to the calling application
              Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
              MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, title);
              fileUri = MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE,"tmp");
              //MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE, path);// create a file to save the image
              intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

              // start the image capture Intent
              startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
//              builder = new AlertDialog.Builder(thisActivity);
//              builder.setMessage(R.string.matchlish_loading);
//              //loadingDialog = builder.create();
//              		        
//              //loadingDialog.show();
	        }
    	
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 builder = new AlertDialog.Builder(thisActivity);
    	 builder.setTitle(R.string.dialog_title);
         builder.setMessage(R.string.matchlish_loading);
         loadingDialog = builder.create();
    	if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
            	File src = new File(Environment.getExternalStoragePublicDirectory(
        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+"tmp"+File.separator+timeStamp);
            	//Log.d(TAG, " " + Environment.getExternalStoragePublicDirectory(
        	          //  Environment.DIRECTORY_PICTURES) + "MyCameraApp"+File.separator+"tmp"+File.separator+timeStamp);
            	File file = new File(Environment.getExternalStoragePublicDirectory(
        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+title);
            	File currentDir = new File(Environment.getExternalStoragePublicDirectory(
        	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+title+File.separator+timeStamp);
            	 // if(file.length()>1||currentDir.listFiles().length>0){
            	if(file.listFiles().length>1){
                     new ImageMatching(this).matching(title,fileUri, loadingDialog);
            
            	  } else {
            		loadingDialog.dismiss();
            		try {
            			PhotoDishesUtils.renameImgFile(src, title);
						PhotoDishesUtils.copyDirectory(src,currentDir);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		
            		Intent i = new Intent(this,Today_TabView.class);
            		i.putExtra(MEAL_TYPE, title);
            		startActivity(i);
            	}
            } else if (resultCode == RESULT_CANCELED) {
            	Log.d(TAG, "cancelled capture the pic");
            	loadingDialog.dismiss();
                // User cancelled the image capture
            } else {
                Toast.makeText(this, "failed", Toast.LENGTH_LONG).show();
            }
        }
    }
    
    //initialize menu list items
    public  void initMenuListView(Context context,ListView listView,int layout){
     	final String[] arr = {"My Meal","Tracking Setting","Help","Acknowledgment"};
     	final Integer[] images = { R.drawable.meal,
                 R.drawable.settings, R.drawable.help ,R.drawable.comments};
         	List<RowItem> rowItems;
         	rowItems = new ArrayList<RowItem>();
             for (int i = 0; i < arr.length; i++) {
                 RowItem item = new RowItem(images[i], arr[i]);
                 rowItems.add(item);
             }
             CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
                     R.layout.trickiestpart_list_item, rowItems);
     	listView.setAdapter(menu_adapter);
     	listView.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view, int position,
    					long id) {
    				// TODO Auto-generated method stub
    				Context context = view.getContext();
    				String str = arr[position];
    				if(str.equals("My Meal")){
    					vibrate(25);
    					startActivity(new Intent(context,Today.class));
//    				}else if(str.equals("History")){
//    					vibrate(25);
//    					startActivity(new Intent(context,History.class));
    				}else if(str.equals("Tracking Setting")){
    					startActivity(new Intent(context,Tracking_Setting.class));
    				}else if(str.equals("Help")){
    					vibrate(25);
    					//startActivity(new Intent(context,TutorialActivity.class));
    					startActivity(new Intent(context,Tutorial.class));
    					Log.d(TAG,"no matcing item!");
    				}else {
    					vibrate(25);
    					startActivity(new Intent(context,Acknowledgment.class));
    				}
    				
    			}
     		
     	});
    }
    private void vibrate(int ms) {
        Vibrator v = 
          (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(ms);
      }
  
    /**
     * Helper for examples with a HSV that should be scrolled by a menu View's width.
     */
    static class ClickListenerForScrolling implements OnClickListener {
        HorizontalScrollView scrollView;
        View menu;
        /**
         * Menu must NOT be out/shown to start with.
         */
        boolean menuOut = false;

        public ClickListenerForScrolling(HorizontalScrollView scrollView, View menu) {
            super();
            this.scrollView = scrollView;
            this.menu = menu;
        }

        @Override
        public void onClick(View v) {
            int menuWidth = menu.getMeasuredWidth();

            // Ensure menu is visible
            menu.setVisibility(View.VISIBLE);

            if (!menuOut) {
                // Scroll to 0 to reveal menu
                int left = 0;
                scrollView.smoothScrollTo(left, 0);
            } else {
                // Scroll to menuWidth so menu isn't on screen.
                int left = menuWidth;
                scrollView.smoothScrollTo(left, 0);
            }
            menuOut = !menuOut;
        }
    }

    /**
     * Helper that remembers the width of the 'slide' button, so that the 'slide' button remains in view, even when the menu is
     * showing.
     */
    static class SizeCallbackForMenu implements SizeCallback {
        int btnWidth;
        View btnSlide;

        public SizeCallbackForMenu(View btnSlide) {
            super();
            this.btnSlide = btnSlide;
        }

        @Override
        public void onGlobalLayout() {
            btnWidth = btnSlide.getMeasuredWidth();
            System.out.println("btnWidth=" + btnWidth);
        }

        @Override
        public void getViewSize(int idx, int w, int h, int[] dims) {
            dims[0] = w;
            dims[1] = h;
            final int menuIdx = 0;
            if (idx == menuIdx) {
                dims[0] = w - 80;
            }
        }
    }
    
   
}
