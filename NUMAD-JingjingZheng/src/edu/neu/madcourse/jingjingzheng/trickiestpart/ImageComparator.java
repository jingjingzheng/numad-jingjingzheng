package edu.neu.madcourse.jingjingzheng.trickiestpart;



///////////////add code

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class ImageComparator {

	private Mat reference;
	private Mat input;
	private Mat refH;
	private Mat inputH;
	
	private ColorHistgram hist;
	private int div;
	
	public ImageComparator(){
		this.div = 32;
		hist = new ColorHistgram();
	}
	
	/**
	 * Color reduction factor
	 * The comparison will be made on images with
	 * color space reduced by this factor in each dimension
	 */
	void setColorReduction(int factor){
		this.div = factor;
	}
	
	void setReferenceImage(final Mat image){
		this.reference = hist.colorReduce(image, div);
		this.refH = hist.getHistogram(reference);
	}
	
	double compare(final Mat image){
		this.input = this.hist.colorReduce(image, div);
		this.inputH = this.hist.getHistogram(input);
		
		return Imgproc.compareHist(refH, inputH, Imgproc.CV_COMP_INTERSECT);
	}
	
}
