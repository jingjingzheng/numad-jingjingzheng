package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import java.util.HashMap;
import android.graphics.Bitmap;
import android.graphics.PointF;

public class Global {
	
	//private static ArrayList<Object> mObjects = null;
	private static ArrayList<Bitmap> mObjects = null;
	private static ArrayList<PointF> mPoints  = null;
	private static ArrayList<Category> mCategory = null;
	private static HashMap<String,String> mMap = null;
	public static void setMapObjects(HashMap<String,String> objects){
		mMap = objects;
	}
	
	public static HashMap<String,String> getMapObjects(){
		return mMap;
	}
	
	public static void setHashMapEmpty(){
		//mObjects.clear();
		mMap = null;
		//ImgLoaded = null;
	}
	public static void setObjects(ArrayList<Bitmap> objects){
		mObjects = objects;
	}
	
	public static ArrayList<Bitmap> getObjects(){
		return mObjects;
	}
	
	public static void setEmpty(){
		//mObjects.clear();
		mObjects=null;
		//ImgLoaded = null;
	}
	
	public static void setCategoryObjects(ArrayList<Category> objects){
		mCategory = objects;
	}
	
	public static ArrayList<Category> getCategoryObjects(){
		return mCategory;
	}
	
	public static void setCategoryEmpty(){
		//mObjects.clear();
		mCategory=null;
		//ImgLoaded = null;
	}
	public static void setPoint(ArrayList<PointF> objects){
		mPoints = objects;
	}
	
	public static ArrayList<PointF> getPoints(){
		return mPoints;
	}
	
	public static void setPointsEmpty(){
	     //mPoints.clear();
		mPoints = null;
		//ImgLoaded = null;
	}
	
}