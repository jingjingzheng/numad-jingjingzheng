package edu.neu.madcourse.jingjingzheng.trickiestpart;


import edu.neu.madcourse.jingjingzheng.R;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class TutorialFragment extends Fragment{
	 private static final String KEY_CONTENT = "TutorialFragment:Content";
	 private static final String PREF_INITIAL_RUN = "initial_run";
	 public static final String TRACKING = "tracking_setting";
	 private SharedPreferences sf;
	 private static  Boolean lastOne=false;
	 private static final String Tutorial1 ="� Simply select a meal."+"\n\n"+"� Take a picture."+"\n\n"+"� Tab on the image to begin!";
	 private static final String Tutorial2 = "� Draw a path around your food."+"\n\n"+"� Tell us what are in it."+"\n\n"+"� Move the points to adjust!";
	 private static final String Tutorial3 ="� Set up your particular food types."+"\n\n"+"� View the percentages of your selected food types in Summary!";
	 private static final String Tutorial4 = "� Label similar food AGAIN and AGAIN?"+"\n\n"+"� NO,TOP matching list can save your time!";
     private static int image[] = {R.drawable.tutorial1,R.drawable.tutorial2,R.drawable.tutorial4,R.drawable.tutorial3};
	    public static TutorialFragment newInstance(String content) {
	        TutorialFragment fragment = new TutorialFragment();
	        
	        System.out.println(content);
	        if(content.equals("Tutorial1")){
	        	fragment.mContent = Tutorial1;
	            fragment.imageId=image[0];
	        }else if(content.equals("Tutorial2")){
	        	fragment.mContent=Tutorial2;
	        	fragment.imageId=image[1];
	        }else if(content.equals("Tutorial3")){
	        	fragment.mContent=Tutorial3;
	        	fragment.imageId=image[2];
	        }else{
	        	fragment.mContent=Tutorial4;
	        	fragment.imageId=image[3];
	        	lastOne=true;
	        }
	        
           
	        return fragment;
	    }

	    private String mContent = "???";
	    private int imageId;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        sf=getActivity().getSharedPreferences(TRACKING, 0);
	        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
	            mContent = savedInstanceState.getString(KEY_CONTENT);
	        }
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    	Resources r = Resources.getSystem();
	    	Display myDisplay = getActivity().getWindowManager().getDefaultDisplay();
	        int screen_width =myDisplay.getWidth();
	        int screen_height = myDisplay.getHeight();
	        float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screen_width/2, r.getDisplayMetrics());
	        float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,screen_height*3/10,r.getDisplayMetrics());
	        float text_height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,screen_height/5,r.getDisplayMetrics());
	        TextView text = new TextView(getActivity());
	        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	        textParams.setMargins(15, 15, 15, 15);
	        text.setLayoutParams(textParams);

	        text.setText(mContent);
	        text.setTextColor(getResources().getColor(R.color.green));
	        text.setTextSize(10 * getResources().getDisplayMetrics().density);
	        text.setPadding(10, 10, 10, 10);
	        text.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border));
	        
	        View line = new View(getActivity());
	        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 5);
	        lineParams.setMargins(15, 0, 15, 0);
	        line.setLayoutParams(lineParams);
	        line.setBackgroundColor(getResources().getColor(R.color.white));
	       

	        ImageView image = new ImageView(getActivity());
	        image.setBackgroundResource(imageId);
	        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams((int)width,(int)(height-15));
	        imageParams.setMargins(15, 15, 15, 15);
	        image.setLayoutParams(imageParams);
	        
	        TextView start = new TextView(getActivity());
	        LinearLayout.LayoutParams startParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
	        startParams.setMargins(15, 0, 15, 0);
	        start.setLayoutParams(startParams);
	        start.setText("START NOW");
	        start.setBackgroundDrawable(getResources().getDrawable(R.drawable.trickiestpart_corner_green));
	        start.setTextColor(getResources().getColor(R.color.white));
	        start.setTextSize(20);
	        start.setClickable(true);
	        start.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if(sf.getBoolean(PREF_INITIAL_RUN, true)){
						sf.edit().putBoolean(PREF_INITIAL_RUN, false).commit();			
						Intent i = new Intent(getActivity(),FirstTimeTrackingSetting.class);
						startActivity(i);
						}else{
							Intent i = new Intent(getActivity(),Today.class);
							startActivity(i);
						}
				}
	        	
	        });
	        
	        LinearLayout layout = new LinearLayout(getActivity());
	        layout.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	        layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
	        layout.setGravity(Gravity.CENTER_HORIZONTAL);
	        layout.setOrientation(LinearLayout.VERTICAL);
	        layout.addView(text);
	        layout.addView(line);
	        layout.addView(image);
            if(lastOne){
            	layout.addView(start);
            	lastOne=false;
            }else {
            	lastOne=false;
            }
	        return layout;
	    }

	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        outState.putString(KEY_CONTENT, mContent);
	    }
}
