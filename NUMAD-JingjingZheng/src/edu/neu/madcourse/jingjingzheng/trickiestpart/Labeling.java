package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Labeling extends FragmentActivity implements OnClickListener{
	  private static final String TAG =" labeling";
	  private final String SPREFSKEY = "edu.neu.madcourse.shanshanhuang.trickiestpart.Labeling";
	  private final String LABELING = "Labeling";
	  String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oils or Fats"};
	  private SharedPreferences sf;
	  public final static String START_LABELING="start_labeling";
	  public final static String TAG_ISEDITING = "editing";
	  public final static String TAG_FILENAME = "FileName";
	  public final static String FILE_NAME_FORMAT = "yyyy-MM-dd_HH+mm+ss";
	  public final static String HISTORY_DATE_FORMAT = "yyyy-MM-dd";
	  public final static String HISTORY_TIME_FORMAT = "HH+mm+ss";
	  /** Called when the activity is first created. */
	  private Chart_View cir;
	  private String mOrigin = "";
	  private String mFileName;
	  private String static_data;
	  private ArrayList<String> categories=new ArrayList<String>();

	  private Date mDate;

	  private Boolean mIsEditing = false;
	  private Boolean fromOnCreate;

	  ImageView imageView;
	  Bitmap image;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Log.d(TAG,"onCreate");
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Display myDisplay =getWindowManager().getDefaultDisplay();
		int width =myDisplay.getWidth();
		int height=myDisplay.getHeight();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
			    width, height/3);
		setContentView(R.layout.trickiestpart_labeling);
		image = (Bitmap)getIntent().getParcelableExtra("Bitmap");
		imageView = (ImageView) findViewById(R.id.labeling_image);
		imageView.setImageBitmap(image);
		imageView.setLayoutParams(params);
		imageView.setOnClickListener(this);
		
		Chart_View circle = (Chart_View)findViewById(R.id.circle);
		FrameLayout.LayoutParams frame_params = new FrameLayout.LayoutParams(width*2/3,height);
		circle.setLayoutParams(frame_params);
		
		View backBtn = findViewById(R.id.labeling_back);
		backBtn.setOnClickListener(this);
		View saveBtn = findViewById(R.id.labeling_save);
		saveBtn.setOnClickListener(this);
		
		sf = this.getSharedPreferences(LABELING, MODE_PRIVATE);
		
		final Chart_View c = (Chart_View) this.findViewById(R.id.circle);
	    cir = c;

	    
		
		if(getIntent().getBooleanExtra(START_LABELING, false)){
			Log.d(TAG,"start labeling!");
			
	    	categories = getIntent().getStringArrayListExtra("categories");
	    	
	    	mIsEditing = sf.getBoolean(TAG_ISEDITING, false);
		}
    }

	public void onResume(){
		  Log.d(TAG,"onResume");
	    super.onResume();

        if(getIntent().getBooleanExtra(START_LABELING, false)){
	    // coming from category selection
	    if(static_data == null){
	      Log.d(TAG, "coming from category selection, no data given");
	      // delete button shouldn't exist if we're just starting to add a chart.
//	     
	      addCategories(categories);
	    }
	    // coming from history
	    else{
	      Log.d(TAG, "coming from history, have data");
	      // get the array of categories from the extra
	      ArrayList<Category> cats = 
	          JSONParser.getCatListFromString(static_data, this);
	      // set the circles' categories and touchpoints
	      this.selectCategoryObjects(cats);
	      cir.setmCategory(cats);
	      cir.setmPoints(Category.getTouchPointsFromCategoryList(cats));
	      cir.linkPointsAndCategories();
	    }

	    // make the date format for the title

//	    SimpleDateFormat fileDate =
//	      new SimpleDateFormat(FILE_NAME_FORMAT, Locale.US);
//
//	   // dates in java are terrible.  what the fuck is a parseposition.
//	    if (!mFileName.equals("")) {
//	      Log.d(TAG, "@@@@no filename");
//	      String filename = mFileName;
//	      mDate = fileDate.parse(filename.replace(".txt", ""), 
//	          new ParsePosition(0));
//	      /* mDate = titleDate.format(mFileName.replace(".txt", "")); */
//	    }
//
//	    String title = "New Chart";
//	    if (mDate != null) { 
//	      title = DateFormat.getDateInstance().format(mDate);
//	      /* title = titleDate.format(mDate); */
//	    }
//	    TextView tv = (TextView)this.findViewById(R.id.addChart_Title);
//	    tv.setText(title);

	  }
	 }

	
	 /**
	   * Gets the category data from a given filename
	   * @param filename The name of the file to get data from.
	   */
	  private String getDataFromFile(String filename) {
	    String data = "ERROR";
	    Log.d(TAG, "ReadData");
	    try {
	      Log.d("readData", "File: "+mFileName);
	      FileInputStream fis = this.openFileInput(mFileName);
	      BufferedReader r = new BufferedReader(new InputStreamReader(fis));
	      data = r.readLine();
	      r.close(); 
	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    return data;
	  }


	
	 private void selectCategoryObjects(ArrayList<Category> cats) {
		    for (Category cat : cats) {
		      selectCategory(cat.getCategory());
		    }
		  }

		  private void addCategories(ArrayList<String> categories) {
			Log.d(TAG,"categories selected : " + categories.toString());
		    for (String cat : categories) {
		      addCategory(cat);
		    }
		   // cleanCategorySF();
		  }
		  
		  private void cleanCategorySF(){
			  for(int i=0;i<select_image.length;i++){
				  sf.edit().putBoolean(select_image[i], false).commit();
			  }
		  }

		  private void selectCategory(String category) {
		    if (category.equals("Grains")) {
		      cir.setProteinSelect(this.findViewById(R.id.grain_view));
		    } else if (category.equals("Veggies")) {
		      cir.setVegetableSelect(this.findViewById(R.id.vegetable_view));
		    } else if ( category.equals("Fruits")) {
		      cir.setFruitSelect(this.findViewById(R.id.fruit_view));
		    } else if (category.equals("Dairy")) {
		      cir.setDairySelect(this.findViewById(R.id.dairy_view));
		    } else if (category.equals("Animal Protein")) {
		      cir.setGrainSelect(this.findViewById(R.id.protein_view));
		    } else if (category.equals("Nuts")) {
		      cir.setOilSugarSelect(this.findViewById(R.id.nuts_view));
		    }else if (category.equals("Oils or Fats")) {
		        cir.setOilSugarSelect(this.findViewById(R.id.oil_view));
		    }else if (category.equals("Sweets")){
		    	cir.setSweetsSelect(this.findViewById(R.id.sweets_view));
		    }
		    
		  }

		  private void addCategory(String category) {
		    if (category.equals("Grains")) {
		      final View grain_view = (View) this.findViewById(R.id.grain_view);
		      onGrainClicked(grain_view);
		    } else if (category.equals("Veggies")) {
		      final View vegetable_view = (View) this.findViewById(R.id.vegetable_view);
		      onVegetableClicked(vegetable_view);
		    } else if ( category.equals("Fruits")) {
		      final View fruit_view = (View) this.findViewById(R.id.fruit_view);
		      onFruitClicked(fruit_view);
		    } else if (category.equals("Dairy")) {
		      final View dairy_view = (View) this.findViewById(R.id.dairy_view);
		      onDairyClicked(dairy_view);
		    } else if (category.equals("Animal Protein")) {
		      final View protein_view = (View) this.findViewById(R.id.protein_view);
		      onProteinClicked(protein_view);
		    } else if (category.equals("Oils or Fats")) {
		      final View oil_view = (View) this.findViewById(R.id.oil_view);
		      onOilSugarClicked(oil_view);
		    } else if (category.equals("Nuts")){
		    	final View nuts_view = (View) this.findViewById(R.id.nuts_view);
		    	onNutsClicked(nuts_view);
		    } else if (category.equals("Sweets")){
		    	final View sweets_view = (View) this.findViewById(R.id.sweets_view);
		    	onSweetsClicked(sweets_view);
		    }
		  }


		  public void onProteinClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onProteinClicked(v);
		    }
		  }

		  public void onVegetableClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onVegetableClicked(v);
		    }
		  }

		  public void onDairyClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onDairyClicked(v);
		    }
		  }

		  public void onFruitClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onFruitClicked(v);
		    }
		  }

		  public void onGrainClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onGrainClicked(v);
		    }
		  }

		  public void onOilSugarClicked(View v){
		    if (mIsEditing) {
		      vibrate(25);
		      cir.onOilSugarClicked(v);
		    }
		  }
		  
		  public void onNutsClicked(View v){
			    if (mIsEditing) {
			      vibrate(25);
			      cir.onNutsClicked(v);
			    }
			  }
		  
		  public void onSweetsClicked(View v){
			    if (mIsEditing) {
			      vibrate(25);
			      cir.onSweetsClicked(v);
			    }
			  }

		  private void vibrate(int ms) {
		    Vibrator v = 
		      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		    v.vibrate(ms);
		  }

		  public void onSaveClicked(View v){
		    if (cir.getChartData().length() == 0) {
		      Toast.makeText(getBaseContext(),
		          "Add some categories before saving your chart!",
		          Toast.LENGTH_SHORT).show();
		      return;
		    }
		    // clicked save
		  	vibrate(25);
		    if (mIsEditing) {
		      Log.d(TAG, "Save clicked, saving...");
		      JSONArray chartData = cir.getChartData();
		      String data = parseData(chartData);
		      save(data, mFileName);
		      mIsEditing = false;
		      static_data = getDataFromFile(mFileName);
		      cir.setIsEditing(false);
		      fromOnCreate = true;
		          onResume();
		    } else { // clicked edit
		      Log.d(TAG, "Edit clicked, editing...");
		      mIsEditing = true;
		      cir.setIsEditing(true);
		      cir.invalidate();
		      fromOnCreate = true;
		      onResume();
		    }
		  }

		public String parseData(JSONArray jArray){
		  String data = jArray.toString();
		  return data;
		}

		/**
		 * Delete the chart and set the deselects for all categories
		 * @param v
		 */
		public void onDeleteClicked(View v){
		  boolean deleted;
		  vibrate(25);
		  if(!mFileName.equals("")){
		    File file = new File(this.getFilesDir(), mFileName);
		    deleted = file.delete();
		  }else {
		    deleted = true;
		  }
		  // show a toast, tell the user if it succeeded.
		  if (deleted) {
		    Toast.makeText(getBaseContext(), "Entry deleted!", Toast.LENGTH_SHORT).show();
		  } else {
		    Toast.makeText(getBaseContext(), "Error deleting entry.",
		        Toast.LENGTH_LONG).show();
		  }
		  finish();
		}

		/**
		 * Gets the current date and time based on the system settings/timezone
		 * @return String formattedDate
		 */
		public String getDateTime(){
		  Calendar c = Calendar.getInstance();
		  SimpleDateFormat df = new SimpleDateFormat(FILE_NAME_FORMAT);
		  String formattedDate = df.format(c.getTime());

		  return formattedDate;
		}
		/**
		 * Gets the Current date for file name
		 * @return String date - format YYYY_MM_DD
		 */
		public String getDate(){
		  Calendar c = Calendar.getInstance();
		  SimpleDateFormat df = new SimpleDateFormat(FILE_NAME_FORMAT);
		  String formattedDate = df.format(c.getTime());    
		  return formattedDate;
		}

		/**
		 * Save file to internal memory
		 * @param data - Stored data
		 */
		public void save(String data, String FileName){
		  String date = getDate();
		  if(FileName.equals("")){         // generates name of file
		    FileName = date+".txt";
		    mFileName = FileName;
		    Log.d(TAG, "making a new filename...");

		    SimpleDateFormat fileDate =
		      new SimpleDateFormat(FILE_NAME_FORMAT, Locale.US);
		    mDate = fileDate.parse(mFileName.replace(".txt", ""), 
		          new ParsePosition(0));

		  }else if (!FileName.equals("")){    // Deletes current file
		    Log.d(TAG, "saving to a file that already exists");
		    File file = new File(this.getFilesDir(), FileName);
		    boolean deleted = file.delete();
		  }
		  Log.d(TAG, "save() filename: " + FileName);
		  try{
		    FileWriter write = new FileWriter(this.getFilesDir() + "/" + FileName, true);
		    write.write(data+"\r\n"); //adds new line.
		    write.close();
		    // if we get here with no exceptions, we did it!
		    Toast.makeText(getBaseContext(), "Entry saved!", 
		          Toast.LENGTH_SHORT).show();

		  } catch (FileNotFoundException e) {
		  Toast.makeText(getBaseContext(), "Uh-Oh.  We couldn't save the chart. Try Again?", 
		          Toast.LENGTH_SHORT).show();
		    e.printStackTrace();
		  } catch (IOException e) {
		  Toast.makeText(getBaseContext(), "Oops! We couldn't read from your phone. Try Again!", 
		          Toast.LENGTH_SHORT).show();
		    e.printStackTrace();
		  }
		}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.labeling_back:
			finish();
			break;
		case R.id.labeling_save:
			Toast.makeText(this, "saving image", Toast.LENGTH_SHORT).show();
			Intent summary = new Intent(this,Summary.class);
			startActivity(summary);
			break;
		case R.id.labeling_image:
//			DialogFragment s = new ShowTypeDialogFragment();
//			s.show(getSupportFragmentManager(), "food_types");
			Intent i = new Intent(this,CategorySelection.class);
			startActivity(i);
			break;
		}
	}
}
