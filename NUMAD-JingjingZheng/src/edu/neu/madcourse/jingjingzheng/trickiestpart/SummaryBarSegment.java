package edu.neu.madcourse.jingjingzheng.trickiestpart;
// Container for segments
public class SummaryBarSegment {
  //  percentage is the percent value of this segment, color is the color
  private double mValue;
  private int mColor;

  public SummaryBarSegment(Category c) {
    this.mValue = (SummaryBar.categoryToPercentage(c));
    this.mColor = c.getColor();
  }

  public SummaryBarSegment(double value, int color) {
    this.mValue = value;
    this.mColor = color;
  }

  public double getValue() {
    return mValue;
  }
  public int getColor() {
    return mColor;
  }
  public void setValue(double val) {
    this.mValue = val;
  }
  public void setColor(int color) {
    this.mColor = color;
  }
  
  public String getString(){
	  return "" + mValue;
  }
}
