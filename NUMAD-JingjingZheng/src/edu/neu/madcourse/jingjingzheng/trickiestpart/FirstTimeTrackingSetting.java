package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class FirstTimeTrackingSetting extends Activity implements OnClickListener{
	public static final String TAG="First time tracking setting";
	public static final String TRACKING = "tracking_setting";
	public static final String GRAINS = "grains";
	public static final String DAIRY = "dairy";
	public static final String ANIMAL_PROTEIN="animal_protein";
	public static final String VEGGIES= "veggies";
	public static final String FRUITS = "fruits";
	public static final String SWEETS="sweets";
	public static final String LEGUMES_NUTS="legumes_nuts";
	public static final String OILS_FATS="oils_fats";
	private SharedPreferences sf;
	
	
	Integer[] imageIDs = {
            R.drawable.grains,
            R.drawable.dairy,
            R.drawable.animal_protein,
            R.drawable.veggies,
            R.drawable.fruits,
            R.drawable.sweets,
            R.drawable.legumes_nuts,
            R.drawable.oils_fats
    };
	
	String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oils or Fats"};
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.trickiestpart_firsttime_tracking_setting);
		Resources r = Resources.getSystem();
    	Display myDisplay = getWindowManager().getDefaultDisplay();
        int screen_width =myDisplay.getWidth();
        int screen_height = myDisplay.getHeight();
		ImageView backButton = (ImageView)findViewById(R.id.firsttime_tracking_back);
		backButton.setOnClickListener(this);
		ImageView nextButton = (ImageView)findViewById(R.id.firsttime_tracking_next);
		nextButton.setOnClickListener(this);
		sf = this.getSharedPreferences(TRACKING, MODE_PRIVATE);
		resetSF();
		GridView gridView = (GridView) findViewById(R.id.gridview);
		float verticalSpacing = screen_height/10;
		float horizontalSpacing = screen_width/40;
		gridView.setVerticalSpacing((int)verticalSpacing);
		gridView.setHorizontalSpacing((int)horizontalSpacing);
        gridView.setAdapter(new ImageAdapter(this));
 
        gridView.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, 
            View v, int position, long id) 
            {                
              
                Log.d(TAG,"select image: " + select_image[position]);
                if(sf.getBoolean(select_image[position], false)){
                	v.setBackgroundColor(getResources().getColor(R.color.border));
                	sf.edit().putBoolean(select_image[position], false).commit();
                }else{
                	sf.edit().putBoolean(select_image[position],true).commit();  
                	v.setBackgroundColor(getResources().getColor(R.color.clicked_border));
                	Toast.makeText(getApplicationContext(), "You've selected: "+select_image[position],1000).show();
                }
            }
        });        
	}
	
	public void resetSF(){
		for(String str:select_image){
			sf.edit().putBoolean(str, false).commit();
		}
	}
	
	public class ImageAdapter extends BaseAdapter 
    {
        private Context context;
 
        public ImageAdapter(Context c) 
        {
            context = c;
        }
 
        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }
 
        //---returns the ID of an item--- 
        public Object getItem(int position) {
            return position;
        }
 
        public long getItemId(int position) {
            return position;
        }
 
        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent) 
        {
            ImageView imageView;
            if (convertView == null) {
            	Resources r = Resources.getSystem();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
                
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams((int)px,(int)px));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8,8,8,8);
            } else {
                imageView = (ImageView) convertView;
            }
            
            imageView.setImageResource(imageIDs[position]);
            imageView.setBackgroundColor(getResources().getColor(R.color.border));
            //imageView.setBackgroundResource(R.drawable.border);
            return imageView;
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.firsttime_tracking_back:
			finish();
			break;
		case R.id.firsttime_tracking_next:
			ArrayList<Boolean> selected = new ArrayList<Boolean>();
			int m = 0;
			for(String str: select_image){
				if(sf.getBoolean(str, false)){
					Intent i = new Intent(this,Today.class);
					startActivity(i);
					break;
				}else if(m == select_image.length-1){
					Toast.makeText(getApplicationContext(), "Please select at least one type.",1000).show();
				}else {
					m++;
				}
				
			}
		
		}
	}    

}
