package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.slidingmenu.lib.app.SlidingActivity;

import edu.neu.madcourse.jingjingzheng.trickiestpart.SummaryBar;
import edu.neu.madcourse.jingjingzheng.trickiestpart.SummaryBarSegment;
import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.ClickListenerForScrolling;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.SizeCallbackForMenu;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class Summary extends SlidingActivity {
	private Summary thisActivity = this;
	private Context mContext;
    private MyHorizontalScrollView scrollView;
    private View menu;
    private View app;
    private ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    private int btnWidth;
    private static final String TAG = "edu.neu.madcourse.shanshanhuang.trickiestpart.Summary";
    private SummaryBar mProfileBar;
    HashMap<Integer, Double> allCategoryAverages = new HashMap<Integer, Double>();
    private HashMap<Integer, String> IntToStrOfColor = new HashMap<Integer, String>();
    private ArrayList<SummaryBarSegment> mProfileBarSegments = 
      new ArrayList<SummaryBarSegment>();
    private ArrayList<Integer> mSegmentColors = 
      new ArrayList<Integer>();
    //private ArrayList<Category> selected=new ArrayList<Category>();
    public static final String TRACKING="tracking_setting";
    public static final String MEAL_TYPE="meal_type";
    public static final String GRAINS = "grains";
	public static final String DAIRY = "dairy";
	public static final String ANIMAL_PROTEIN="animal_protein";
	public static final String VEGGIES= "veggies";
	public static final String FRUITS = "fruits";
	public static final String SWEETS="sweets";
	public static final String NUTS="nuts";
	public static final String OILS_FATS="oils_fats";
	//private Summary thisActivity = this;
	private SharedPreferences sf;
	ArrayList<String> trackingList;
	private static String title;
	String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oil or Fats"};
	private ArrayList<String> color = new ArrayList<String>();
	//private float[] items = { (float) 21.0, (float) 20.0, (float) 10.0, (float) 10.0, (float) 10.0, (float) 10.0, (float) 10.0, (float) 10.0, (float) 10.0, (float) 10.0 };
	//private float[] items = { (float) 20.0, (float) 20.0, (float) 10.0 };
	private ArrayList<Float> item = new ArrayList<Float>();
	private int total = 150;
	private int radius;
	private int strokeWidth = 0;
	private String strokeColor = "#000000";
	private float animSpeed = (float) 2;
	private String mFileName;
	private ArrayList<String> selected_cats_str;
	private ArrayList<Category>selected_cats;
	private float radiumLeft = 0;
	private ArrayList<String>tracklistAreSelected = new ArrayList<String>();

	private PieChartView pieChart;

	private TextView textInfo;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		LayoutInflater inflater = LayoutInflater.from(this);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		sf = this.getSharedPreferences("SCREEN_WIDTH", 0);
		int screen_width = sf.getInt("SCREEN_WIDTH", 0);
		radius = screen_width /4;
        scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.trickiestpart_scroll_view, null);
    	
      /// added code for touch to slide
        setContentView(R.layout.trickiestpart_today);
  	    setBehindContentView(R.layout.trickiestpart_touch_menu);
  	    ListView touchMenuList =(ListView) findViewById(R.id.touch_menu_list);
  	    initMenuListView(this,touchMenuList,android.R.layout.simple_list_item_1);
  	    getSlidingMenu().setBehindOffset(80);
        // added code here 
  	    
        setContentView(scrollView);
        trackingList = getTrackingTypes();
        menu = inflater.inflate(R.layout.trickiestpart_menu, null);
        
        app = inflater.inflate(R.layout.trickiestpart_summary, null);
        
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.summary_tabBar);

        ListView listView = (ListView) menu.findViewById(R.id.menu_list);
        initMenuListView(this, listView, android.R.layout.simple_list_item_1);

        btnSlide = (ImageView) tabBar.findViewById(R.id.summary_BtnSlide);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));
        mFileName = getIntent().getStringExtra("mFileName");
        if(getIntent().getBooleanExtra("FROM_TABVIEW", false)){
        	mFileName = getIntent().getStringExtra("FileName");
        }
        title = getIntent().getStringExtra(MEAL_TYPE);
        //selected_cats = getIntent().getStringArrayListExtra("CUR_CHOSENCAT");
        final View[] children = new View[] { menu, app };

        // Scroll to app (view[1]) when layout finished.
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
        
        ImageView btnDone = (ImageView)findViewById(R.id.summary_done);
        btnDone.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Summary.this,Today_TabView.class);
				i.putExtra(MEAL_TYPE, title);
				Log.d(TAG,"current tab is : "+ title);
				startActivity(i);
			}
        	
        });
       
        IntToStrOfColorFun();
        //selected = Global.getCategoryObjects();
        GenPieChart();
//        for(int i = 0; i < mProfileBarSegments.size(); i++){
//        	Log.d(TAG, "color in summary" + mProfileBarSegments.get(i).getColor());
//        	item.add((float)mProfileBarSegments.get(i).getValue());
//        }
        
		textInfo = (TextView) findViewById(R.id.text_item_info);
		pieChart = (PieChartView) findViewById(R.id.parbar_view);
		pieChart.setItemsSizes(item);
		pieChart.setItemsColors(color);
		pieChart.setRaduis(radius);
		pieChart.setStrokeWidth(strokeWidth);
		pieChart.setStrokeColor(strokeColor);
		pieChart.setSeparateDistence(15);
		
		pieChart.setOnItemSelectedListener(new OnPieChartItemSelectedLinstener() {

			public void onPieChartItemSelected(PieChartView view, int position, String colorRgb, float size, float rate, boolean isFreePart, float rotateTime) {
				// TODO Auto-generated method stub
				Log.e("Main", "onClicked item : " + position);
				if (isFreePart) {
					//textInfo.setText( "Category: \n" + selected_cats.get(position).getCategory() + "\n\n" + "Rate: \n " + (int)(rate * 100) + "%");
					textInfo.setText( "Tracking: \n" + tracklistAreSelected.get(position) + "\n\n" + "Rate: \n " + (int)(rate * 100) + "%");
				} else {
					textInfo.setText( "Tracking: \n" + tracklistAreSelected.get(position) + "\n\n" + "Rate: \n " + (int)(rate * 100) + "%");
					//textInfo.setText("item position: " + position + "\r\nitem size: " + size + "\r\nitem color: " + colorRgb + "\r\nitem rate: " + rate);
				}
				textInfo.setVisibility(View.VISIBLE);
				Animation myAnimation_Alpha = new AlphaAnimation(0.1f, 1.0f);
				myAnimation_Alpha.setDuration((int) (3 * rotateTime));
				textInfo.startAnimation(myAnimation_Alpha);
			}


		});
	}
	
	 private void vibrate(int ms) {
		    Vibrator v = 
		      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		    v.vibrate(ms);
		  }
	 
	 private void GenPieChart(){
		 thisActivity.mProfileBar = new SummaryBar(thisActivity);
		 thisActivity.mProfileBar.setCurrentFile(mFileName);
		 thisActivity.mProfileBarSegments = mProfileBar.averageAllSegments(); 
		 Log.d(TAG, "### mProfileBarSegments: " + this.mProfileBarSegments.size());
		   
		 //Log.d(TAG, "color value by getResource" + Integer.parseInt(getResources().getColor(R.color.Protein), 16));
		 thisActivity.setHashMapCategoryAverages();
		 thisActivity.updateHashMapCategoryAverages();
		 //Log.d(TAG, "mfILENAME IN SUMMARY: " + mFileName);
		 selected_cats = thisActivity.mProfileBar.getAllCategoriesFromFiles();
		 
		 for(int i = 0; i <selected_cats.size(); i++){
			 if(trackingList.contains(selected_cats.get(i).getCategory())){
				 tracklistAreSelected.add(selected_cats.get(i).getCategory());
				 item.add((float)mProfileBarSegments.get(i).getValue());
				 color.add(IntToStrOfColor.get(mProfileBarSegments.get(i).getColor()));
				 radiumLeft +=(float)mProfileBarSegments.get(i).getValue();
			 }
		 }
		 radiumLeft = 1 - radiumLeft;
		 if(radiumLeft > 0){
			 tracklistAreSelected.add("OTHERS");
			 item.add(radiumLeft);
			 color.add("#afafaf");
		 }

	 }
	 
//   <color name="Protein">#ff1673</color>
//   <color name="Vegetable">#00c978</color>
//   <color name="Dairy">#ffa500</color>
//   <color name="Oil_Sugar">#ff0000</color>
//   <color name="Fruit">#b300b3</color>
//   <color name="Grain">#be5518</color>
//   <color name="Legumes_Nuts">#1a31d3</color>
//   <color name="Sweets">#681a1a</color>
	 	private void IntToStrOfColorFun(){
	 		IntToStrOfColor.put(getResources().getColor(R.color.Protein), "#ff1673");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Vegetable), "#00c978");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Dairy), "#ffa500");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Grain), "#ff0000");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Fruit), "#b300b3");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Oil_Sugar), "#be5518");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Legumes_Nuts), "#1a31d3");
	 		IntToStrOfColor.put(getResources().getColor(R.color.Sweets), "#681a1a");
	 	} 
		  private void setHashMapCategoryAverages(){
		    double base = 0;
		    allCategoryAverages.put(getResources().getColor(R.color.Protein), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Vegetable), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Dairy), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Grain), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Fruit), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Oil_Sugar), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Legumes_Nuts), base);
		    allCategoryAverages.put(getResources().getColor(R.color.Sweets), base);
		  }
//
		  private void updateHashMapCategoryAverages() {
		    Log.d(TAG, "profile size: " + mProfileBarSegments.size());
		    Log.d(TAG, "BEFORE HASHMAP content" + allCategoryAverages.toString());
		    if(mProfileBarSegments.size() == 1){
		      SummaryBarSegment p = mProfileBarSegments.get(0);
		      allCategoryAverages.remove(p.getColor());
		      allCategoryAverages.put(p.getColor(), 1.00);
		    }
		    else if(mProfileBarSegments.size() > 1 ){
		    	//int IndexOfSumBarSeg = 0;
		    	//int IndexOfColor = 0;
		      for (SummaryBarSegment p : mProfileBarSegments){
		        Log.d(TAG, "Value : " + p.getValue());
		        allCategoryAverages.remove(p.getColor());
		        //colors[IndexOfSumBarSeg] = IntToStrOfColor.get(p.getColor());
		        //color.add(IntToStrOfColor.get(p.getColor()));
		        //IndexOfSumBarSeg++;
		        allCategoryAverages.put(p.getColor(), p.getValue());
		        
		      }
		    }
		  }
		  
	//initialize menu list items
    public  void initMenuListView(Context context,ListView listView,int layout){
     	final String[] arr = {"My Meal", "Tracking Setting","Help","Acknowledgment"};
     	final Integer[] images = { R.drawable.meal,
                 R.drawable.settings, R.drawable.help ,R.drawable.comments};
         	List<RowItem> rowItems;
         	rowItems = new ArrayList<RowItem>();
             for (int i = 0; i < arr.length; i++) {
                 RowItem item = new RowItem(images[i], arr[i]);
                 rowItems.add(item);
             }
             CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
                     R.layout.trickiestpart_list_item, rowItems);
             listView.setAdapter(menu_adapter);
             listView.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view, int position,
    					long id) {
    				// TODO Auto-generated method stub
    				Context context = view.getContext();
    				String str = arr[position];
    				if(str.equals("My Meal")){
    					vibrate(25);
    					startActivity(new Intent(context,Today.class));
    				}else if(str.equals("Tracking Setting")){
    					startActivity(new Intent(context,Tracking_Setting.class));
    				}else if(str.equals("Help")){
    					vibrate(25);
    					//startActivity(new Intent(context,TutorialActivity.class));
    					startActivity(new Intent(context,Tutorial.class));
    					Log.d(TAG,"no matcing item!");
    				}else {
    					vibrate(25);
    					startActivity(new Intent(context,Acknowledgment.class));
    				}
    				
    			}
     		
     	});
    }
    
    
    public ArrayList<String> getTrackingTypes(){
    	sf = this.getSharedPreferences(TRACKING, MODE_PRIVATE);
    	ArrayList<String> tracking = new ArrayList<String>();
    	for(String str:select_image){
    		if(sf.getBoolean(str, false)){
    			tracking.add(str);
    		}
    	}
    	return tracking;
    }


}
