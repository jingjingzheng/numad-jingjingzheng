package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.json.JSONArray;

import edu.neu.madcourse.jingjingzheng.R;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public final class TabViewFragment extends Fragment {
	private static final String KEY_CONTENT = "TestFragment:Content";
	public static final String TAG_MAPPING="Mapping";
	private static final String LABELING = "labeling";
	public static final String MEAL_TYPE="meal_type";
	private static final String START_LABELING="start_labeling";
	private static final String TAG ="TavViewFragment";
	public SharedPreferences sf;
//	public SharedPreferences sharedPref;
    private  String key;
    ArrayList<Bitmap> myPhotos = new ArrayList<Bitmap>();
    ArrayList<String> myImageFileNames = new ArrayList<String>();
    private static HashMap<String,String> mImageLabelingMap;
    File[] listFile;
    public static TabViewFragment newInstance(String content) {
        TabViewFragment fragment = new TabViewFragment();
        fragment.key = content;
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sf= this.getActivity().getSharedPreferences("SCREEN_WIDTH", 0);
        mImageLabelingMap = new HashMap<String,String>();
        if(Global.getMapObjects()!=null){
        	mImageLabelingMap = Global.getMapObjects();
        }
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            key = savedInstanceState.getString(KEY_CONTENT);
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, key);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
    	//Get the size of the screen 
    	Resources r = Resources.getSystem();
        Display myDisplay = getActivity().getWindowManager().getDefaultDisplay();
    	int screen_width =myDisplay.getWidth();
    	sf.edit().putInt("SCREEN_WIDTH", screen_width).commit();
    	//set up linear layout params, set the layout width to be the 3/4 of screen width.
    	LinearLayout layout = new LinearLayout(getActivity());
    	float layout_width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screen_width*3/4, r.getDisplayMetrics());
    	LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(lp);        
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
        
        File file = new File(Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+key+File.separator+timeStamp);
        Log.d("TEST","MyCameraApp"+File.separator+key+File.separator+timeStamp);
	   // File file = new File(path, "MyCameraApp"+File.separator+key+File.separator+timeStamp);
	    if(file.exists()){
	    	Log.d("TestFragment","files in pictures directory:"+ file.listFiles().length);
	    	listFile = file.listFiles();
	    	PhotoDishesUtils.loadImages(listFile,myPhotos,myImageFileNames);
	    }
	    
        ListView listView = new ListView(getActivity());
        listView.setAdapter(new PhotoListAdapter(getActivity(),myPhotos));
        listView.setDivider(this.getResources().getDrawable(R.drawable.trickiestpart_background));
        listView.setDividerHeight(30);       
        
        listView.setOnItemClickListener(new ListView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Log.d("test","inside on click listener");
				Context context = view.getContext();
				Bitmap image = myPhotos.get(position);
				Log.d(TAG, "Bitmap position: " + String.valueOf(position));
				Log.d(TAG,"Bitmap name:" + myImageFileNames.get(position));			    
				Log.d(TAG,"image onclick is: "+ image);
				Log.d(TAG,"image string:" + String.valueOf(image));
				if(checkLabeled(position)){
					String fileName = mImageLabelingMap.get(myImageFileNames.get(position));
					Intent summary = new Intent(context,Summary.class);
					summary.putExtra("FileName",fileName);
					summary.putExtra("FROM_TABVIEW",true);
					startActivity(summary);
				}else {
				Intent labeling = new Intent(context,AddChart.class);
				labeling.putExtra("ImageFileName",myImageFileNames.get(position));
				labeling.putExtra("Bitmap",image);
				labeling.putExtra(MEAL_TYPE,key);
				startActivity(labeling);
				}
			}});
        
        layout.addView(listView);

        return layout;
    }

    
  public class PhotoListAdapter extends BaseAdapter{
	  Context c;
	  private LayoutInflater mInflater;
	  ArrayList<Bitmap> myPhotos = new ArrayList<Bitmap>();
	  public PhotoListAdapter(Context mContext,ArrayList<Bitmap> bitmapArrayList){
		  c = mContext;
		  myPhotos = bitmapArrayList;
		  mInflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	  }
	  @Override
	  public int getCount() {
	  	// TODO Auto-generated method stub
	  	return myPhotos.size();
	  }

	  @Override
	  public Object getItem(int position) {
	  	// TODO Auto-generated method stub
	  	return position;
	  }

	  @Override
	  public long getItemId(int position) {
	  	// TODO Auto-generated method stub
	  	return position;
	  }

	  @Override
	  public View getView(int position, View view, ViewGroup parent) {
	  	// TODO Auto-generated method stub
          ImageView imageView;
          //set the image width to be 3/4 of the screen width, height to be 200
          Resources r = Resources.getSystem();
    	  Display myDisplay = getActivity().getWindowManager().getDefaultDisplay();
    	  int screen_width =myDisplay.getWidth();
          float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screen_width*9/10, r.getDisplayMetrics());
          float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,200,r.getDisplayMetrics());
          
          if (view == null) {
              view = mInflater.inflate(R.layout.trickiestpart_photolist, null);   
              //view.setBackgroundResource(R.drawable.photolist_entry_middle);
             view.setPadding(20, 20, 20, 20);
          } else {
        	  imageView = (ImageView)view.findViewById(R.id.photo_list);
          }
          imageView = (ImageView)view.findViewById(R.id.photo_list);
          imageView.setLayoutParams(new LinearLayout.LayoutParams((int)width,(int)height));
          if(checkLabeled(position)){
        	  Log.d(TAG,"position is :" + position + "labeled: yes"+"filename: "+ myImageFileNames.get(position) );
        	  Bitmap mBitmap = myPhotos.get(position);
              BitmapFactory.Options ops = new BitmapFactory.Options();
              //ops.inSampleSize=;
              Bitmap checkBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.label_check);
              mBitmap = getBitmapOverlay(mBitmap,checkBitmap,mBitmap.getWidth()/3,0);
              imageView.setImageBitmap(mBitmap);
          }else{
        	  imageView.setImageBitmap(myPhotos.get(position));
          }
          
          return view;
      }
  }
  
  public static Bitmap getBitmapOverlay(Bitmap bmp1,Bitmap bmp2,int left, int top ){
	  Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),bmp1.getHeight(),bmp1.getConfig());
	  Canvas canvas = new Canvas(bmOverlay);
	  Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
	  canvas.drawBitmap(bmp1, 0, 0,paint);
	  canvas.drawBitmap(bmp2, left, top,paint);
	  return bmOverlay;
  }
  
  public Boolean checkLabeled(int position){
	  String key = myImageFileNames.get(position);
	  Log.d(TAG,"check labeled:"+ key +" value is :"+ mImageLabelingMap.get(key));
	  //if(mImageLabelingMap.get(key) != null){
	  if(mImageLabelingMap.containsKey(key)){
		  return true;
	  }else {
	  return false;
	  }
  }



}
