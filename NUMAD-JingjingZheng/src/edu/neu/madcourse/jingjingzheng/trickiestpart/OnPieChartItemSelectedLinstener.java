package edu.neu.madcourse.jingjingzheng.trickiestpart;

public interface OnPieChartItemSelectedLinstener {

	void onPieChartItemSelected(PieChartView view, int position, String colorRgb, float size, float rate, boolean isFreePart, float rotateTime);
}
