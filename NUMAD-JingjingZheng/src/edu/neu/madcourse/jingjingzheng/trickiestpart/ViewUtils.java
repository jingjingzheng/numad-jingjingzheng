package edu.neu.madcourse.jingjingzheng.trickiestpart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Utility methods for Views.
 */
public class ViewUtils extends Activity{
	private static final String TAG = "ViewUtils";
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
    public ViewUtils() {
    }

    public static void setViewWidths(View view, View[] views) {
        int w = view.getWidth();
        int h = view.getHeight();
        for (int i = 0; i < views.length; i++) {
            View v = views[i];
            v.layout((i + 1) * w, 0, (i + 2) * w, h);
            printView("view[" + i + "]", v);
        }
    }

    public static void printView(String msg, View v) {
        System.out.println(msg + "=" + v);
        if (null == v) {
            return;
        }
        System.out.print("[" + v.getLeft());
        System.out.print(", " + v.getTop());
        System.out.print(", w=" + v.getWidth());
        System.out.println(", h=" + v.getHeight() + "]");
        System.out.println("mw=" + v.getMeasuredWidth() + ", mh=" + v.getMeasuredHeight());
        System.out.println("scroll [" + v.getScrollX() + "," + v.getScrollY() + "]");
    }

    
  //initialize menu list items
    public  void initMenuListView(Context context,ListView listView,int layout){
    	final String[] arr = {"Today", "History","Tracking Setting",""};
    	listView.setAdapter(new ArrayAdapter<String>(context,layout,arr));
    	listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Context context = view.getContext();
				String str = arr[position];
				if(str.equals("Today")){
					Log.d(TAG,"start Today activity");
					startActivity(new Intent(getApplicationContext(),Today.class));
					Log.d(TAG,"finish activity");
				}else if(str.equals("History")){
					startActivity(new Intent(context,History.class));
				}else if(str.equals("Tracking Setting")){
					startActivity(new Intent(context,Tracking_Setting.class));
				}else {
					Log.d(TAG,"no matcing item!");
				}
				
			}
    		
    	});
    }
}
