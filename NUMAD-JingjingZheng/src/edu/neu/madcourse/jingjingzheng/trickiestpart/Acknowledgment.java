package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import java.util.List;

import com.slidingmenu.lib.app.SlidingActivity;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.ClickListenerForScrolling;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.SizeCallbackForMenu;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Acknowledgment extends SlidingActivity{
	public final static  String TAG="Acknowledgment";
	MyHorizontalScrollView scrollView;
    View menu;
    View app;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
 public void onCreate(Bundle savedInstanceState){
	 super.onCreate(savedInstanceState);
	 this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	 LayoutInflater inflater = LayoutInflater.from(this);
     scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.trickiestpart_scroll_view, null);
     /// added code for touch to slide
     setContentView(R.layout.trickiestpart_today);
	 setBehindContentView(R.layout.trickiestpart_touch_menu);
	 ListView touchMenuList =(ListView) findViewById(R.id.touch_menu_list);
	 initMenuListView(this,touchMenuList,android.R.layout.simple_list_item_1);
	 getSlidingMenu().setBehindOffset(80);
     // added code here 
     setContentView(scrollView);
     Log.d(TAG,"set scroll view");
     menu = inflater.inflate(R.layout.trickiestpart_menu, null);
     app = inflater.inflate(R.layout.trickiestpart_ack, null);
     ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.ack_tabBar);
     Log.d(TAG,"get the tabbar from history");
     //ListView listView = (ListView) app.findViewById(R.id.list);
     //new ViewUtils().initListView(this, listView, android.R.layout.simple_list_item_1);

     ListView listView = (ListView) menu.findViewById(R.id.menu_list);
     initMenuListView(this, listView, android.R.layout.simple_list_item_1);

     btnSlide = (ImageView) tabBar.findViewById(R.id.ack_BtnSlide);
     Log.d(TAG,"get slide button");
     btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));

     final View[] children = new View[] { menu,app};

     // Scroll to app (view[1]) when layout finished.
     int scrollToViewIdx = 1;
     scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
 }
//initialize menu list items
 public  void initMenuListView(Context context,ListView listView,int layout){
 	final String[] arr = {"My Meal","Tracking Setting","Help","Acknowledgment"};
 	final Integer[] images = { R.drawable.meal,
              R.drawable.settings, R.drawable.help ,R.drawable.comments};
     	List<RowItem> rowItems;
     	rowItems = new ArrayList<RowItem>();
         for (int i = 0; i < arr.length; i++) {
             RowItem item = new RowItem(images[i], arr[i]);
             rowItems.add(item);
         }
         CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
                 R.layout.trickiestpart_list_item, rowItems);
 	listView.setAdapter(menu_adapter);
 	listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Context context = view.getContext();
				String str = arr[position];
				if(str.equals("My Meal")){
					vibrate(25);
					startActivity(new Intent(context,Today.class));
//				}else if(str.equals("History")){
//					vibrate(25);
//					startActivity(new Intent(context,History.class));
				}else if(str.equals("Tracking Setting")){
					startActivity(new Intent(context,Tracking_Setting.class));
				}else if(str.equals("Help")){
					vibrate(25);
					//startActivity(new Intent(context,TutorialActivity.class));
					startActivity(new Intent(context,Tutorial.class));
					Log.d(TAG,"no matcing item!");
				}else {
					vibrate(25);
					startActivity(new Intent(context,Acknowledgment.class));
				}
				
			}
 		
 	});
 }
 
 private void vibrate(int ms) {
	    Vibrator v = 
	      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    v.vibrate(ms);
	  }

}
