package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.File;
import java.io.FileFilter;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PhotoListFragment extends ListFragment{
	private static String TAG = "PhotoListFragment";
	private String key = "";
	private File[] listFile;
	
	public static PhotoListFragment newInstance(String content){
		PhotoListFragment plf = new PhotoListFragment();
		plf.key = content;
		return plf;
	}
	 
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		if(container!=null){
    		((ViewGroup)container.getParent()).removeView(container);
    	}
		View photoList = inflater.inflate(R.layout.trickiestpart_photolist, container);
		Log.d(TAG,"inflate success");
		return photoList;
		
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,"setting list adapter");
        setListAdapter(new ImageAdapter(getActivity().getApplicationContext()));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }
	
    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private Bitmap[] myPhotos;

        public ImageAdapter(Context c) {
        	Log.d(TAG,"setting image adapter");
            mContext = c;    
            }

        public int getCount() {
            get_images();
            return myPhotos.length;
            }

        public Object getItem(int position) {
            return position;    
            }

        public long getItemId(int position) {
            return position;    
            }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
            } else {
                imageView = (ImageView) convertView;
            }
            imageView.setImageBitmap(myPhotos[position]);
            return imageView;
        }
        
        

        private void get_images(){
            File path = Environment.getExternalStoragePublicDirectory(
    	            Environment.DIRECTORY_PICTURES);
    	    File file = new File(path, "MyCameraApp"+File.separator+key);
    	    if(file.exists()){
    	    	Log.d(TAG,"files in pictures directory:"+ file.listFiles());
    	    	listFile = file.listFiles();
    	    	for(int i = 0;i<listFile.length;i++){
    	    		myPhotos[i] = BitmapFactory.decodeFile(listFile[i].getAbsolutePath());
    	    	}
    	    }
        }
    }
	
	
}
