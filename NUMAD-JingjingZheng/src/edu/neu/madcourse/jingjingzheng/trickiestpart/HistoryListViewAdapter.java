package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import edu.neu.madcourse.jingjingzheng.R;

public class HistoryListViewAdapter extends ArrayAdapter<RowItem>{

    Context context;
    
    public HistoryListViewAdapter(Context context, int resourceId,
            List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }
 
    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.trickiespart_history_list_item, null);
//            if(rowItem.getTitle().equals("Tue, Apr 9")){
//            	convertView.setBackgroundResource(R.drawable.selector_round_corner_bottom);
//            }
//            else if (rowItem.getTitle().equals("Today")){
//            	convertView.setBackgroundResource(R.drawable.selector_round_corner_top);
//                }
//            else {
            	convertView.setBackgroundResource(R.drawable.selector_middle);
//            	}
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title_history);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon_history);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
 
        return convertView;
    }
}
