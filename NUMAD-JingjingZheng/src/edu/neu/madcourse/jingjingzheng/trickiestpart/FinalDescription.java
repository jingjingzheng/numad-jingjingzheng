package edu.neu.madcourse.jingjingzheng.trickiestpart;

import edu.neu.madcourse.jingjingzheng.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class FinalDescription extends Activity implements OnClickListener{
	
	protected Button mStartButton = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.final_description);	

		// get views and set listeners
		setupViews();
		// adjust layouts according to the screen resolution
		adjustLayout();        
	}
	
	private void setupViews() {
		mStartButton = (Button) findViewById(R.id.bt_start_descr_final);
		mStartButton.setOnClickListener(this);
	}
	
	private void adjustLayout() {
		getWindow().setFlags(
			WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_FULLSCREEN, 
			WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_FULLSCREEN
		); 
		// get screen resolution
		DisplayMetrics dm = new DisplayMetrics();  
        Display display = getWindowManager().getDefaultDisplay(); 		
        display.getMetrics(dm);        
        // adjust the layout according to the screen resolution
//	 	LinearLayout main = (LinearLayout)findViewById(R.id.rocketrankRoot);				   
		LayoutParams laParams = null;
	    LinearLayout linearLayout = (LinearLayout)findViewById(R.id.sv_final_decr_ctx);
		laParams = linearLayout.getLayoutParams();
		laParams.height = (int) (dm.heightPixels * 0.6f);
		linearLayout.setLayoutParams(laParams);
	}

	public void onClick(View v) {
		Intent i = new Intent(this, PhotoDishes.class);
		startActivity(i);
		finish();
	}
}
