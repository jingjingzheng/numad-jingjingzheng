package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class ShowImage extends Activity implements OnClickListener{
	public static final String TAG="ShowImage";
    public final static String TAG_FILENAME = "FileName";
    public final static String TAG_MAPPING="Mapping";
	public final static String FILE_NAME_FORMAT = "yyyy-MM-dd_HH+mm+ss";
	public final static String HISTORY_DATE_FORMAT = "yyyy-MM-dd";
	public final static String HISTORY_TIME_FORMAT = "HH+mm+ss";
	public static final String TRACKING="tracking_setting";
    public static final String MEAL_TYPE="meal_type";
    public static final String GRAINS = "grains";
	public static final String DAIRY = "dairy";
	public static final String ANIMAL_PROTEIN="animal_protein";
	public static final String VEGGIES= "veggies";
	public static final String FRUITS = "fruits";
	public static final String SWEETS="sweets";
	public static final String NUTS="nuts";
	public static final String OILS_FATS="oils_fats";
	private SharedPreferences sf;
	ArrayList<String> trackingList;
	String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oil or Fats"};
	private String mFileName;
	private String mMappingFileName;
	private String ImgFileName;
	private ArrayList<String> selected_cats=new ArrayList<String>();
	private ArrayList<Category> selected_categories = new ArrayList<Category>();
	private String static_data;
	private Date mDate;
    Chart_View myView;
    private Bitmap image;
    public static float mCircleX,mCircleY;
    private ArrayList<String> categories;
	public ArrayList<Bitmap> croppedBitmap;
	private HashMap<String,String> imageLabelingMap;
	private String title; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_image);
		title = getIntent().getStringExtra(MEAL_TYPE);
		//sf = this.getSharedPreferences(TAG_MAPPING, MODE_PRIVATE);
		imageLabelingMap = new HashMap<String,String>();
		 Display myDisplay =getWindowManager().getDefaultDisplay();
		int width =myDisplay.getWidth();
		int height=myDisplay.getHeight();
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				    width, height/2);
		View backBtn = findViewById(R.id.show_image_back);
	    backBtn.setOnClickListener(this);
	    View saveBtn = findViewById(R.id.show_image_save);
		saveBtn.setOnClickListener(this);
		
		final Chart_View c =  (Chart_View)findViewById(R.id.bitmap);
		myView =c;
		image = (Bitmap)getIntent().getParcelableExtra("Bitmap");
		Drawable d = new BitmapDrawable(getResources(),image);
		myView.setLayoutParams(params);
	    myView.setBackgroundDrawable(d);
	    
		Bundle extras = getIntent().getExtras();
		ImgFileName = extras.getString("ImageFileName");
	    categories =  extras.getStringArrayList("categories");
	    mFileName = extras.getString("FileName");
	    mCircleX = extras.getFloat("center_x", 0);
	    mCircleY = extras.getFloat("center_y",0);
	    if(categories != null){
	    	addCategories(categories);
	    }
	    myView.setmCenter(mCircleX,mCircleY);
	    
	    
	}
	    
	    private void addCategories(ArrayList<String> categories) {
	    	Log.d(TAG,"categories selected : " + categories.toString());
	        for (String cat : categories) {
	          addCategory(cat);
	        }
	      }
	    
	    private void addCategory(String category) {
	        if (category.equals("Grains")) {
	          final View grain_view = (View) this.findViewById(R.id.grain_view);
	          onGrainClicked(grain_view);
	        } else if (category.equals("Veggies")) {
	          final View vegetable_view = (View) this.findViewById(R.id.vegetable_view);
	          onVegetableClicked(vegetable_view);
	        } else if ( category.equals("Fruits")) {
	          final View fruit_view = (View) this.findViewById(R.id.fruit_view);
	          onFruitClicked(fruit_view);
	        } else if (category.equals("Dairy")) {
	          final View dairy_view = (View) this.findViewById(R.id.dairy_view);
	          onDairyClicked(dairy_view);
	        } else if (category.equals("Animal Protein")) {
	          final View protein_view = (View) this.findViewById(R.id.protein_view);
	          onProteinClicked(protein_view);
	        } else if (category.equals("Oil or Fats")) {
	          final View oil_view = (View) this.findViewById(R.id.oil_view);
	          onOilSugarClicked(oil_view);
	        } else if (category.equals("Nuts")){
	        	final View nuts_view = (View) this.findViewById(R.id.nuts_view);
	        	onNutsClicked(nuts_view);
	        } else if (category.equals("Sweets")){
	        	final View sweets_view = (View) this.findViewById(R.id.sweets_view);
	        	onSweetsClicked(sweets_view);
	        }
	      }
	    
	    
	    public void onProteinClicked(View v){
	          myView.onProteinClicked(v);
	      }

	      public void onVegetableClicked(View v){
	          myView.onVegetableClicked(v);
	      }

	      public void onDairyClicked(View v){
	          myView.onDairyClicked(v);
	      }

	      public void onFruitClicked(View v){
	          myView.onFruitClicked(v);
	      }

	      public void onGrainClicked(View v){
	          myView.onGrainClicked(v);
	      }

	      public void onOilSugarClicked(View v){
	          myView.onOilSugarClicked(v);
	      }
	      
	      public void onNutsClicked(View v){
	    	      myView.onNutsClicked(v);
	    	  }
	      
	      public void onSweetsClicked(View v){
	    	      myView.onSweetsClicked(v);
	    	  }
	      
	      
	      /**
	       * Gets the category data from a given filename
	       * @param filename The name of the file to get data from.
	       */
	      private String getDataFromFile(String filename) {
	        String data = "ERROR";
	        Log.d(TAG, "ReadData");
	        try {
	          Log.d("readData", "File: "+mFileName);
	          FileInputStream fis = this.openFileInput(mFileName);
	          BufferedReader r = new BufferedReader(new InputStreamReader(fis));
	          data = r.readLine();
	          r.close(); 
	        } catch (FileNotFoundException e) {
	          e.printStackTrace();
	        } catch (IOException e) {
	          e.printStackTrace();
	        }
	        return data;
	      }
	      
	      
	      public String parseData(JSONArray jArray){
	    	  String data = jArray.toString();
	    	  return data;
	    	}
	      
	      
	      /**
	       * Gets the current date and time based on the system settings/timezone
	       * @return String formattedDate
	       */
	      public String getDateTime(){
	        Calendar c = Calendar.getInstance();
	        SimpleDateFormat df = new SimpleDateFormat(FILE_NAME_FORMAT);
	        String formattedDate = df.format(c.getTime());

	        return formattedDate;
	      }
	      /**
	       * Gets the Current date for file name
	       * @return String date - format YYYY_MM_DD
	       */
	      public String getDate(){
	        Calendar c = Calendar.getInstance();
	        SimpleDateFormat df = new SimpleDateFormat(FILE_NAME_FORMAT);
	        String formattedDate = df.format(c.getTime());    
	        return formattedDate;
	      }

	      /**
	       * Save file to internal memory
	       * @param data - Stored data
	       */
	      public void save(String data, String FileName){
	        String date = getDate();
	        if(FileName.equals("")){         // generates name of file
	          FileName = date+".txt";
	          mFileName = FileName;
	        
	          Log.d(TAG, "making a new filename...");

	          SimpleDateFormat fileDate =
	            new SimpleDateFormat(FILE_NAME_FORMAT, Locale.US);
	          mDate = fileDate.parse(mFileName.replace(".txt", ""), 
	                new ParsePosition(0));

	        }else if(FileName.equals("mapping")){
	        	FileName = ImgFileName+date+".txt";
	        	mMappingFileName = FileName;
	        	SimpleDateFormat fileDate =
	    	            new SimpleDateFormat(FILE_NAME_FORMAT, Locale.US);
	    	          mDate = fileDate.parse(mFileName.replace(".txt", ""), 
	    	                new ParsePosition(0));
	        } else if (!FileName.equals("")){    // Deletes current file
	          Log.d(TAG, "saving to a file that already exists");
	          File file = new File(this.getFilesDir(), FileName);
	          boolean deleted = file.delete();
	        }
	        Log.d(TAG, "save() filename: " + FileName);
	        try{
	          FileWriter write = new FileWriter(this.getFilesDir() + "/" + FileName, true);
	          Log.d(TAG, String.valueOf(this.getFilesDir()));
	          write.write(data+"\r\n"); //adds new line.
	          write.close();
	          // if we get here with no exceptions, we did it!
//	          Toast.makeText(getBaseContext(), "Entr!", 
//	                Toast.LENGTH_SHORT).show();

	        } catch (FileNotFoundException e) {
	        Toast.makeText(getBaseContext(), "Uh-Oh.  We couldn't save the chart. Try Again?", 
	                Toast.LENGTH_SHORT).show();
	          e.printStackTrace();
	        } catch (IOException e) {
	        Toast.makeText(getBaseContext(), "Oops! We couldn't read from your phone. Try Again!", 
	                Toast.LENGTH_SHORT).show();
	          e.printStackTrace();
	        }
	      }
	      private void vibrate(int ms) {
	    	    Vibrator v = 
	    	      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    	    v.vibrate(ms);
	    	  }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.show_image_back:
			finish();
			break;
		case R.id.show_image_save:
			if (myView.getChartData().length() == 0) {
			      Toast.makeText(getBaseContext(),
			          "Add some categories before saving your chart!",
			          Toast.LENGTH_SHORT).show();
			      return;
			}
			      Log.d(TAG, "Save clicked, saving...");
			      JSONArray chartData = myView.getChartData();
			      for(Category cat : myView.getMyCategories()){
			    	 selected_cats.add(cat.getCategory());
			      }
//			      for(String str: getTrackingTypes()){
//			    	  for(int i = 0; i < selected_cats.size(); i++){
//			    		  if(selected_cats.get(i).equals(str)){
//			    			  selected_categories.add(myView.getMyCategories().get(i));
//			    		  }
//			    	  }
//			      }
//			      
			      String data = parseData(chartData);
			      save(data, mFileName);
			      static_data = getDataFromFile(mFileName);
			      ArrayList<Category> cats = //selected_categories;
			          JSONParser.getCatListFromString(static_data, this);
//			      Log.d(TAG,"static data : "+static_data);
//			      Log.d(TAG,"categories from user selected for tracking: "+ cats);
			      //Log.d(TAG,imageLabelingMap.get("b"));
                  imageLabelingMap.put(ImgFileName, mFileName);
                  Global.setMapObjects(imageLabelingMap);
                  Intent summary = new Intent(this,Summary.class);
                  summary.putExtra("mFileName", mFileName);
                  summary.putExtra(MEAL_TYPE, title);
                  summary.putExtra("CUR_CHOSENCAT", selected_cats);
                  startActivity(summary);
                  break;
		}
	 }

	
	 public ArrayList<String> getTrackingTypes(){
	    	sf = this.getSharedPreferences(TRACKING, MODE_PRIVATE);
	    	ArrayList<String> tracking = new ArrayList<String>();
	    	for(String str:select_image){
	    		if(sf.getBoolean(str, false)){
	    			tracking.add(str);
	    		}
	    	}
	    	return tracking;
	    }
	 
	 public JSONArray saveToJSONArray(Map<String,String> obj){
		  
		    JSONArray jsonArray = new JSONArray();

		      JSONObject jsonObject = new JSONObject(obj);
		      jsonArray.put(jsonObject);

		    return jsonArray;
		  }


	
}