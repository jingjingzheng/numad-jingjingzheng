package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.TableRow;
import android.widget.Toast;

public class CategorySelection extends Activity {
	private static final String PLATE_PREF = "edu.neu.madcourse.shanshanhuang.trickiestpart";
	private static final String PREF_CATEGORY_INITIAL_RUN = "category_initial_run";
	public static final String MEAL_TYPE="meal_type";
	private String fName;
	ArrayList<String> mCategories = new ArrayList<String>();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// no  titlebar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.platechart_select_categories);
		fName = getIntent().getStringExtra("ImageFileName");
	}

	public void onResume(){
		super.onResume();
	}

	public void onNextClicked(View v) {
		Bitmap image = (Bitmap)getIntent().getParcelableExtra("Bitmap");
		float center_x = getIntent().getFloatExtra("center_x", 0);
		float center_y = getIntent().getFloatExtra("center_y", 0);
		String title = getIntent().getStringExtra(MEAL_TYPE);
		Intent i = new Intent(this, ShowImage.class);
		// need to give AddChart the array of items selected.
		i.putExtra("FileName", "");
		i.putExtra("categories", mCategories);
		i.putExtra("center_x", center_x);
		i.putExtra("center_y", center_y);
		i.putExtra("Bitmap", image);
		i.putExtra("ImageFileName", fName);
		i.putExtra(MEAL_TYPE,title);
		startActivity(i);

	}
	
	public void onBackClicked(View v){
		vibrate(25);
		finish();
	}

	//--------
	// Protein
	// ------
	public void onProteinClicked(View v) {
		String category = "Animal Protein";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setProteinDeselect(v);
			removeCategory(category);
		}else{
			setProteinSelect(v);
			addCategory(category);
		}
	}

	private void setProteinSelect(View v){
		TextView box = (TextView) v.findViewById(R.id.protein_cs_box);
		TextView text = (TextView) v.findViewById(R.id.protein_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowProtein);

		box.setBackgroundColor(getResources().getColor(R.color.Protein));
		text.setTextColor(getResources().getColor(R.color.Protein));
		row.setBackgroundColor(getResources().getColor(R.color.Protein_Grayed));
	}

	private void setProteinDeselect(View v) {
		TextView box = (TextView) v.findViewById(R.id.protein_cs_box);
		TextView text = (TextView) v.findViewById(R.id.protein_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowProtein);

		box.setBackgroundColor(getResources().getColor(R.color.Protein_Grayed));
		text.setTextColor(getResources().getColor(R.color.Protein_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}

	// ---------
	// Vegetable
	// ---------
	public void onVegetableClicked(View v) {
		String category = "Veggies";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setVegetableDeselect(v);
			removeCategory(category);
		}else{
			setVegetableSelect(v);
			addCategory(category);
		}
	}

	private void setVegetableSelect(View v){
		TextView box = (TextView) v.findViewById(R.id.vegetable_cs_box);
		TextView text = (TextView) v.findViewById(R.id.vegetable_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowVegetable);

		box.setBackgroundColor(getResources().getColor(R.color.Vegetable));
		text.setTextColor(getResources().getColor(R.color.Vegetable));
		row.setBackgroundColor(getResources().getColor(R.color.Vegetable_Grayed));
	}

	private void setVegetableDeselect(View v) {

		TextView box = (TextView) v.findViewById(R.id.vegetable_cs_box);
		TextView text = (TextView) v.findViewById(R.id.vegetable_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowVegetable);

		box.setBackgroundColor(getResources().getColor(R.color.Vegetable_Grayed));
		text.setTextColor(getResources().getColor(R.color.Vegetable_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}

	// --------
	// Dairy
	// --------
	public void onDairyClicked(View v) {
		String category = "Dairy";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setDairyDeselect(v);
			removeCategory(category);
		}else{
			setDairySelect(v);
			addCategory(category);
		}
	}

	private void setDairySelect(View v){
		TextView box = (TextView) v.findViewById(R.id.dairy_cs_box);
		TextView text = (TextView) v.findViewById(R.id.dairy_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowDairy);

		box.setBackgroundColor(getResources().getColor(R.color.Dairy));
		text.setTextColor(getResources().getColor(R.color.Dairy));
		row.setBackgroundColor(getResources().getColor(R.color.Dairy_Grayed));
	}

	private void setDairyDeselect(View v) {

		TextView box = (TextView) v.findViewById(R.id.dairy_cs_box);
		TextView text = (TextView) v.findViewById(R.id.dairy_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowDairy);

		box.setBackgroundColor(getResources().getColor(R.color.Dairy_Grayed));
		text.setTextColor(getResources().getColor(R.color.Dairy_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}

	// --------
	// Fruit
	// --------
	public void onFruitClicked(View v) {
		String category = "Fruits";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setFruitDeselect(v);
			removeCategory(category);
		}else{
			setFruitSelect(v);
			addCategory(category);
		}
	}

	private void setFruitSelect(View v){
		TextView box = (TextView) v.findViewById(R.id.fruit_cs_box);
		TextView text = (TextView) v.findViewById(R.id.fruit_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowFruit);

		box.setBackgroundColor(getResources().getColor(R.color.Fruit));
		text.setTextColor(getResources().getColor(R.color.Fruit));
		row.setBackgroundColor(getResources().getColor(R.color.Fruit_Grayed));
	}

	private void setFruitDeselect(View v) {

		TextView box = (TextView) v.findViewById(R.id.fruit_cs_box);
		TextView text = (TextView) v.findViewById(R.id.fruit_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowFruit);

		box.setBackgroundColor(getResources().getColor(R.color.Fruit_Grayed));
		text.setTextColor(getResources().getColor(R.color.Fruit_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}


	// --------
	// Grain
	// --------
	public void onGrainClicked(View v) {
		String category = "Grains";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setGrainDeselect(v);
			removeCategory(category);
		}else{
			setGrainSelect(v);
			addCategory(category);
		}
	}

	private void setGrainSelect(View v){
		TextView box = (TextView) v.findViewById(R.id.grain_cs_box);
		TextView text = (TextView) v.findViewById(R.id.grain_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowGrain);

		box.setBackgroundColor(getResources().getColor(R.color.Grain));
		text.setTextColor(getResources().getColor(R.color.Grain));
		row.setBackgroundColor(getResources().getColor(R.color.Grain_Grayed));
	}

	private void setGrainDeselect(View v) {

		TextView box = (TextView) v.findViewById(R.id.grain_cs_box);
		TextView text = (TextView) v.findViewById(R.id.grain_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowGrain);

		box.setBackgroundColor(getResources().getColor(R.color.Grain_Grayed));
		text.setTextColor(getResources().getColor(R.color.Grain_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}

	// --------
	// Oil/Sugar
	// --------
	public void onOilSugarClicked(View v) {
		String category = "Oil or Fats";
		vibrate(25);
		boolean inList = mCategories.contains(category);
		if (inList) {
			setOilDeselect(v);
			removeCategory(category);
		}else{
			setOilSelect(v);
			addCategory(category);
		}
	}

	private void setOilSelect(View v){
		TextView box = (TextView) v.findViewById(R.id.oil_cs_box);
		TextView text = (TextView) v.findViewById(R.id.oil_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowOil);

		box.setBackgroundColor(getResources().getColor(R.color.Oil_Sugar));
		text.setTextColor(getResources().getColor(R.color.Oil_Sugar));
		row.setBackgroundColor(getResources().getColor(R.color.Oil_Sugar_Grayed));
	}

	private void setOilDeselect(View v) {
		TextView box = (TextView) v.findViewById(R.id.oil_cs_box);
		TextView text = (TextView) v.findViewById(R.id.oil_cs_label);
		TableRow row = (TableRow)this.findViewById(R.id.csTableRowOil);

		box.setBackgroundColor(getResources().getColor(R.color.Oil_Sugar_Grayed));
		text.setTextColor(getResources().getColor(R.color.Oil_Sugar_Grayed));
		row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
	}
	
	
	    // --------
		// Nuts
		// --------
		public void onNutsClicked(View v) {
			String category = "Nuts";
			vibrate(25);
			boolean inList = mCategories.contains(category);
			if (inList) {
				setNutsDeselect(v);
				removeCategory(category);
			}else{
				setNutsSelect(v);
				addCategory(category);
			}
		}

		private void setNutsSelect(View v){
			TextView box = (TextView) v.findViewById(R.id.nuts_cs_box);
			TextView text = (TextView) v.findViewById(R.id.nuts_cs_label);
			TableRow row = (TableRow)this.findViewById(R.id.csTableRowNuts);

			box.setBackgroundColor(getResources().getColor(R.color.Legumes_Nuts));
			text.setTextColor(getResources().getColor(R.color.Legumes_Nuts));
			row.setBackgroundColor(getResources().getColor(R.color.Legumes_Nuts_Grayed));
		}

		private void setNutsDeselect(View v) {
			TextView box = (TextView) v.findViewById(R.id.nuts_cs_box);
			TextView text = (TextView) v.findViewById(R.id.nuts_cs_label);
			TableRow row = (TableRow)this.findViewById(R.id.csTableRowNuts);

			box.setBackgroundColor(getResources().getColor(R.color.Legumes_Nuts_Grayed));
			text.setTextColor(getResources().getColor(R.color.Legumes_Nuts_Grayed));
			row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
		}
		
		// --------
				// Sweets
				// --------
				public void onSweetsClicked(View v) {
					String category = "Sweets";
					vibrate(25);
					boolean inList = mCategories.contains(category);
					if (inList) {
						setSweetsDeselect(v);
						removeCategory(category);
					}else{
						setSweetsSelect(v);
						addCategory(category);
					}
				}

				private void setSweetsSelect(View v){
					TextView box = (TextView) v.findViewById(R.id.sweets_cs_box);
					TextView text = (TextView) v.findViewById(R.id.sweets_cs_label);
					TableRow row = (TableRow)this.findViewById(R.id.csTableRowSweets);

					box.setBackgroundColor(getResources().getColor(R.color.Sweets));
					text.setTextColor(getResources().getColor(R.color.Sweets));
					row.setBackgroundColor(getResources().getColor(R.color.Sweets_Grayed));
				}

				private void setSweetsDeselect(View v) {
					TextView box = (TextView) v.findViewById(R.id.sweets_cs_box);
					TextView text = (TextView) v.findViewById(R.id.sweets_cs_label);
					TableRow row = (TableRow)this.findViewById(R.id.csTableRowSweets);

					box.setBackgroundColor(getResources().getColor(R.color.Sweets_Grayed));
					text.setTextColor(getResources().getColor(R.color.Sweets_Grayed));
					row.setBackgroundColor(getResources().getColor(R.color.Chart_Background));
				}
				
		

  private void vibrate(int ms) {
    Vibrator v = 
      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    v.vibrate(ms);
  }
	
	
	public void removeCategory(String category){
		mCategories.remove(category);
	}

	public void addCategory(String category){
		mCategories.add(category);
	}

}
