package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class MediaUtils {
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	/** Create a file Uri for saving an image or video */
	public static Uri getOutputMediaFileUri(int type,String path){
	      return Uri.fromFile(getOutputMediaFile(type,path));
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type,String path){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.
		System.out.println(Environment.getExternalStorageState());
		String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+path+File.separator+timeStamp);
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp"+File.separator+path, "failed to create directory");
	            return null;
	        }
	    }
	 // Create a media file name
	    String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        path+ time + ".jpg");
	        Log.d("MyCameraApp"+File.separator+path,"save to this path");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        path+ time + ".mp4");
	    } else {
	        return null;
	    }

	    return mediaFile;
 }
	
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}
