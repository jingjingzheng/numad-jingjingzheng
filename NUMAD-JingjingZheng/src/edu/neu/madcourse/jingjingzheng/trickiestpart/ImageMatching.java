package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import edu.neu.madcourse.jingjingzheng.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class ImageMatching{
	private static final String MEAL_PREF = "edu.madcourse.jingjingzheng.trickiestpart.mealtime";
	private static final String TAG = "edu.neu.madcourse.jingjingzheng.trickiestpart.ImageMathcing";
	public SharedPreferences mMealTimeSF;
	public static final String MEAL_TYPE="meal_type";
	public static final int MEDIA_TYPE_IMAGE = 1;
	private static Uri fileUri;
	//private ImageMatchServe mImageMatchServe;
	private File[] listFile;
	public ArrayList<Bitmap> myPhotos;
	public ArrayList<String> myFileNames;
	public Map<Double,String> myMatchingFileNames;
	//private List<Map<Bitmap, Object>> imageHisPair;
	private Map<Double, Bitmap> mMap;
	Mat curimg;
	Mat img;
	private ImageComparator c;
	private double scale = 0.1;
	private AlertDialog loadingDialog;
	private ArrayList<Bitmap> orderedTopMatchBitMap;
	private ArrayList<String> orderedTopMatchFileName;
	private static String title;
	private static String timeStamp;
	private static Context mContext;
	public ImageMatching(Context c){
		mContext = c;
	}

   public void matching(String t,Uri u, AlertDialog lD){
	   fileUri = u;
	   title = t;
	   myPhotos = new ArrayList<Bitmap>();
	   myFileNames= new ArrayList<String>();
   	   mMap = new HashMap<Double, Bitmap>(); 
   	   myMatchingFileNames = new HashMap<Double,String>();
       orderedTopMatchBitMap = new ArrayList<Bitmap>();
       orderedTopMatchFileName = new ArrayList<String>();
       //fileUri = MediaUtils.getOutputMediaFileUri(MEDIA_TYPE_IMAGE,"tmp"); 
	   timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
/*	   builder = new AlertDialog.Builder(mContext);
       builder.setMessage(R.string.matchlish_loading);
       loadingDialog = builder.create();*/
	   loadingDialog = lD;
       // Image captured and saved to fileUri specified in the Intent
   	    Log.d(TAG, "successfully capture the pic");          
		mMealTimeSF = mContext.getSharedPreferences(MEAL_PREF, 0);
		SharedPreferences.Editor edit = mMealTimeSF.edit();

		edit.putString("Cur_MealPath", String.valueOf(fileUri)).commit();
		edit.putString("Cur_MealDir", String.valueOf(title+File.separator+timeStamp)).commit();
		
   	
   	Log.d(TAG, "onActivityResult-fileUri: " + fileUri);
   	
   	File file = new File(Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES), "MyCameraApp"+File.separator+title);
       Log.d("TEST",title);
	    //File file = new File(mpath, "MyCameraApp"+File.separator+title);
	    if(file.exists()){
	    	Log.d("TestFragment","files in pictures directory:"+ file.listFiles().length);
	    	listFile = file.listFiles();
	    	if(listFile.length!=0){
	    		for(int i = 0; i < listFile.length; i++){
		    		if(listFile[i].isDirectory()){
		    			PhotoDishesUtils.loadImages(listFile[i].listFiles(), myPhotos,myFileNames);
		    		}
		    		Log.d(TAG, "listFile path: " + String.valueOf(listFile[i]));
	    	}
	    	
	      }


       
          if (!OpenCVLoader.initDebug()) {
           	// Handle initialization error
           	Log.e(TAG, "initDebug failed");

           }
   
           /////// end of TA added code
           Log.i(TAG, "Trying to load OpenCV library");
           if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, mContext,
           		mOpenCVCallBack)) {
           	Log.e(TAG, "Cannot connect to OpenCV Manager");
           }
	    	}
   }
   
   public void changeImagesize(File listFiles) {
 	   
	    final int REQUIRED_SIZE=200;
		BitmapFactory.Options options = new BitmapFactory.Options(); 
		options.inJustDecodeBounds = true;      
		String fName;
       	 Bitmap scaled = null;	
       	 BitmapFactory.decodeFile(listFiles.getAbsolutePath(),options);
       	 
       	 Log.d(TAG, String.valueOf(listFiles.getAbsolutePath()));
       	 int width_tmp = options.outWidth,height_tmp=options.outHeight;
       	 int scale = 1;
       	 while(true){
       		 if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
       			 break;
       		 width_tmp/=2;
       		 height_tmp/=2;
       		 scale*=2;
       	 }
     	
       	 BitmapFactory.Options options2 = new BitmapFactory.Options();
       	 options2.inSampleSize = scale;
       	 scaled = BitmapFactory.decodeFile(listFiles.getAbsolutePath(), options2);
       	 fName = listFiles.getName();
       	 // add to the image list
       	 if (scaled != null) {
       		 //origin.recycle(); // explicit call to avoid out of memory
       		 myPhotos.add(scaled);
       		 myFileNames.add(fName);
       	 } else {
       		// myPhotos.add(origin);
       	 }
	}
   
   private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(mContext) {
       public void onManagerConnected(int status) {
       	//double scale = 0.1;
       	switch (status) {
           case LoaderCallbackInterface.SUCCESS: {
               Log.i(TAG, "OpenCV loaded successfully"); 
               
               String[] splitStr = String.valueOf(fileUri).split(":");
               File curfile = new File(String.valueOf(splitStr[1]));
               if(curfile.exists()){
               	Log.d(TAG, "current file in picture directory");
               	changeImagesize(curfile);
               	Log.d(TAG,"myFileNames size : " + myFileNames.size());
               	Log.d(TAG,"myPhotoss size : " + myPhotos.size());
               }
               Log.d(TAG, String.valueOf(curfile));
               
               curimg = new Mat();
               Utils.bitmapToMat(myPhotos.get(myPhotos.size() - 1), curimg);
               
               Size dsize = new Size(curimg.width()*scale, curimg.height()*scale);
               
               Mat img2 = new Mat(dsize, CvType.CV_32F);
               curimg.convertTo(img2, CvType.CV_32F);
               
               c = new ImageComparator();
               c.setReferenceImage(img2);
               img = new Mat();             
               
   	        for (int i = 0; i< (myPhotos.size()-1);i++) {
   	        	Log.d(TAG, "file-imagFile1 exists");
   	        	Utils.bitmapToMat(myPhotos.get(i), img);
   	        	Log.d(TAG, String.valueOf(myPhotos.get(i)));
                   //img = Highgui.imread(listFile[i].getAbsolutePath());
                   Size dsize1 = new Size(img.width()*scale, img.height()*scale);
                   
                   Mat img3 = new Mat(dsize1, CvType.CV_32F);
                   img.convertTo(img3, CvType.CV_32F);
                  // Toast.makeText(this, String.valueOf(c.compare(img3)), Toast.LENGTH_LONG).show();
                   
                   double HistMatchNum = c.compare(img3);
                   Log.d(TAG, "histogram: " + String.valueOf(HistMatchNum));
                   mMap.put(HistMatchNum, myPhotos.get(i));
                   Log.d(TAG,"myFileNames size : " + myFileNames.size());
                  // Log.d(TAG,"myFileNames get  : "+ i +" element : "+  myFileNames.get(i));
                   myMatchingFileNames.put(HistMatchNum,myFileNames.get(i));
                                     
   	        }
   	        
   	        SortByKeys(mMap);
   	        SortStringByKeys(myMatchingFileNames);
   	        
   	        
   	        Global.setObjects(orderedTopMatchBitMap);
               //loadingDialog.dismiss(); // get rid of the game is loading dialog.
   	        
				Intent _intent = new Intent(mContext,edu.neu.madcourse.jingjingzheng.trickiestpart.DishMatchList.class);
				_intent.putExtra(MEAL_TYPE, title);
				_intent.putExtra("orderedTopMatchFileName", orderedTopMatchFileName);
				mContext.startActivity(_intent);
				
           }
           break;
           default: {
               super.onManagerConnected(status);
           }
           break;
       }
       }
   };
   
   public void SortByKeys(Map<Double, Bitmap> map){
   	Log.d(TAG, "SortByKeys begin....");
   	SortedSet<Double> keys = new TreeSet<Double>(map.keySet());
   	
   	if (keys.size() >= 3){
   		Log.d(TAG, "SortByKeys is more than five....");
   		for(int i = 0; i < 3; i ++){
   			orderedTopMatchBitMap.add(map.get(keys.last()));
   			Log.d(TAG, String.valueOf(map.get(keys.last())));
   			keys.remove(keys.last());
   		}
   	}else{
   		int key_length=keys.size();
   		for(int i = 0; i < key_length; i++){
   			Log.d(TAG, "SortByKeys is less than five....");
   			orderedTopMatchBitMap.add(map.get(keys.last()));
   			Log.d(TAG, String.valueOf(map.get(keys.last())));
   			keys.remove(keys.last());
   		}
   	}
   }
   	
   	public void SortStringByKeys(Map<Double, String> map){
   	   	Log.d(TAG, "SortByKeys begin....");
   	   	SortedSet<Double> keys = new TreeSet<Double>(map.keySet());
   	   	
   	   	if (keys.size() >= 3){
   	   		Log.d(TAG, "SortByKeys is more than five....");
   	   		for(int i = 0; i < 3; i ++){
   	   		    orderedTopMatchFileName.add(map.get(keys.last()));
   	   			Log.d(TAG, String.valueOf(map.get(keys.last())));
   	   			keys.remove(keys.last());
   	   		}
   	   	}else{
   	   		int key_length=keys.size();
   	   		for(int i = 0; i < key_length; i++){
   	   			Log.d(TAG, "SortByKeys is less than five....");
   	   		    orderedTopMatchFileName.add(map.get(keys.last()));
   	   			Log.d(TAG, String.valueOf(map.get(keys.last())));
   	   			keys.remove(keys.last());
   	   		}
   	   	}
   	
   }
  
}
