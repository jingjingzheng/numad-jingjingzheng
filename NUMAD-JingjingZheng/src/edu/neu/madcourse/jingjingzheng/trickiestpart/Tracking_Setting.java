package edu.neu.madcourse.jingjingzheng.trickiestpart;

import java.util.ArrayList;
import java.util.List;

import com.slidingmenu.lib.app.SlidingActivity;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.FirstTimeTrackingSetting.ImageAdapter;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.ClickListenerForScrolling;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.SizeCallbackForMenu;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Tracking_Setting extends SlidingActivity{
	public static final String TAG="First time tracking setting";
	public static final String TRACKING = "tracking_setting";
	public static final String GRAINS = "grains";
	public static final String DAIRY = "dairy";
	public static final String ANIMAL_PROTEIN="animal_protein";
	public static final String VEGGIES= "veggies";
	public static final String FRUITS = "fruits";
	public static final String SWEETS="sweets";
	public static final String LEGUMES_NUTS="legumes_nuts";
	public static final String OILS_FATS="oils_fats";
	private SharedPreferences sf;
	
	
	Integer[] imageIDs = {
            R.drawable.grains,
            R.drawable.dairy,
            R.drawable.animal_protein,
            R.drawable.veggies,
            R.drawable.fruits,
            R.drawable.sweets,
            R.drawable.legumes_nuts,
            R.drawable.oils_fats
    };
	
	String[] select_image = {"Grains","Dairy","Animal Protein","Veggies","Fruits","Sweets","Nuts","Oils or Fats"};

	MyHorizontalScrollView scrollView;
    View menu;
    View app;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Resources r = Resources.getSystem();
    	Display myDisplay = getWindowManager().getDefaultDisplay();
        int screen_width =myDisplay.getWidth();
        int screen_height = myDisplay.getHeight();
//        float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, screen_width/2, r.getDisplayMetrics());
//        float height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,screen_height/3,r.getDisplayMetrics());
		LayoutInflater inflater = LayoutInflater.from(this);
        scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.trickiestpart_scroll_view, null);
      /// added code for touch to slide
        setContentView(R.layout.trickiestpart_today);
  	    setBehindContentView(R.layout.trickiestpart_touch_menu);
  	    ListView touchMenuList =(ListView) findViewById(R.id.touch_menu_list);
  	    initMenuListView(this,touchMenuList,android.R.layout.simple_list_item_1);
  	    getSlidingMenu().setBehindOffset(80);
        // added code here 
        setContentView(scrollView);
        
        menu = inflater.inflate(R.layout.trickiestpart_menu, null);
        app = inflater.inflate(R.layout.trickiestpart_tracking_setting, null);
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tracking_tabBar);
        

        ListView listView = (ListView) menu.findViewById(R.id.menu_list);
        initMenuListView(this, listView, android.R.layout.simple_list_item_1);

        btnSlide = (ImageView) tabBar.findViewById(R.id.tracking_BtnSlide);
        Log.d(TAG,"get slide button");
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));

        final View[] children = new View[] { menu,app};

        // Scroll to app (view[1]) when layout finished.
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
		
		sf = this.getSharedPreferences(TRACKING, MODE_PRIVATE);
		//resetSF();
		GridView gridView = (GridView) findViewById(R.id.tracking_setting_gridview);
		float verticalSpacing = screen_height/10;
		float horizontalSpacing = screen_width/40;
		gridView.setVerticalSpacing((int)verticalSpacing);
		gridView.setHorizontalSpacing((int)horizontalSpacing);
        gridView.setAdapter(new ImageAdapter(this));
 
        gridView.setOnItemClickListener(new OnItemClickListener() 
        {
            public void onItemClick(AdapterView<?> parent, 
            View v, int position, long id) 
            {                
          
            	Log.d(TAG,"select image: " + select_image[position]);
                if(sf.getBoolean(select_image[position], false)){
                	v.setBackgroundColor(getResources().getColor(R.color.border));
                	sf.edit().putBoolean(select_image[position], false).commit();
                }else{
                	sf.edit().putBoolean(select_image[position], true).commit();  
                	v.setBackgroundColor(getResources().getColor(R.color.clicked_border));
                	Toast.makeText(getApplicationContext(), "You've selected: "+select_image[position],1000).show();
                }
            }
        });        
	}
	
	public void resetSF(){
		for(String str:select_image){
			sf.edit().putBoolean(str, false).commit();
		}
	}
	
	//initialize menu list items
    public  void initMenuListView(Context context,ListView listView,int layout){
     	final String[] arr = {"My Meal","Tracking Setting","Help","Acknowledgment"};
     	final Integer[] images = { R.drawable.meal,
                 R.drawable.settings, R.drawable.help ,R.drawable.comments};
         	List<RowItem> rowItems;
         	rowItems = new ArrayList<RowItem>();
             for (int i = 0; i < arr.length; i++) {
                 RowItem item = new RowItem(images[i], arr[i]);
                 rowItems.add(item);
             }
             CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
                     R.layout.trickiestpart_list_item, rowItems);
     	listView.setAdapter(menu_adapter);
     	listView.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view, int position,
    					long id) {
    				// TODO Auto-generated method stub
    				Context context = view.getContext();
    				String str = arr[position];
    				if(str.equals("My Meal")){
    					vibrate(25);
    					startActivity(new Intent(context,Today.class));
//    				}else if(str.equals("History")){
//    					vibrate(25);
//    					startActivity(new Intent(context,History.class));
    				}else if(str.equals("Tracking Setting")){
    					startActivity(new Intent(context,Tracking_Setting.class));
    				}else if(str.equals("Help")){
    					vibrate(25);
    					//startActivity(new Intent(context,TutorialActivity.class));
    					startActivity(new Intent(context,Tutorial.class));
    					Log.d(TAG,"no matcing item!");
    				}else {
    					vibrate(25);
    					startActivity(new Intent(context,Acknowledgment.class));
    				}
    				
    			}
     		
     	});
    }
    private void vibrate(int ms) {
        Vibrator v = 
          (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(ms);
      }
  
    
    public class ImageAdapter extends BaseAdapter 
    {
        private Context context;
 
        public ImageAdapter(Context c) 
        {
            context = c;
        }
 
        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }
 
        //---returns the ID of an item--- 
        public Object getItem(int position) {
            return position;
        }
 
        public long getItemId(int position) {
            return position;
        }
 
        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent) 
        {
            ImageView imageView;
            if (convertView == null) {
            	Resources r = Resources.getSystem();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
                
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams((int)px,(int)px));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }
            
            imageView.setImageResource(imageIDs[position]);
            if(sf.getBoolean(select_image[position], false)){
            	imageView.setBackgroundColor(getResources().getColor(R.color.clicked_border));
            }else{
            imageView.setBackgroundColor(getResources().getColor(R.color.border));
            }
            return imageView;
        }
    }
}
