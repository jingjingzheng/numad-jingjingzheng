package edu.neu.madcourse.jingjingzheng.trickiestpart;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.slidingmenu.lib.app.SlidingActivity;

import edu.neu.madcourse.jingjingzheng.R;
import edu.neu.madcourse.jingjingzheng.trickiestpart.MyHorizontalScrollView.SizeCallback;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.ClickListenerForScrolling;
import edu.neu.madcourse.jingjingzheng.trickiestpart.Today.SizeCallbackForMenu;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;




public class History extends SlidingActivity{
	public final static  String TAG="History";
	MyHorizontalScrollView scrollView;
    View menu;
    View app;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
    private static final int MAX_DATEBEFORE = 9; 

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.trickiestpart_history);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = LayoutInflater.from(this);
        scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.trickiestpart_scroll_view, null);
      /// added code for touch to slide
        setContentView(R.layout.trickiestpart_today);
  	    setBehindContentView(R.layout.trickiestpart_touch_menu);
  	    ListView touchMenuList =(ListView) findViewById(R.id.touch_menu_list);
  	    initMenuListView(this,touchMenuList,android.R.layout.simple_list_item_1);
  	    getSlidingMenu().setBehindOffset(80);
        // added code here 
        setContentView(scrollView);
        Log.d(TAG,"set scroll view");
        menu = inflater.inflate(R.layout.trickiestpart_menu, null);
        app = inflater.inflate(R.layout.trickiestpart_history, null);
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.history_tabBar);
        Log.d(TAG,"get the tabbar from history");
        
        ListView listView = (ListView) app.findViewById(R.id.lv_history_tricky);
        initListView(this, listView, android.R.layout.simple_list_item_1);

        listView = (ListView) menu.findViewById(R.id.menu_list);
        initMenuListView(this, listView, android.R.layout.simple_list_item_1);
        
//        ListView listView = (ListView) app.findViewById(R.id.list);
//        initListView(this, listView, android.R.layout.simple_list_item_1);

        btnSlide = (ImageView) tabBar.findViewById(R.id.history_BtnSlide);
        Log.d(TAG,"get slide button");
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));

        final View[] children = new View[] { menu,app};

        // Scroll to app (view[1]) when layout finished.
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
	}
	
	private void initListView(Context context, ListView listView,
			int layout) {
		// TODO Auto-generated method stub
		
		final ArrayList<String> arr = new ArrayList<String>();
		String dateString = new SimpleDateFormat("MM-dd-yyyy").format(new Date());
		Log.d(TAG, "dateString:" + dateString);
		try {
			// This will print 20100315
			for(int i = 0; i < MAX_DATEBEFORE; i++){
				//System.out.println(previousDateString(dateString));
				arr.add(dateString);
				Log.d(TAG, "arr：" + arr.get(i));
				dateString = previousDateString(dateString);
			}
		} catch (ParseException e) {
			System.out.println("Invalid date string");
			e.printStackTrace();
		}
		//final String[] arr = {"Today","Yesterday","Mon, Apr 15","Sun, Apr 14", "Sat, Apr 13", "Fri, Apr 12", "Thu, Apr 11", "Wed, Apr 10", "Tue, Apr 9"};
		 
    	final Integer[] images = { R.drawable.chart_pie_sample5,
            R.drawable.chart_pie_sample2, R.drawable.chart_pie_grey, R.drawable.chart_pie_grey,
            R.drawable.chart_pie_grey, R.drawable.chart_pie_grey, R.drawable.chart_pie_grey, R.drawable.chart_pie_grey, R.drawable.chart_pie_grey};
    	List<RowItem> rowItems;
    	rowItems = new ArrayList<RowItem>();
    	for (int i = 0; i < arr.size(); i++) {
            RowItem item = new RowItem(images[i], arr.get(i));
            rowItems.add(item);
        }
       HistoryListViewAdapter adapter = new HistoryListViewAdapter(this,
                R.layout.trickiestpart_list_item, rowItems);
        listView.setAdapter(adapter);
	}

	//initialize menu list items
    public  void initMenuListView(Context context,ListView listView,int layout){
     	final String[] arr = {"My Meal", "History","Tracking Setting","Help","Acknowledgment"};
     	final Integer[] images = { R.drawable.meal,
                 R.drawable.documents, R.drawable.settings, R.drawable.help ,R.drawable.comments};
         	List<RowItem> rowItems;
         	rowItems = new ArrayList<RowItem>();
             for (int i = 0; i < arr.length; i++) {
                 RowItem item = new RowItem(images[i], arr[i]);
                 rowItems.add(item);
             }
             CustomListViewAdapter menu_adapter = new CustomListViewAdapter(this,
                     R.layout.trickiestpart_list_item, rowItems);
     	listView.setAdapter(menu_adapter);
     	listView.setOnItemClickListener(new OnItemClickListener(){

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view, int position,
    					long id) {
    				// TODO Auto-generated method stub
    				Context context = view.getContext();
    				String str = arr[position];
    				if(str.equals("My Meal")){
    					vibrate(25);
    					startActivity(new Intent(context,Today.class));
    				}else if(str.equals("History")){
    					vibrate(25);
    					startActivity(new Intent(context,History.class));
    				}else if(str.equals("Tracking Setting")){
    					startActivity(new Intent(context,Tracking_Setting.class));
    				}else if(str.equals("Help")){
    					vibrate(25);
    					//startActivity(new Intent(context,TutorialActivity.class));
    					startActivity(new Intent(context,Tutorial.class));
    					Log.d(TAG,"no matcing item!");
    				}else {
    					vibrate(25);
    					startActivity(new Intent(context,Acknowledgment.class));
    				}
    				
    			}
     		
     	});
    }
    
    private void vibrate(int ms) {
	    Vibrator v = 
	      (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    v.vibrate(ms);
	  }
    
	public static String previousDateString(String dateString) 
			throws ParseException {
		// Create a date formatter using your format string
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

		// Parse the given date string into a Date object.
		// Note: This can throw a ParseException.
		Date myDate = dateFormat.parse(dateString);

		// Use the Calendar class to subtract one day
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(myDate);
		calendar.add(Calendar.DAY_OF_YEAR, -1);

		// Use the date formatter to produce a formatted date string
		Date previousDate = calendar.getTime();
		String result = dateFormat.format(previousDate);

		return result;
	}

}
